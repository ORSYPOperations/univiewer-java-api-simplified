package com.orsyp.du6;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.orsyp.UniverseException;
import com.orsyp.api.Context;
import com.orsyp.api.event.JobEvent;
import com.orsyp.api.event.JobEventId;
import com.orsyp.api.event.JobEventItem;
import com.orsyp.api.event.JobEventList;
import com.orsyp.api.execution.Execution;
import com.orsyp.api.execution.ExecutionFilter;
import com.orsyp.api.execution.ExecutionId;
import com.orsyp.api.execution.ExecutionItem;
import com.orsyp.api.execution.ExecutionList;
import com.orsyp.api.history.ExecutionHistory;
import com.orsyp.api.history.ExecutionHistoryId;
import com.orsyp.api.history.ExecutionHistoryItem;
import com.orsyp.api.launch.Launch;
import com.orsyp.api.launch.LaunchId;
import com.orsyp.api.launch.LaunchItem;
import com.orsyp.api.launch.LaunchList;
import com.orsyp.api.syntaxerules.OwlsSyntaxRules;
import com.orsyp.api.uproc.Uproc;
import com.orsyp.owls.impl.event.OwlsJobEventImpl;
import com.orsyp.owls.impl.event.OwlsJobEventListImpl;
import com.orsyp.owls.impl.execution.OwlsExecutionImpl;
import com.orsyp.owls.impl.execution.OwlsExecutionListImpl;
import com.orsyp.owls.impl.history.OwlsExecutionHistoryImpl;
import com.orsyp.owls.impl.launch.OwlsLaunchImpl;
import com.orsyp.owls.impl.launch.OwlsLaunchListImpl;

public class Executions extends GenericObjects {
	private Context GlobalCtx;

	public Executions (com.orsyp.stores.UVAPIWrapper conn, Context GlobalCtx){
		super(conn);
		this.GlobalCtx = GlobalCtx;
	}
	private Context getContext(){
		return GlobalCtx;
	}
	public JobEvent getJobEvent(JobEventItem item) throws UniverseException{
		JobEventId id = item.getIdentifier();
		JobEvent obj = new JobEvent(getContext(),id);
		obj.setImpl(new OwlsJobEventImpl());
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
	    obj.extract();
	    return obj;
	}
	public Execution getExecution(ExecutionItem item) throws Exception{  
		ExecutionId id = item.getIdentifier();
		Execution obj = new Execution(getContext(),id);
		obj.setImpl(new OwlsExecutionImpl());
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
	    obj.extract();
	    return obj;
	}
	public ExecutionHistory getExecutionHistory(ExecutionHistoryItem item) throws Exception{  
		ExecutionHistoryId id = item.getIdentifier();
		ExecutionHistory obj = new ExecutionHistory(getContext(),id);
		obj.setImpl(new OwlsExecutionHistoryImpl());
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
	    obj.extract();
	    return obj;
	}
	public Launch getLaunch(LaunchItem item) throws Exception{  
		LaunchId id = item.getIdentifier();
		Launch obj = new Launch(getContext(),id);
		obj.setImpl(new OwlsLaunchImpl());
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
	    obj.extract();
	    return obj;
	}
	public JobEventList getJobEventList() throws UniverseException{
		JobEventList list = new JobEventList(getContext());
		list.setImpl(new OwlsJobEventListImpl());
		list.extract();
		return list;
	}
	public ExecutionList getExecutionList() throws UniverseException{
		ExecutionFilter filter = new ExecutionFilter();
		ExecutionList list = new ExecutionList(getContext(),filter);
		list.setImpl(new OwlsExecutionListImpl());
		list.extract();
		return list;
	}
	public LaunchList getLaunchList() throws UniverseException{
		LaunchList list = new LaunchList(getContext());
		list.setImpl(new OwlsLaunchListImpl());
		list.extract();
		return list;
	}
	public String launchUprocNow(Uproc upr, String mu, String user) throws Exception{
		// why 20hrs? because the API rests the time to GMT everytime ...
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		//sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
		Date start = new Date(System.currentTimeMillis()-20*60*60*1000);
		String sDate= sdf.format(start);
		Date end = new Date(System.currentTimeMillis()+20*60*60*1000);
		String eDate= sdf.format(end);
				
		String numlanc = launchUproc(upr,mu,user,sDate.substring(0, 8),sDate.substring(0, 8),sDate.substring(8, 12),eDate.substring(0, 8),eDate.substring(8, 12));
		return numlanc;
	}
	public String launchUproc(Uproc upr, String mu, String user, String procDate, String startDate, String startTime, String endDate, String endTime) throws Exception{  
		
		LaunchId id = LaunchId.createWithName(null, null, upr.getName(), upr.getCurrentVersion(), mu, "0");
		Launch obj = new Launch(getContext(),id);
		
		obj.setImpl(new OwlsLaunchImpl());
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
		obj.setUserName(user);
		obj.setUprocName(upr.getName());
		obj.setQueue("SYS_BATCH");
		obj.setProcessingDate(procDate);
		obj.setMuName(mu);
		obj.setPriority("1");
		obj.setPrinter("IMPR");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmss");

        Date beginDate = sdf.parse(startDate+startTime+"00");
        Date finDate =   sdf.parse(endDate+endTime+"00");
		obj.setBeginDate(beginDate);
		obj.setEndDate(finDate);

		obj.create();
		return obj.getNumlanc();
	}
}
