package com.orsyp.du6;

import java.util.Date;

import com.orsyp.UniverseException;
import com.orsyp.api.Context;
import com.orsyp.api.execution.Execution;
import com.orsyp.api.note.Note;
import com.orsyp.api.note.NoteId;
import com.orsyp.api.note.NoteId.NoteType;
import com.orsyp.api.note.NoteItem;
import com.orsyp.api.note.NoteList;
import com.orsyp.api.note.NoteNature;
import com.orsyp.api.syntaxerules.OwlsSyntaxRules;
import com.orsyp.owls.impl.note.OwlsNoteImpl;
import com.orsyp.owls.impl.note.OwlsNoteListImpl;

public class Notes  extends GenericObjects{
	private Context GlobalCtx;

	public Notes (com.orsyp.stores.UVAPIWrapper conn, Context GlobalCtx){
		super(conn);
		this.GlobalCtx = GlobalCtx;
	}
	private Context getContext(){
		return GlobalCtx;
	}
	public Note getNote(NoteItem item) throws Exception{  
		NoteId id = item.getIdentifier();
		Note obj = new Note(getContext(),id);
		obj.setImpl(new OwlsNoteImpl());
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
	    obj.extract();
	    return obj;
	}
	public NoteList getNoteList() throws UniverseException{
		NoteList list = new NoteList(getContext());
		list.setImpl(new OwlsNoteListImpl());
		list.extract();
		return list;
	}
	public Note createNote(String noteText, Execution exec) throws Exception{  
		NoteId id = NoteId.create(NoteType.UNKNOWN, exec.getSessionName(), exec.getSessionVersion(), exec.getUprocName(), 
				exec.getUprocVersion(), exec.getTaskName(), exec.getTaskVersion(), exec.getMuName(), Integer.parseInt(exec.getNumsess()),
				Integer.parseInt(exec.getNumproc()), Integer.parseInt(exec.getNumlanc()));
		Note obj = new Note(getContext(),id);
		obj.setImpl(new OwlsNoteImpl());
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
		obj.setNoteNature(NoteNature.INTERNAL);
		obj.setNoteText(noteText);
		obj.setCreationDate(new Date());
	    obj.create();
	    return obj;
	}
	
}
