package com.orsyp.du6;

import java.util.ArrayList;

import com.orsyp.SyntaxException;
import com.orsyp.api.Context;
import com.orsyp.api.session.Session;
import com.orsyp.api.session.SessionAtom;
import com.orsyp.api.session.SessionData;
import com.orsyp.api.session.SessionFilter;
import com.orsyp.api.session.SessionId;
import com.orsyp.api.session.SessionItem;
import com.orsyp.api.session.SessionList;
import com.orsyp.api.syntaxerules.OwlsSyntaxRules;
import com.orsyp.api.uproc.Uproc;
import com.orsyp.owls.impl.session.OwlsSessionImpl;
import com.orsyp.owls.impl.session.OwlsSessionListImpl;

public class Sessions  extends GenericObjects {
	private Context GlobalCtx;

	public Sessions(com.orsyp.stores.UVAPIWrapper conn, Context GlobalCtx){
		super(conn);
		this.GlobalCtx = GlobalCtx;
	}
	private Context getContext(){
		return GlobalCtx;
	}
	public ArrayList<Uproc> getUprocListFromSession(Session obj) throws Exception{
		String[] uprLst = obj.getUprocs();
		ArrayList<Uproc> uprArray = new ArrayList<Uproc>();
		for(int i=0;i<uprLst.length;i++){
			//uprArray.add(super.getUVAPIWrapper().DU6Store.Uprocs.getUprocByName(uprLst[i], "000"));	// Strange ??
			uprArray.add(super.getUVAPIWrapper().DU6Store.Uprocs.getUprocByName(uprLst[i], obj.getVersion()));
		}
		return uprArray;
	}
	public SessionList getSessionList() throws Exception {  
        SessionFilter filter = new SessionFilter("*","*");
        SessionList list = new SessionList(getContext(), filter);
        list.setImpl(new OwlsSessionListImpl());
        list.extract();
        return list;
	}
	public Session getSession(SessionItem item) throws Exception{  
		SessionId id = item.getIdentifier();
		Session obj = new Session(getContext(),id);
		obj.setImpl(new OwlsSessionImpl());
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
	    obj.extract();
	    return obj;
	}
	public Session getSessionByName(String name, String version) throws Exception {
		SessionId id = new SessionId(name, version);
		Session u = new Session(getContext(), id);
		u.setImpl(new OwlsSessionImpl());
		u.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
		u.extract();
		return u;
	}
	public void deleteSession(String name, String version) throws Exception {
		Context ctx = getContext();
		Session obj = new Session(ctx, SessionId.create(name, version));
		obj.setImpl(new OwlsSessionImpl());
		obj.disableSyntaxCheck();
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
		obj.delete();
	}
	public Session duplicateSession(Session originalObj, String newName, String newLabel) throws Exception{  
		SessionId id = new SessionId(newName,originalObj.getVersion());
		id.setSyntaxRules(OwlsSyntaxRules.getInstance());
		originalObj.duplicate(id, newLabel);
		return getSessionByName(newName,originalObj.getVersion());
	}
	public Session createSession(String sessionName, String sessionLabel,String headerName, String sessionVersion) throws SyntaxException{
		Session ses = new Session(sessionName,sessionVersion);
		ses.setImpl(new OwlsSessionImpl());
		ses.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
        ses.setHeader(headerName);
        ses.setLabel(sessionLabel);
        SessionData sd = new SessionData();
        sd.setUprocName(headerName);
        ses.getTree().setRoot(new SessionAtom(sd));
        return ses;
	}
}
