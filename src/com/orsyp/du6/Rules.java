package com.orsyp.du6;

import java.util.Vector;

import com.orsyp.api.Context;
import com.orsyp.api.rule.MonthAuthorization;
import com.orsyp.api.rule.Rule;
import com.orsyp.api.rule.Rule.Direction;
import com.orsyp.api.rule.Rule.PeriodTypeEnum;
import com.orsyp.api.rule.RuleFilter;
import com.orsyp.api.rule.RuleId;
import com.orsyp.api.rule.RuleItem;
import com.orsyp.api.rule.RuleList;
import com.orsyp.api.rule.UniPattern;
import com.orsyp.api.rule.UniPattern.RunOverEnum;
import com.orsyp.api.rule.UniPattern.UDayType;
import com.orsyp.api.rule.WeekAuthorization;
import com.orsyp.api.rule.YearAuthorization;
import com.orsyp.api.syntaxerules.OwlsSyntaxRules;
import com.orsyp.api.task.Task;
import com.orsyp.api.task.TaskImplicitData;
import com.orsyp.api.task.TaskPlanifiedData;
import com.orsyp.owls.impl.rule.OwlsRuleImpl;
import com.orsyp.owls.impl.rule.OwlsRuleListImpl;

public class Rules  extends GenericObjects{
	private Context GlobalCtx;

	public Rules (com.orsyp.stores.UVAPIWrapper conn, Context GlobalCtx){
		super(conn);
		this.GlobalCtx = GlobalCtx;
	}
	private Context getContext(){
		return GlobalCtx;
	}
	public RuleList getRuleList() throws Exception {  
        RuleFilter filter = new RuleFilter("*","*");
        RuleList list = new RuleList(getContext(), filter);
        list.setImpl(new OwlsRuleListImpl());
        list.extract();
        return list;
	}
	public Rule getRuleByName(String name) throws Exception{  
		Rule obj = new Rule(getContext(), RuleId.create(name));
	    obj.setImpl(new OwlsRuleImpl());
	    obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance ());
	    obj.extract();
	    return obj;
	}
	public void deleteRule(String ruleName) throws Exception {
		Context ctx = getContext();
		Rule obj = new Rule(ctx, RuleId.create(ruleName));
		obj.setImpl(new OwlsRuleImpl());
		obj.disableSyntaxCheck();
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
		obj.delete();
	}
	public Rule getRule(RuleItem item) throws Exception{  
		RuleId id = item.getIdentifier();
		Rule obj = new Rule(getContext(),id);
		obj.setImpl(new OwlsRuleImpl());
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
	    obj.extract();
	    return obj;
	}
	public Vector<Rule> getRulesFromTask(Task obj) throws Exception{ 
		Vector<Rule> vector = new Vector<Rule>();
		String TaskType = obj.getSpecificData().getType();
		if(TaskType.equalsIgnoreCase("Planified_Task")){
			TaskPlanifiedData tpld = obj.getPlanifiedData(); // contains the list of Launch Times
			TaskImplicitData[] timpd = tpld.getImplicitData(); // Contains all Rules
			for(int i=0;i<timpd.length;i++){
				Rule rule = getRuleByName(timpd[i].getIdentifier().getName());
				vector.add(rule);
			}
		}
		return vector;
	}
	public Rule createRule(String ruleName, String ruleLabel) throws Exception{  
		RuleId id = RuleId.create(ruleName);
		Rule obj = new Rule(getContext(),id);
		obj.setImpl(new OwlsRuleImpl());
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
		
		obj.setLabel(ruleLabel);
		obj.setPeriodNumber(1);
		obj.setPeriodType(PeriodTypeEnum.DAY);
		UniPattern p = new UniPattern();
		p.setDayType(UDayType.CALENDAR);
		p.setPositionDirection(Direction.FROM_BEGINNING);
		p.setPositionInPeriod(1);
		p.setRunOver(RunOverEnum.YES);
		obj.setPattern(p);
		obj.setMonthAuthorization(new MonthAuthorization());
		obj.setWeekAuthorization(new WeekAuthorization());
		obj.setYearAuthorization(new YearAuthorization());
		
	    obj.create();
	    return obj;
	}
}