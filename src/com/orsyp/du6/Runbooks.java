package com.orsyp.du6;

import com.orsyp.api.Context;
import com.orsyp.api.runbook.Runbook;
import com.orsyp.api.runbook.RunbookContext;
import com.orsyp.api.runbook.RunbookFilter;
import com.orsyp.api.runbook.RunbookId;
import com.orsyp.api.runbook.RunbookItem;
import com.orsyp.api.runbook.RunbookList;
import com.orsyp.api.runbook.RunbookNature;
import com.orsyp.api.syntaxerules.OwlsSyntaxRules;
import com.orsyp.owls.impl.runbook.OwlsRunbookImpl;
import com.orsyp.owls.impl.runbook.OwlsRunbookListImpl;

public class Runbooks  extends GenericObjects{
	private Context GlobalCtx;

	public Runbooks (com.orsyp.stores.UVAPIWrapper conn, Context GlobalCtx){
		super(conn);
		this.GlobalCtx = GlobalCtx;
	}
	private Context getContext(){
		return GlobalCtx;
	}
	public RunbookList getRunbookList() throws Exception {  
        RunbookFilter filter = new RunbookFilter();
        RunbookList list = new RunbookList(getContext(), filter);
        list.setImpl(new OwlsRunbookListImpl());
        list.extract();
        return list;
	}
	public Runbook getRunbook(RunbookItem item) throws Exception{  
		RunbookId id = item.getIdentifier();
		Runbook obj = new Runbook(getContext(),id);
		obj.setImpl(new OwlsRunbookImpl());
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
	    obj.extract();
	    return obj;
	}
	public Runbook createRunbookInternal(String runbookName,RunbookContext runbookParams, String runbookContent) throws Exception{  
		RunbookId id =RunbookId.create(runbookName);
		Runbook obj = new Runbook(getContext(),id);
		obj.setImpl(new OwlsRunbookImpl());
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
		obj.setRunbookNature(RunbookNature.INTERNAL);
		obj.setRunbookContent(runbookContent);
		
		obj.setRunbookContext(runbookParams);
	    obj.create();
	    return obj;
	}
}
