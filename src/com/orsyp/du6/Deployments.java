package com.orsyp.du6;

import com.orsyp.Area;
import com.orsyp.UniverseException;
import com.orsyp.api.Context;
import com.orsyp.api.history.deployment.DeploymentHistoryItem;
import com.orsyp.api.history.deployment.DeploymentHistoryList;
import com.orsyp.api.node.Node;
import com.orsyp.api.syntaxerules.OwlsSyntaxRules;
import com.orsyp.kmeleon.persistence.NetworkConfigurationFactory;
import com.orsyp.kmeleon.universe.deployment.NodeLocation;
import com.orsyp.kmeleon.universe.deployment.PackageLocation;
import com.orsyp.owls.impl.deploymenthistory.OwlsDeploymentHistoryListImpl;

public class Deployments extends GenericObjects  {
	private Context GlobalCtx;

	public Deployments (com.orsyp.stores.UVAPIWrapper conn, Context GlobalCtx){
		super(conn);
		this.GlobalCtx = GlobalCtx;
	}
	private Context getContext(){
		return GlobalCtx;
	}

	public DeploymentHistoryList getDeploymentList() throws UniverseException{
	DeploymentHistoryList list = new DeploymentHistoryList(getContext());
		
		list.setSyntaxRules(OwlsSyntaxRules.getInstance());
		list.setImpl(new OwlsDeploymentHistoryListImpl());
		list.extract();
		
		return list;
	}
	public void showDeploymentHistoryItem(DeploymentHistoryItem item){
		
		String a = Integer.toString(item.getIdentifier().getBatchId());
		String b = item.getLabel();
		String c = item.getDate().toString();
		String d = item.getIdentifier().getObjectType().name();
		String e = item.getIdentifier().getObjectId();
		String f = item.getIdentifier().getOriginType().name();
		String g = item.getIdentifier().getOrigin();
		String h = item.getIdentifier().getDestinationType().name();
		String i = item.getIdentifier().getDestination();
		String j = item.getStatus().name();
		
		System.out.println(a+" "+b+" "+c+" "+d+" "+e+" "+f+" "+g+" "+h+" "+i+" "+j);
	}
	
	public void test(String packageName, String nodeName, Area area) throws Exception{
		PackageLocation pckLoc = new PackageLocation();
		pckLoc.setPackageId(super.getUVAPIWrapper().UVMSStore.Packages.getDu6PackageFromName(packageName).getIdentifier());
		
		Node node = super.getUVAPIWrapper().DU6Store.Nodes.getDuNodeByName(nodeName);
		NodeLocation nodeLoc = new NodeLocation(node.getIdentifier(),area);
		
		NetworkConfigurationFactory.getInstance().getPersistence(null).getNodeConfigurations().getNodeConfiguration(getContext().getEnvironment());
		
		//NodeConfigurations nodeConfigurations =  NetworkConfigurationFactory.getInstance().getPersistence().getAllNodeConfigurations();

		//nodeConfigurations.;
	  //  List<NodeConfiguration> nodeConfigurations = NetworkConfigurationFactory.getInstance().getPersistence(null).getNodeConfigurations().getNodeConfigurations(PRODUCT_TYPE.DU_OWLS);
	 // System.out.println("LILI: " +nodeConfigurations.size());
	    
	 //   NodeCompanyConfiguration config = nodeConfigurations.getNodeCompanyConfiguration("UNIPRD","AGENT_PRD_01", PRODUCT_TYPE.DU_OWLS);
	    
	  //  System.out.println("DEBUG LALA:"+config.getHostname());
	    
	   // Product product = nodeConfigurations.getProductForEnvironment (environment);
		
		//UnifiedDeploymentBusiness udb = new UnifiedDeploymentBusiness(getContext().getEnvironment());
		
		
		
		
	}

	
	
}
