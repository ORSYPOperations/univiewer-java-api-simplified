package com.orsyp.du6;

import java.util.Date;

import com.orsyp.UniverseException;
import com.orsyp.api.Context;
import com.orsyp.api.license.License;
import com.orsyp.api.license.LicenseFilter;
import com.orsyp.api.license.LicenseId;
import com.orsyp.api.license.LicenseItem;
import com.orsyp.api.license.LicenseList;
import com.orsyp.api.security.Operation;
import com.orsyp.api.syntaxerules.OwlsSyntaxRules;
import com.orsyp.std.license.LicenseListStdImpl;
import com.orsyp.std.license.LicenseStdImpl;
import com.orsyp.tools.stdlicense.StdLicense;

public class Licenses extends GenericObjects{
	private Context GlobalCtx;
	
	public Licenses (com.orsyp.stores.UVAPIWrapper conn, Context GlobalCtx){
		super(conn);
		this.GlobalCtx = GlobalCtx;
	}
	private Context getContext(){
		return GlobalCtx;
	}
	
	public LicenseList getLicenseList() throws UniverseException{
		   LicenseFilter filter = new LicenseFilter();
		   LicenseList list = new LicenseList(getContext(), filter);
	        list.setSyntaxRules(OwlsSyntaxRules.getInstance());
	        LicenseListStdImpl impl = new LicenseListStdImpl();
	        impl.init(list, Operation.DISPLAYLIST);
	        list.setImpl(impl);
	        list.extract();
	        return list;
	}
	public LicenseList getLicenseListWithFilter(String product) throws UniverseException{
		   LicenseFilter filter = new LicenseFilter();
		   filter.setProduct(product);
		   LicenseList list = new LicenseList(getContext(), filter);
	        list.setSyntaxRules(OwlsSyntaxRules.getInstance());
	        LicenseListStdImpl impl = new LicenseListStdImpl();
	        impl.init(list, Operation.DISPLAYLIST);
	        list.setImpl(impl);
	        list.extract();
	        return list;
	}
	public License getLicense(LicenseItem item) throws UniverseException{
		
		License obj = new License(getContext(),item.getIdentifier());
		
	     LicenseStdImpl impl = new LicenseStdImpl();
	     impl.init(obj);
	     obj.setImpl(impl);
		 obj.extract();

	    return obj;
	}
	public void addLicense(String nodeName, String product, String version, Date expDate, String licKey) throws UniverseException{
		LicenseId id = new LicenseId(nodeName, product, version);
		id.setSyntaxRules(OwlsSyntaxRules.getInstance());
		License obj = new License(getContext(),id);
		obj.setImpl(new LicenseStdImpl());
		obj.setExpirationDate(expDate);
		obj.setKey(licKey);
		obj.create();
	}
	public boolean isLicenseValid(String license){
		// Parameter passed must be the entire license line.
		StdLicense licTool = new StdLicense(license);
		return licTool.isKeyValid();
	}
	public boolean isLicenseExpired(String license){
		// Parameter passed must be the entire license line.
		StdLicense licTool = new StdLicense(license);
		return licTool.isExpired();
	}
	public boolean isLicenseValid(LicenseItem ent){
		//  public StdLicense(String node, String product, String release, String date, String key) {
		StdLicense licTool = new StdLicense(ent.getNodeName(),ent.getProduct(),ent.getProductVersion(),ent.getExpirationDate().toString(),ent.getKey());
		return licTool.isKeyValid();
	}
	public boolean isLicenseExpired(LicenseItem ent){
		//  public StdLicense(String node, String product, String release, String date, String key) {
		StdLicense licTool = new StdLicense(ent.getNodeName(),ent.getProduct(),ent.getProductVersion(),ent.getExpirationDate().toString(),ent.getKey());
		return licTool.isExpired();
	}
	
}
