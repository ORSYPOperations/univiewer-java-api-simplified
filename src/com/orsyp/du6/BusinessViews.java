package com.orsyp.du6;

import com.orsyp.api.Context;
import com.orsyp.api.bv.BusinessView;
import com.orsyp.api.bv.BusinessViewFilter;
import com.orsyp.api.bv.BusinessViewId;
import com.orsyp.api.bv.BusinessViewItem;
import com.orsyp.api.bv.BusinessViewList;
import com.orsyp.api.syntaxerules.OwlsSyntaxRules;
import com.orsyp.owls.impl.bv.OwlsBusinessViewImpl;
import com.orsyp.owls.impl.bv.OwlsBusinessViewListImpl;

public class BusinessViews extends GenericObjects  {  
	private Context GlobalCtx;

	public BusinessViews (com.orsyp.stores.UVAPIWrapper conn, Context GlobalCtx){
		super(conn);
		this.GlobalCtx = GlobalCtx;
	}
	private Context getContext(){
		return GlobalCtx;
	}
	public BusinessView getBusinessView(BusinessViewItem item) throws Exception{  
		BusinessViewId id = item.getIdentifier();
		BusinessView obj = new BusinessView(getContext(),id);
		obj.setImpl(new OwlsBusinessViewImpl());
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
	    obj.extract();
	    return obj;
	}
	public BusinessViewList getBusinessViewList() throws Exception {
		BusinessViewFilter filter = new BusinessViewFilter();
		BusinessViewList list = new BusinessViewList(getContext(), filter);
		list.setImpl(new OwlsBusinessViewListImpl());
		list.extract();
		return list;
	}
}
