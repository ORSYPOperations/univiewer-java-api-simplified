package com.orsyp.du6;

import com.orsyp.SyntaxException;
import com.orsyp.UniverseException;
import com.orsyp.api.Context;
import com.orsyp.api.event.ResourceEvent;
import com.orsyp.api.event.ResourceEventItem;
import com.orsyp.api.event.ResourceEventList;
import com.orsyp.api.resource.Resource;
import com.orsyp.api.resource.ResourceFilter;
import com.orsyp.api.resource.ResourceId;
import com.orsyp.api.resource.ResourceItem;
import com.orsyp.api.resource.ResourceList;
import com.orsyp.api.resource.ResourceType;
import com.orsyp.api.resource.ResourceTypeFilter;
import com.orsyp.api.resource.ResourceTypeId;
import com.orsyp.api.resource.ResourceTypeItem;
import com.orsyp.api.resource.ResourceTypeList;
import com.orsyp.api.resource.SpecificFile;
import com.orsyp.api.resource.SpecificJms;
import com.orsyp.api.resource.SpecificLogical;
import com.orsyp.api.resource.SpecificSAPEvent;
import com.orsyp.api.resource.SpecificSAPJob;
import com.orsyp.api.resource.SpecificSOAP;
import com.orsyp.api.resource.SpecificSPMonitor;
import com.orsyp.api.resource.SpecificScript;
import com.orsyp.api.resource.SpecificSystem;
import com.orsyp.api.resource.SpecificWSRest;
import com.orsyp.api.syntaxerules.OwlsSyntaxRules;
import com.orsyp.owls.impl.event.OwlsResourceEventImpl;
import com.orsyp.owls.impl.event.OwlsResourceEventListImpl;
import com.orsyp.owls.impl.resource.OwlsResourceImpl;
import com.orsyp.owls.impl.resource.OwlsResourceListImpl;
import com.orsyp.owls.impl.resource.OwlsResourceTypeListImpl;

public class Resources  extends GenericObjects{
	private Context GlobalCtx;

	public Resources (com.orsyp.stores.UVAPIWrapper conn, Context GlobalCtx){
		super(conn);
		this.GlobalCtx = GlobalCtx;
	}
	private Context getContext(){
		return GlobalCtx;
	}
	public ResourceList getResourceList() throws Exception {  
        ResourceFilter filter = new ResourceFilter("*");
        ResourceList list = new ResourceList(getContext(), filter);
        list.setImpl(new OwlsResourceListImpl());
        list.extract();
        return list;
	}
	public ResourceEventList getResourceEventList() throws UniverseException{
		ResourceEventList list = new ResourceEventList(getContext());
		list.setImpl(new OwlsResourceEventListImpl());
		list.extract();
		return list;
	}
	public ResourceEvent getResourceEvent(ResourceEventItem item) throws UniverseException{
		ResourceId id = item.getIdentifier();
		ResourceEvent obj = new ResourceEvent(getContext(),id);
		obj.setImpl(new OwlsResourceEventImpl());
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
	    //obj.extract();
	    return obj;
	}
	public Resource getResource(ResourceItem item) throws Exception{  
		ResourceId id = item.getIdentifier();
		Resource obj = new Resource(getContext(),id);
		obj.setImpl(new OwlsResourceImpl());
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
	    obj.extract();
	    return obj;
	}
	public void deleteResource(String resName) throws Exception {
		Context ctx = getContext();
		Resource obj = new Resource(ctx, ResourceId.create(resName));
		obj.setImpl(new OwlsResourceImpl());
		obj.disableSyntaxCheck();
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
		obj.delete();
	}
	public ResourceTypeList getResourceTypeList() throws Exception {  
        ResourceTypeFilter filter = new ResourceTypeFilter("*");
        ResourceTypeList list = new ResourceTypeList(getContext(), filter);
        list.setImpl(new OwlsResourceTypeListImpl());
        list.extract();
        return list;
	}
	public ResourceType getResourceType(ResourceTypeItem item) throws Exception{  
		ResourceTypeId id = item.getIdentifier();
		ResourceType obj = new ResourceType(getContext(),id);
		//obj.setImpl(new OwlsResourceTypeImpl()); ??
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
	    obj.extract();
	    return obj;
	}
	private Resource createResourceStub(String resourceName, String resourceLabel, int frequency) throws SyntaxException{
		Resource obj = new Resource(getContext(), ResourceId.create(resourceName));
		obj.setImpl(new OwlsResourceImpl());
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
		obj.setFrequency(frequency);
		obj.setLabel(resourceLabel);
		obj.setReference(resourceName);
		return obj;
	}
	public Resource createResourceLogical(String resourceName, String resourceLabel, SpecificLogical resourceParams) throws UniverseException{
		Resource obj = createResourceStub(resourceName, resourceLabel, 30);
		obj.setNature("LOG");// "FIL", "LOG", "SAE", "SAJ", "JMS", "SCR", "SYS", "SPM", "WSP", "WRT"
		obj.setSpecificData(resourceParams);
		obj.create();
		return obj;
	}
	public Resource createResourceFile(String resourceName, String resourceLabel, SpecificFile resourceParams) throws UniverseException{
		Resource obj = createResourceStub(resourceName, resourceLabel, 30);
		obj.setNature("FIL");// "FIL", "LOG", "SAE", "SAJ", "JMS", "SCR", "SYS", "SPM", "WSP", "WRT"
		obj.setSpecificData(resourceParams);
		obj.create();
		return obj;
	}
	public Resource createResourceScript(String resourceName, String resourceLabel, SpecificScript resourceParams) throws UniverseException{
		Resource obj = createResourceStub(resourceName, resourceLabel, 30);
		obj.setNature("SCR");// "FIL", "LOG", "SAE", "SAJ", "JMS", "SCR", "SYS", "SPM", "WSP", "WRT"
		obj.setSpecificData(resourceParams);
		obj.create();
		return obj;
	}
	public Resource createResourceSystem(String resourceName, String resourceLabel, SpecificSystem resourceParams) throws UniverseException{
		Resource obj = createResourceStub(resourceName, resourceLabel, 30);
		obj.setNature("SYS");// "FIL", "LOG", "SAE", "SAJ", "JMS", "SCR", "SYS", "SPM", "WSP", "WRT"
		obj.setSpecificData(resourceParams);
		obj.create();
		return obj;
	}
	public Resource createResourceSoap(String resourceName, String resourceLabel, SpecificSOAP resourceParams) throws UniverseException{
		Resource obj = createResourceStub(resourceName, resourceLabel, 30);
		obj.setNature("WSP");// "FIL", "LOG", "SAE", "SAJ", "JMS", "SCR", "SYS", "SPM", "WSP", "WRT"
		obj.setSpecificData(resourceParams);
		obj.create();
		return obj;
	}
	public Resource createResourceRest(String resourceName, String resourceLabel, SpecificWSRest resourceParams) throws UniverseException{
		Resource obj = createResourceStub(resourceName, resourceLabel, 30);
		obj.setNature("WRT");// "FIL", "LOG", "SAE", "SAJ", "JMS", "SCR", "SYS", "SPM", "WSP", "WRT"
		obj.setSpecificData(resourceParams);
		obj.create();
		return obj;
	}
	public Resource createResourceSpm(String resourceName, String resourceLabel, SpecificSPMonitor resourceParams) throws UniverseException{
		Resource obj = createResourceStub(resourceName, resourceLabel, 30);
		obj.setNature("SPM");// "FIL", "LOG", "SAE", "SAJ", "JMS", "SCR", "SYS", "SPM", "WSP", "WRT"
		obj.setSpecificData(resourceParams);
		obj.create();
		return obj;
	}
	public Resource createResourceJms(String resourceName, String resourceLabel, SpecificJms resourceParams) throws UniverseException{
		Resource obj = createResourceStub(resourceName, resourceLabel, 30);
		obj.setNature("JMS");// "FIL", "LOG", "SAE", "SAJ", "JMS", "SCR", "SYS", "SPM", "WSP", "WRT"
		obj.setSpecificData(resourceParams);
		obj.create();
		return obj;
	}
	public Resource createResourceSapJob(String resourceName, String resourceLabel, SpecificSAPJob resourceParams) throws UniverseException{
		Resource obj = createResourceStub(resourceName, resourceLabel, 30);
		obj.setNature("SAJ");// "FIL", "LOG", "SAE", "SAJ", "JMS", "SCR", "SYS", "SPM", "WSP", "WRT"
		obj.setSpecificData(resourceParams);
		obj.create();
		return obj;
	}
	public Resource createResourceSapEvent(String resourceName, String resourceLabel, SpecificSAPEvent resourceParams) throws UniverseException{
		Resource obj = createResourceStub(resourceName, resourceLabel, 30);
		obj.setNature("SAE");// "FIL", "LOG", "SAE", "SAJ", "JMS", "SCR", "SYS", "SPM", "WSP", "WRT"
		obj.setSpecificData(resourceParams);
		obj.create();
		return obj;
	}
	
}
