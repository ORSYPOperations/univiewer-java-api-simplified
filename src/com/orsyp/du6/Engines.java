package com.orsyp.du6;

import com.orsyp.UniverseException;
import com.orsyp.api.Context;
import com.orsyp.api.engine.Engine;
import com.orsyp.api.engine.EngineId;
import com.orsyp.api.engine.EngineItem;
import com.orsyp.api.engine.EngineList;
import com.orsyp.api.syntaxerules.OwlsSyntaxRules;
import com.orsyp.owls.impl.engine.OwlsEngineImpl;
import com.orsyp.owls.impl.engine.OwlsEngineListImpl;

public class Engines extends GenericObjects  {
	private Context GlobalCtx;

	public Engines (com.orsyp.stores.UVAPIWrapper conn, Context GlobalCtx){
		super(conn);
		this.GlobalCtx = GlobalCtx;
	}
	private Context getContext(){
		return GlobalCtx;
	}
	public EngineList getEngineList() throws UniverseException{
		EngineList list = new EngineList(getContext());
		list.setSyntaxRules(OwlsSyntaxRules.getInstance());
		list.setImpl(new OwlsEngineListImpl());
		list.extract();
		return list;
	}
	public Engine getEngine(EngineItem item) throws UniverseException{
		EngineId id = item.getIdentifier();
		Engine obj = new Engine(getContext(),id);
		obj.setImpl(new OwlsEngineImpl());
		//obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
	    obj.extract();
	    return obj;
	}
}
