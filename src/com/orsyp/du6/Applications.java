package com.orsyp.du6;

import com.orsyp.api.Context;
import com.orsyp.api.application.Application;
import com.orsyp.api.application.ApplicationFilter;
import com.orsyp.api.application.ApplicationId;
import com.orsyp.api.application.ApplicationItem;
import com.orsyp.api.application.ApplicationList;
import com.orsyp.api.syntaxerules.OwlsSyntaxRules;
import com.orsyp.owls.impl.application.OwlsApplicationImpl;
import com.orsyp.owls.impl.application.OwlsApplicationListImpl;

public class Applications extends GenericObjects {
	private Context GlobalCtx;

	public Applications (com.orsyp.stores.UVAPIWrapper conn, Context GlobalCtx){
		super(conn);
		this.GlobalCtx = GlobalCtx;
	}
	private Context getContext(){
		return GlobalCtx;
	}
	public ApplicationList getApplicationList() throws Exception {  
        ApplicationFilter filter = new ApplicationFilter("*");
        ApplicationList list = new ApplicationList(getContext(), filter);
        list.setImpl(new OwlsApplicationListImpl());
        list.extract();
        return list;
	}
	public Application getApplication(ApplicationItem item) throws Exception{  
		ApplicationId id = item.getIdentifier();
		Application obj = new Application(getContext(),id);
		obj.setImpl(new OwlsApplicationImpl());
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
	    obj.extract();
	    return obj;
	}
	public void createApp(String appName, String label) throws Exception {
        Application obj = new Application(getContext(), appName);
        obj.setDomain("I");
        obj.setLabel(label);
        obj.setImpl(new OwlsApplicationImpl());
        obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
        obj.create();
	}
}
