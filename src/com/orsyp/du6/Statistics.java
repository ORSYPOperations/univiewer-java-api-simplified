package com.orsyp.du6;

import com.orsyp.api.Context;
import com.orsyp.api.hr.HrStatistic;
import com.orsyp.api.hr.HrStatisticFilter;
import com.orsyp.api.hr.HrStatisticId;
import com.orsyp.api.hr.HrStatisticItem;
import com.orsyp.api.hr.HrStatisticList;
import com.orsyp.api.syntaxerules.OwlsSyntaxRules;
import com.orsyp.owls.impl.hr.OwlsStatisticImpl;
import com.orsyp.owls.impl.hr.OwlsStatisticListImpl;

public class Statistics  extends GenericObjects{
	private Context GlobalCtx;

	public Statistics (com.orsyp.stores.UVAPIWrapper conn, Context GlobalCtx){
		super(conn);
		this.GlobalCtx = GlobalCtx;
	}
	private Context getContext(){
		return GlobalCtx;
	}
	public HrStatistic getHrStatistic(HrStatisticItem item) throws Exception{  
		HrStatisticId id = item.getIdentifier();
		HrStatistic obj = new HrStatistic(getContext(),id);
		obj.setImpl(new OwlsStatisticImpl());
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
	    obj.extract();
	    return obj;
	}	
	public HrStatisticList getStatisticList() throws Exception{  
		 HrStatisticFilter filter = new HrStatisticFilter();
		 HrStatisticList list = new HrStatisticList(getContext(), filter);
		 list.setImpl(new OwlsStatisticListImpl());
	        
	        list.extract();
	    return list;
	}	
}
