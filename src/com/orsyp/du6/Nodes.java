package com.orsyp.du6;

import com.orsyp.UniverseException;
import com.orsyp.api.Context;
import com.orsyp.api.node.Node;
import com.orsyp.api.node.NodeFilter;
import com.orsyp.api.node.NodeId;
import com.orsyp.api.node.NodeItem;
import com.orsyp.api.node.NodeList;
import com.orsyp.api.node.NodeVariable;
import com.orsyp.api.node.NodeVariableId;
import com.orsyp.api.settings.NodeSettingItem;
import com.orsyp.api.settings.NodeSettingList;
import com.orsyp.api.syntaxerules.OwlsSyntaxRules;
import com.orsyp.owls.impl.node.OwlsNodeImpl;
import com.orsyp.owls.impl.node.OwlsNodeListImpl;
import com.orsyp.owls.impl.nodesetting.OwlsNodeSettingListImpl;
import com.orsyp.std.NodeVariableStdImpl;

public class Nodes  extends GenericObjects{
	private Context GlobalCtx;

	public Nodes (com.orsyp.stores.UVAPIWrapper conn, Context GlobalCtx){
		super(conn);
		this.GlobalCtx = GlobalCtx;
	}
	private Context getContext(){
		return GlobalCtx;
	}
	public NodeSettingItem getNodeSetting(NodeSettingItem item) throws Exception{  
		return item;
	}

	public NodeList getDuNodeList() throws Exception {  
        NodeFilter filter = new NodeFilter("*");
        NodeList list = new NodeList(getContext(), filter);
        list.setImpl(new OwlsNodeListImpl());
        list.extract();
        return list;
	}
	public Node getDuNode(NodeItem item) throws Exception {  
		NodeId id = item.getIdentifier();
		Node obj = new Node(getContext(),id);
		obj.setImpl(new OwlsNodeImpl());
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
	    obj.extract();
	    return obj;
	}
	public Node getDuNodeByName(String name) throws Exception {  
		NodeId id = new NodeId(name);
		Node u = new Node(getContext(), id);
		u.setImpl(new OwlsNodeImpl());
		u.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
		u.extract();
		return u;
	}
	public NodeSettingList getNodeSettingList() throws UniverseException{
		NodeSettingList list = new NodeSettingList(getContext());
		list.setImpl(new OwlsNodeSettingListImpl());
		list.extract();
		return list;
	}
	public NodeVariable getNodeVariable(String variableName) throws UniverseException{
		NodeVariable nodeVariable = new NodeVariable(getContext(), new NodeVariableId(variableName));
		nodeVariable.setImpl(new NodeVariableStdImpl());
		
		nodeVariable.extract();
		return nodeVariable;
	}
	public void createNodeVariable(String varName, String value) throws Exception {
		//a specific context is needed, node var creation works only on area X
		NodeVariable nv = new NodeVariable(getContext(),varName);
		nv.setImpl(new NodeVariableStdImpl());
		nv.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
		nv.setVariableValue(value);
		nv.create();
	}

}
