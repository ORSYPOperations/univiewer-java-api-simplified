package com.orsyp.du6;

import com.orsyp.api.Context;
import com.orsyp.api.syntaxerules.OwlsSyntaxRules;
import com.orsyp.api.user.User;
import com.orsyp.api.user.UserFilter;
import com.orsyp.api.user.UserId;
import com.orsyp.api.user.UserItem;
import com.orsyp.api.user.UserList;
import com.orsyp.api.user.UserType;
import com.orsyp.owls.impl.user.OwlsUserImpl;
import com.orsyp.owls.impl.user.OwlsUserListImpl;

public class Users  extends GenericObjects{
	private Context GlobalCtx;

	public Users (com.orsyp.stores.UVAPIWrapper conn, Context GlobalCtx){
		super(conn);
		this.GlobalCtx = GlobalCtx;
	}
	private Context getContext(){
		return GlobalCtx;
	}
	
	public void deleteUser(String userName) throws Exception {
		Context ctx = getContext();
		User obj = new User(ctx, UserId.create(userName));
		obj.setImpl(new OwlsUserImpl());
		obj.disableSyntaxCheck();
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
		obj.delete();
	}
	public UserList getDu6UserList() throws Exception {  
        UserFilter filter = new UserFilter("*","*");
        UserList list = new UserList(getContext(), filter);
        list.setImpl(new OwlsUserListImpl());
        list.extract();
        return list;
	}
	public User getUser(UserItem item) throws Exception{  
		UserId id = item.getIdentifier();
		User obj = new User(getContext(),id);
		obj.setImpl(new OwlsUserImpl());
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
	    obj.extract();
	    return obj;
	}
	public User getUserByName(String name) throws Exception {
		UserId id = UserId.create(name);
		User u = new User(getContext(), id);
		u.setImpl(new OwlsUserImpl());
		u.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
		u.extract();
		return u;
	}
	public User createUser(String userName, String userLabel) throws Exception{  
		UserId id = UserId.create(userName);
		User obj = new User(getContext(),id);
		obj.setImpl(new OwlsUserImpl());
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
		obj.setLabel(userLabel);
		obj.setProfile("PROFADM");
		obj.setUserType(UserType.Both);
		
	    obj.create();
	    return obj;
	}
}
