package com.orsyp.du6;

import com.orsyp.api.Context;
import com.orsyp.api.domain.Domain;
import com.orsyp.api.domain.DomainFilter;
import com.orsyp.api.domain.DomainId;
import com.orsyp.api.domain.DomainItem;
import com.orsyp.api.domain.DomainList;
import com.orsyp.api.syntaxerules.OwlsSyntaxRules;
import com.orsyp.owls.impl.domain.OwlsDomainImpl;
import com.orsyp.owls.impl.domain.OwlsDomainListImpl;

public class Domains extends GenericObjects  {
	private Context GlobalCtx;

	public Domains (com.orsyp.stores.UVAPIWrapper conn, Context GlobalCtx){
		super(conn);
		this.GlobalCtx = GlobalCtx;
	}
	private Context getContext(){
		return GlobalCtx;
	}
	public DomainList getDomainList() throws Exception {  
        DomainFilter filter = new DomainFilter("*");
        DomainList list = new DomainList(getContext(), filter);
        list.setImpl(new OwlsDomainListImpl());
        list.extract();
        return list;
	}
	public Domain getDomain(DomainItem item) throws Exception{  
		DomainId id = item.getIdentifier();
		Domain obj = new Domain(getContext(),id);
		obj.setImpl(new OwlsDomainImpl());
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
	    obj.extract();
	    return obj;
	}
	public void createDomain(String name, String label) throws Exception {
        Domain obj = new Domain(getContext(), name);
        obj.setLabel(label);
        obj.setImpl(new  OwlsDomainImpl());
        obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
        obj.create();
	}
}
