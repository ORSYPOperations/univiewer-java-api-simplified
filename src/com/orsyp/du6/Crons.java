package com.orsyp.du6;

import java.io.IOException;
import java.util.List;

import com.orsyp.UniverseException;
import com.orsyp.api.Context;
import com.orsyp.api.mu.Mu;
import com.orsyp.api.rule.Rule;
import com.orsyp.api.task.Task;
import com.orsyp.api.task.TaskPlanifiedData;
import com.orsyp.api.uproc.Uproc;
import com.orsyp.api.user.User;
import com.orsyp.schedconv.SchedConv;
import com.orsyp.schedconv.SchedFile;
import com.orsyp.schedconv.SchedJob;
import com.orsyp.schedconv.SchedNaming;
import com.orsyp.schedconv.converters.MUConverter;
import com.orsyp.schedconv.converters.RuleConverter;
import com.orsyp.schedconv.converters.SchedNamingConverter;
import com.orsyp.schedconv.converters.TaskConverter;
import com.orsyp.schedconv.converters.TaskPlanifiedDataConverter;
import com.orsyp.schedconv.converters.UprocConverter;
import com.orsyp.schedconv.converters.UserConverter;
import com.orsyp.schedconv.std.cron.SchedJobStdCronImpl;
import com.orsyp.schedconv.std.cron.SchedStdCronImplFactory;
import com.orsyp.std.ConnectionFactory;
import com.orsyp.std.EnvironmentStdImpl;
import com.orsyp.std.NodeConnectionFactory;
import com.orsyp.uvms6.GenericObjects;

	public class Crons  extends GenericObjects {
		private Context GlobalCtx;

		public Crons (com.orsyp.stores.UVAPIWrapper conn, Context GlobalCtx){
			super(conn);
			this.GlobalCtx = GlobalCtx;
		}
		private Context getContext(){
			return GlobalCtx;
		}

public Uproc convertIntoUproc(SchedJob job) throws UniverseException{
	ConnectionFactory connFactory = NodeConnectionFactory.getInstance(super.getUVAPIWrapper().central);
	SchedJobStdCronImpl impl = new SchedJobStdCronImpl(connFactory);
	impl.init(job);
	UprocConverter uconv = new UprocConverter(impl);
	TaskConverter tconv = new TaskConverter(impl);
	Uproc u = uconv.convert();
	return u;
}
// FOLLOWING METHOD is possibly for Unijob
public SchedNaming convertIntoSchedNaming(SchedJob job) throws UniverseException{
	ConnectionFactory connFactory = NodeConnectionFactory.getInstance(super.getUVAPIWrapper().central);
	SchedJobStdCronImpl impl = new SchedJobStdCronImpl(connFactory);
	impl.init(job);
	SchedNamingConverter conv = new SchedNamingConverter(impl);
	SchedNaming obj = conv.convert();
	return obj;
}
public Task convertIntoTask(SchedJob job) throws UniverseException{
	ConnectionFactory connFactory = NodeConnectionFactory.getInstance(super.getUVAPIWrapper().central);
	SchedJobStdCronImpl impl = new SchedJobStdCronImpl(connFactory);
	impl.init(job);
	TaskConverter tconv = new TaskConverter(impl);
	Task t = tconv.convert();
	return t;
}
public TaskPlanifiedData convertIntoTaskPlanified(SchedJob job) throws UniverseException{
	ConnectionFactory connFactory = NodeConnectionFactory.getInstance(super.getUVAPIWrapper().central);
	SchedJobStdCronImpl impl = new SchedJobStdCronImpl(connFactory);
	impl.init(job);
	
	TaskPlanifiedDataConverter conv = new TaskPlanifiedDataConverter(impl);
	TaskPlanifiedData obj = conv.convert();
	return obj;
}
public Mu convertIntoMU(SchedJob job) throws UniverseException{
	ConnectionFactory connFactory = NodeConnectionFactory.getInstance(super.getUVAPIWrapper().central);
	SchedJobStdCronImpl impl = new SchedJobStdCronImpl(connFactory);
	impl.init(job);
	
	MUConverter conv = new MUConverter(impl);
	Mu obj = conv.convert();
	return obj;
}
public Rule convertIntoRule(SchedJob job) throws UniverseException{
	ConnectionFactory connFactory = NodeConnectionFactory.getInstance(super.getUVAPIWrapper().central);
	SchedJobStdCronImpl impl = new SchedJobStdCronImpl(connFactory);
	impl.init(job);
	
	RuleConverter conv = new RuleConverter(impl);
	Rule obj = conv.convert();
	return obj;
}
public User convertIntoUser(SchedJob job) throws UniverseException{
	ConnectionFactory connFactory = NodeConnectionFactory.getInstance(super.getUVAPIWrapper().central);
	SchedJobStdCronImpl impl = new SchedJobStdCronImpl(connFactory);
	impl.init(job);
	
	UserConverter conv = new UserConverter(impl);
	User obj = conv.convert();
	return obj;
}
public List<SchedJob> getSchedJobList(SchedFile file){
	return file.getSchedJobList();
}		
public List<SchedFile> getAllCronFiles() throws UniverseException{
	   ConnectionFactory connFactory = NodeConnectionFactory.getInstance(super.getUVAPIWrapper().central);
       getContext().setImpl(new EnvironmentStdImpl());
       SchedStdCronImplFactory impl = new SchedStdCronImplFactory(connFactory, true,false, false, false);               
       // Operations
       SchedConv schedConv = new SchedConv(getContext(), impl);
       schedConv.isAuthorized();
       schedConv.check();
       schedConv.list();
       schedConv.expand();
       schedConv.preview();
       return schedConv.getFileList();
}
public SchedFile getCronFileForUser(String user) throws UniverseException, IOException{
		   ConnectionFactory connFactory = NodeConnectionFactory.getInstance(super.getUVAPIWrapper().central);
           getContext().setImpl(new EnvironmentStdImpl());
           SchedStdCronImplFactory impl = new SchedStdCronImplFactory(connFactory, true,false, false, false);               
           // Operations
           SchedConv schedConv = new SchedConv(getContext(), impl);
           schedConv.isAuthorized();
           schedConv.check();
           schedConv.list();
           schedConv.expand();
           schedConv.preview();
           for (SchedFile file:schedConv.getFileList()) {
        	   if(file.getOwner().equals(user)){
        		   return file;}
           }
           return null;


	}
}
