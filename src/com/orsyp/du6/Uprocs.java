package com.orsyp.du6;

import com.orsyp.UniverseException;
import com.orsyp.api.Context;
import com.orsyp.api.FunctionalPeriod;
import com.orsyp.api.deploymentpackage.ApiConflictPolicy;
import com.orsyp.api.deploymentpackage.Package;
import com.orsyp.api.deploymentpackage.PackageElement;
import com.orsyp.api.security.Operation;
import com.orsyp.api.syntaxerules.OwlsSyntaxRules;
import com.orsyp.api.uproc.Memorization;
import com.orsyp.api.uproc.Memorization.Type;
import com.orsyp.api.uproc.StatusManagement;
import com.orsyp.api.uproc.Uproc;
import com.orsyp.api.uproc.UprocFilter;
import com.orsyp.api.uproc.UprocId;
import com.orsyp.api.uproc.UprocItem;
import com.orsyp.api.uproc.UprocList;
import com.orsyp.api.uproc.cl.InternalScript;
import com.orsyp.api.uproc.cmd.CmdData;
import com.orsyp.api.uprocclass.UprocClass;
import com.orsyp.api.uprocclass.UprocClassFilter;
import com.orsyp.api.uprocclass.UprocClassId;
import com.orsyp.api.uprocclass.UprocClassItem;
import com.orsyp.api.uprocclass.UprocClassList;
import com.orsyp.owls.impl.uproc.OwlsUprocImpl;
import com.orsyp.owls.impl.uproc.OwlsUprocListImpl;
import com.orsyp.owls.impl.uprocclass.OwlsClassImpl;
import com.orsyp.owls.impl.uprocclass.OwlsClassListImpl;
import com.orsyp.std.deploymentpackage.PackageElementStdImpl;

public class Uprocs extends GenericObjects {
	
	private Context GlobalCtx;

	public Uprocs (com.orsyp.stores.UVAPIWrapper conn, Context GlobalCtx){
		super(conn);
		this.GlobalCtx = GlobalCtx;
	}
	private Context getContext(){
		return GlobalCtx;
	}

	public void addUprocListToPackage(UprocList list, Package pck) throws Exception{
		for(int i=0;i<list.getCount();i++){
			Uproc obj = getUproc(list.get(i));
			PackageElement el = new PackageElement();
			el.setImpl(new PackageElementStdImpl());
			el.enableSyntaxCheck();
			el.setConflictPolicy(ApiConflictPolicy.OVERWRITE);
			el.setUproc(obj);
			pck.addOrUpdatePackageElement(el);
		}
	}
	public UprocList getUprocList() throws Exception {  
		
        UprocFilter filter = new UprocFilter("*");
        UprocList list = new UprocList(getContext(), filter);
        
        list.setSyntaxRules(OwlsSyntaxRules.getInstance());
        OwlsUprocListImpl impl = new OwlsUprocListImpl();
        impl.init(list, Operation.DISPLAYLIST);
        list.setImpl(impl);
       // list.setImpl(new OwlsUprocListImpl());
        list.extract();
        return list;
	}
	public UprocClass getUprocClass(UprocClassItem item) throws Exception{  
		UprocClassId id = item.getIdentifier();
		UprocClass obj = new UprocClass(getContext(),id);
		obj.setImpl(new OwlsClassImpl());
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
	    obj.extract();
	    return obj;
	}
	public Uproc getUproc(UprocItem item) throws Exception{  

		Uproc obj = new Uproc(getContext(),item.getIdentifier());
		
	     OwlsUprocImpl impl = new OwlsUprocImpl();
	     impl.init(obj, Operation.DISPLAY);
	     obj.setImpl(impl);
		 obj.extract();

		//InternalScript is = new InternalScript(obj);
		//System.out.println(is.extractContent()[0]);
		//is.save();
		//obj.update();
	    return obj;
	}
	public Uproc getUprocByName(String name, String version) throws Exception {
		UprocList l = getUprocList();
		for(int i=0;i<l.getCount();i++){
			if(l.get(i).getName().equalsIgnoreCase(name)){
				return getUproc(l.get(i));
			}
		}
		return null;
		
		//UprocId uprocId = new UprocId(name, version);
		//uprocId.setSyntaxRules(OwlsSyntaxRules.getInstance());
		
		//Uproc u = new Uproc(getContext(), uprocId);
		
		//u.setImpl(new OwlsUprocImpl());
		//u.extract();
		//return u;
	}

	public void deleteUProc(String name, String version) throws Exception {
		Context ctx = getContext();
		Uproc obj = new Uproc(ctx, UprocId.create(name, name, version));
		obj.setImpl(new OwlsUprocImpl());
		obj.disableSyntaxCheck();
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
		obj.delete();
	}
	public Uproc duplicateUproc(Uproc originalObj, String newName, String newLabel) throws Exception{  
		UprocId id = new UprocId(newName,originalObj.getVersion());
		id.setSyntaxRules(OwlsSyntaxRules.getInstance());
		originalObj.duplicate(id, newLabel);
		return getUprocByName(newName,originalObj.getVersion());
	}
	
	public UprocClassList getUprocClassList() throws Exception {  
        UprocClassFilter filter = new UprocClassFilter("*");
        UprocClassList list = new UprocClassList(getContext(), filter);
        list.setImpl(new OwlsClassListImpl());
        list.extract();
        return list;
	}
	
	public Uproc createCLINTUproc(String uprocName, String uprocLabel, String uprocVersion, String[] uprocScript) throws UniverseException{

		Uproc obj = new Uproc(getContext(),UprocId.create(uprocName, null, uprocVersion));
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
		obj.setType("CL_INT");
	    obj.setInternal(true);
		OwlsUprocImpl impl = new OwlsUprocImpl();
		impl.init(obj);
		obj.setImpl(impl);
		obj.setLabel(uprocLabel);
		obj.setFunctionalPeriod(FunctionalPeriod.Day);
		obj.setApplication("U_");
		obj.setDomain("I");
		obj.setDefaultInformation("");
	    obj.setDefaultSeverity(0);
	    	Memorization mem = new Memorization();
	    	mem.setType(Type.FULL);
		obj.setMemorization(mem);
	    obj.setEnhancedStatus(false);
	    StatusManagement sm = new StatusManagement();
	    sm.setEnabled(false);
		obj.setStatusManagement(sm);
		// Uproc MUST be created before adding an internal script
		obj.create();
		InternalScript script = new InternalScript(obj);
			uprocScript[0]="echo something";
			script.setBinary(false);
			script.setLines(uprocScript);
			script.setUproc(obj);
			script.save();
			obj.setInternalScript(script);
		obj.update();
		
		return obj;
	}
	public Uproc createCMDUproc(String uprocName, String uprocLabel, String uprocVersion, String uprocCmd) throws UniverseException{
		UprocId id = new UprocId(uprocName,uprocVersion);
		id.setSyntaxRules(OwlsSyntaxRules.getInstance());
		Uproc obj = new Uproc(getContext(),id);
		obj.setImpl(new OwlsUprocImpl());
		obj.setLabel(uprocLabel);
		obj.setFunctionalPeriod(FunctionalPeriod.Day);
		obj.setType("CMD");
        CmdData cmdData = new CmdData();
        cmdData.setCommandLine(uprocCmd);
        obj.setSpecificData(cmdData);
		obj.setApplication("U_");
		obj.setDomain("I");
		obj.setDefaultInformation("");
	    obj.setDefaultSeverity(0);
	    	Memorization mem = new Memorization();
	    	mem.setType(Type.FULL);
		obj.setMemorization(mem);
	    obj.setEnhancedStatus(false);
	    StatusManagement sm = new StatusManagement();
	    sm.setEnabled(false);
		obj.setStatusManagement(sm);
		obj.create();
		return obj;
	}
}
