package com.orsyp.du6;

import com.orsyp.UniverseException;
import com.orsyp.api.Context;
import com.orsyp.api.oex.OEX;
import com.orsyp.api.oex.OEXExceptionSubType;
import com.orsyp.api.oex.OEXFilter;
import com.orsyp.api.oex.OEXId;
import com.orsyp.api.oex.OEXItem;
import com.orsyp.api.oex.OEXList;
import com.orsyp.api.oex.OEXType;
import com.orsyp.api.oex.OEXValidity;
import com.orsyp.api.outage.Outage;
import com.orsyp.api.outage.OutageFilter;
import com.orsyp.api.outage.OutageId;
import com.orsyp.api.outage.OutageItem;
import com.orsyp.api.outage.OutageList;
import com.orsyp.api.syntaxerules.OwlsSyntaxRules;
import com.orsyp.owls.impl.oex.OwlsOEXImpl;
import com.orsyp.owls.impl.oex.OwlsOEXListImpl;
import com.orsyp.owls.impl.outage.OwlsOutageImpl;
import com.orsyp.owls.impl.outage.OwlsOutageListImpl;

public class Exceptions extends GenericObjects  {
	private Context GlobalCtx;

	public Exceptions (com.orsyp.stores.UVAPIWrapper conn, Context GlobalCtx){
		super(conn);
		this.GlobalCtx = GlobalCtx;
	}
	private Context getContext(){
		return GlobalCtx;
	}
	public OEXList getOEXList() throws Exception {  
		OEXFilter filter = new OEXFilter();
		OEXList list = new OEXList(getContext(), filter);
        list.setImpl(new OwlsOEXListImpl());
        list.extract();
        return list;
	}
	public OutageList getOutageList() throws Exception {  
		OutageFilter filter = new OutageFilter();
		OutageList list = new OutageList(getContext(), filter);
        list.setImpl(new OwlsOutageListImpl());
        list.extract();
        return list;
	}
	public OEX getOEX(OEXItem item) throws Exception{  
		OEXId id = item.getIdentifier();
		OEX obj = new OEX(getContext(),id);
		obj.setImpl(new OwlsOEXImpl());
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
	    obj.extract();
	    return obj;
	}
	public Outage getOutage(OutageItem item) throws Exception{  
		OutageId id = item.getIdentifier();
		Outage obj = new Outage(getContext(),id);
		obj.setImpl(new OwlsOutageImpl());
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
	    obj.extract();
	    return obj;
	}
	public OEX createException(OEXType exceptType, OEXExceptionSubType subType, String taskName, String taskVersion, String taskMu, String sessionName, String sessionVersion,
			String uprocName, String uprocVersion, String muName, char muType, int numlanc, int numsess, int numproc, int numordartse) throws UniverseException{
		
		OEXId id = OEXId.create(exceptType, subType, taskName, taskVersion, taskMu, 
				sessionName, sessionVersion, uprocName, uprocVersion, muName, muType, 
				numlanc, numsess, numproc, numordartse);
		id.setSyntaxRules(OwlsSyntaxRules.getInstance());
		OEX obj = new OEX(getContext(),id);
		obj.setImpl(new OwlsOEXImpl());
		
		obj.setValidity(new OEXValidity());
		
	    obj.create();
	    return obj;
	}
	
}
