package com.orsyp.du6;

import java.util.ArrayList;
import java.util.List;

import com.orsyp.UniverseException;
import com.orsyp.api.Context;
import com.orsyp.jobconverter.JobConverter;
import com.orsyp.jobconverter.Ojob;
import com.orsyp.jobconverter.WTSjob;
import com.orsyp.jobconverter.std.JobConverterStdFactory;
import com.orsyp.std.ConnectionFactory;
import com.orsyp.std.EnvironmentStdImpl;
import com.orsyp.std.NodeConnectionFactory;
import com.orsyp.uvms6.GenericObjects;

public class WTSchedules extends GenericObjects{
	private Context GlobalCtx;

	public WTSchedules (com.orsyp.stores.UVAPIWrapper conn, Context GlobalCtx){
		super(conn);
		this.GlobalCtx = GlobalCtx;
	}
	private Context getContext(){
		return GlobalCtx;
	}
	
	public List<WTSjob> getAllWTSJobs() throws UniverseException{
		   ConnectionFactory connFactory = NodeConnectionFactory.getInstance(super.getUVAPIWrapper().central);
	       getContext().setImpl(new EnvironmentStdImpl());
	                   
	       JobConverterStdFactory jobConverterfactory = new JobConverterStdFactory(getContext(), connFactory, false, true);
           JobConverter MyJobConverter = jobConverterfactory.getStdJobConverter();
           MyJobConverter.setJobConverterImplFactory(jobConverterfactory.getJobConverterImplFactory());

           MyJobConverter.extract();

           List<Ojob> oOjobList = MyJobConverter.getOjobList();
           List<WTSjob> WTSjobList = new ArrayList();
           
           for (int nIdx = 0; nIdx < oOjobList.size(); nIdx++) {
               WTSjob oWtsJob = (WTSjob) oOjobList.get(nIdx);
               WTSjobList.add(oWtsJob);
               oWtsJob.print();
              
           }
	       return WTSjobList;
	}
	
}
