package com.orsyp.du6;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Vector;

import com.orsyp.UniverseException;
import com.orsyp.api.Context;
import com.orsyp.api.FunctionalPeriod;
import com.orsyp.api.deploymentpackage.Package;
import com.orsyp.api.rule.UniPattern;
import com.orsyp.api.session.Session;
import com.orsyp.api.syntaxerules.OwlsSyntaxRules;
import com.orsyp.api.task.DetailedSimulation;
import com.orsyp.api.task.LaunchHourPattern;
import com.orsyp.api.task.SimpleLaunch;
import com.orsyp.api.task.Task;
import com.orsyp.api.task.TaskFilter;
import com.orsyp.api.task.TaskId;
import com.orsyp.api.task.TaskImplicitData;
import com.orsyp.api.task.TaskItem;
import com.orsyp.api.task.TaskList;
import com.orsyp.api.task.TaskPlanifiedData;
import com.orsyp.api.task.TaskProvokedData;
import com.orsyp.api.task.TaskType;
import com.orsyp.api.uproc.Uproc;
import com.orsyp.owls.impl.task.OwlsTaskImpl;
import com.orsyp.owls.impl.task.OwlsTaskListImpl;
import com.orsyp.util.DateTools;
import com.orsyp.util.Run;

public class Tasks extends GenericObjects {
	private Context GlobalCtx;

	public Tasks (com.orsyp.stores.UVAPIWrapper conn, Context GlobalCtx){
		super(conn);
		this.GlobalCtx = GlobalCtx;
	}
	private Context getContext(){
		return GlobalCtx;
	}
	public TaskList getTaskList() throws Exception {
		return getTaskListWithFilter(new TaskFilter());
	}
	public TaskList getTaskListWithFilter(TaskFilter tf) throws Exception {
		TaskList list = new TaskList(getContext(), tf);
		list.setImpl(new OwlsTaskListImpl());
		list.extract();
		return list;
	}
	public Task getTask(TaskItem item) throws Exception{  
		TaskId id = item.getIdentifier();
		Task obj = new Task(getContext(),id);
		obj.setImpl(new OwlsTaskImpl());
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
	    obj.extract();
	    return obj;
	}
	
	public Package getAllObjectsFromTask(Task obj,Context ctx) throws Exception{
		com.orsyp.stores.UVAPIWrapper c = super.getUVAPIWrapper();
		Package pck = c.UVMSStore.Packages.initiateDu6ObjectPackage("TEMPORARY", "");

		c.UVMSStore.Packages.addTaskToPackage(obj, pck);
		
		if(obj.getSessionName() != null && obj.getSessionName()!=""){
			// adding the session

			Session session = c.DU6Store.Sessions.getSessionByName(obj.getSessionName(), obj.getSessionVersion());
			c.DU6Store.Sessions.getUprocListFromSession(session);
			c.UVMSStore.Packages.addSessionToPackage(session,pck);
			ArrayList<Uproc> arUpr = c.DU6Store.Sessions.getUprocListFromSession(session);
			// adding the Uprocs from session
			for(int i=0;i<arUpr.size();i++){
				c.UVMSStore.Packages.addUprocToPackage(arUpr.get(i),pck);
			}
			
		}else{
			// adding just the Uproc
			c.UVMSStore.Packages.addUprocToPackage(
					
					c.DU6Store.Uprocs.getUprocByName(obj.getUprocName(), obj.getUprocVersion()),
					pck);
		}
		c.UVMSStore.Packages.addMuToPackage(
				c.DU6Store.Mus.getMuByName(obj.getMuName()),
				pck);
		c.UVMSStore.Packages.addUserToPackage(
				c.DU6Store.Users.getUserByName(obj.getUserName()),
				pck);
		c.UVMSStore.Packages.addDqmQueueToPackage(
				c.DU6Store.DqmQueues.getDqmQueueByName(obj.getQueue()),
				pck);
		return pck;
	}
	
	public Task getTaskById(TaskId id) throws Exception{  
		Task obj = new Task(getContext(),id);
		obj.setImpl(new OwlsTaskImpl());
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
	    obj.extract();
	    return obj;
	}
	public Task getTaskByName(String name,boolean isTemplate) throws Exception {
		TaskList list = getTaskList();
		for(int i=0;i<list.getCount();i++){
			TaskItem ent = list.get(i);
			ent.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
			if(isTemplate){
				if(ent.getIdentifier().getName().equalsIgnoreCase(name) && ent.isTemplate()){
					TaskId id = ent.getIdentifier();
					Task u = new Task(getContext(), id);
					u.setImpl(new OwlsTaskImpl());
					u.extract();
					return u;
				}
			}else{
				if(ent.getIdentifier().getName().equalsIgnoreCase(name) && !ent.isTemplate()){
					TaskId id = ent.getIdentifier();
					Task u = new Task(getContext(), id);
					u.setImpl(new OwlsTaskImpl());
					u.extract();
					return u;
			}
		}
		}
		return null;
	}
	public void deleteTask(String name, String muName, String version) throws Exception {
		Context ctx = getContext();
		Task obj = new Task(ctx, TaskId.createWithName(name, version, muName, true));
		obj.setImpl(new OwlsTaskImpl());
		obj.disableSyntaxCheck();
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
		obj.delete();
	}
	public Vector<Run> getRunsWithinWindow(Task obj, String startdate, String starttime, String enddate, String endtime) throws Exception{  
		Date tmpD=DateTools.toDate(startdate);
		Date tmpE=DateTools.toDate(enddate);
		
		// we add minutes and hours based on what is passed by parameters
		Date tmpDFULL=DateTools.toDate(startdate+starttime);
		Date tmpEFULL=DateTools.toDate(enddate+endtime);
		
		DetailedSimulation simDetails = obj.simulateFull(tmpDFULL, tmpEFULL);
		Vector<Run> runVector = new Vector(Arrays.asList(simDetails.getRuns()));
		
		return runVector;
	}

	public Task createTaskprovokedWithUproc(String taskName, String muName, String uprocName, String uprocVersion, String userName, boolean template) throws Exception{
		return createTaskProvokedWithSession(taskName,muName,"", "", uprocName, uprocVersion, userName, template);
	}
	public Task createTaskProvokedWithSession(String taskName, String muName, String sessionName, String sessionVersion, String uprocName, String uprocVersion, String userName, boolean template) throws Exception{  
		String taskVersion="000";
		Task obj = new Task(getContext(), TaskId.createWithName(taskName, taskVersion, muName, true));
        obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance ());
	    obj.getIdentifier().setSessionName(sessionName);
	    obj.getIdentifier().setSessionVersion(sessionVersion);
        obj.getIdentifier().setUprocName(uprocName);
        obj.getIdentifier().setUprocVersion(uprocVersion);
        obj.setUserName(userName);        
        obj.setMuName(muName);      
        obj.setFunctionalPeriod(FunctionalPeriod.Day);
	    obj.setTaskType(TaskType.Provoked);
	    obj.setActive(true);
        obj.setTypeDayOffset(com.orsyp.api.task.DayType.CALENDAR);                
        obj.setQueue("SYS_BATCH");
        obj.setPriority("100");
        obj.setPrinter("PRN");
        obj.setValidFrom(DateTools.toDate("20130101", "000000"));
        TaskProvokedData dt=  new TaskProvokedData ();
	    dt.setStartLaunchTime("999999");
	    dt.setNbHourElaps("999");
	    dt.setNbMinuteElaps("59");
	    dt.setStartExcludTime("000000");
	    dt.setEndExcludTime("000000");
	    obj.setSpecificData (dt);
        obj.setImpl(new OwlsTaskImpl());
        obj.create();
		return obj;
	}
	public Task createTaskScheduledWithUproc(String taskName, String ruleName, String muName, String uprocName, String uprocVersion, String userName, boolean template) throws Exception{ 
		return createTaskScheduledWithSession(taskName, ruleName, muName, "", "", uprocName, uprocVersion, userName, template);
	}
	public Task createTaskScheduledWithSession(String taskName, String ruleName, String muName, String sessionName, String sessionVersion, String uprocName, String uprocVersion, String userName, boolean template) throws Exception{  
		String taskVersion="000";
		Task obj = new Task(getContext(), TaskId.createWithName(taskName, taskVersion, muName, true));
        obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance ());
	    obj.getIdentifier().setSessionName(sessionName);
	    obj.getIdentifier().setSessionVersion(sessionVersion);
        obj.getIdentifier().setUprocName(uprocName);
        obj.getIdentifier().setUprocVersion(uprocVersion);
        obj.setUserName(userName);        
        obj.setMuName(muName);      
        obj.setFunctionalPeriod(FunctionalPeriod.Day);
	    obj.setTaskType(TaskType.Scheduled);
	    obj.setActive(true);
        obj.setTypeDayOffset(com.orsyp.api.task.DayType.CALENDAR);                
        obj.setQueue("SYS_BATCH");
        obj.setPriority("100");
        obj.setPrinter("PRN");
        obj.setValidFrom(DateTools.toDate("20130101", "000000"));
        
        TaskPlanifiedData dt = new TaskPlanifiedData();
        ArrayList<SimpleLaunch> SLTable = new ArrayList<SimpleLaunch>();
        	SimpleLaunch sl = new SimpleLaunch();
        	sl.setHoursCount("00");
        	sl.setMinutesCount("00");
        	sl.setMinutesCount("00");
        	sl.setLaunchTime("0000");
        	SLTable.add(sl);
        	SimpleLaunch[] sllist = new SimpleLaunch[SLTable.size()];
        	SLTable.toArray(sllist);
        dt.setSimpleLaunches(sllist);
        
        ArrayList<TaskImplicitData> TITable = new ArrayList<TaskImplicitData>();
        	TaskImplicitData tid = new TaskImplicitData(ruleName);
        	TITable.add(tid);
        	TaskImplicitData[] tilist = new TaskImplicitData[TITable.size()];
        dt.setImplicitData(tilist);

	    obj.setSpecificData (dt);
        obj.setImpl(new OwlsTaskImpl());
        obj.create();
		return obj;
	}
	public void showTaskSchedule(Task t) throws UniverseException{
		String taskName = t.getIdentifier().getName();
		String muName = t.getIdentifier().getMuName();
		String uprocName = t.getIdentifier().getUprocName();
		String sessionName = t.getIdentifier().getSessionName();
		char funcPeriod = t.getFunctionalPeriod().getUnit();
		char taskType = t.getTaskType().getCode();
		String typeDayOff = t.getTypeDayOffset().name();
		String queueName = t.getQueue();
		
		TaskPlanifiedData tpd = t.getPlanifiedData();
		TaskImplicitData[] tidTable = tpd.getImplicitData();
		LaunchHourPattern[] lhpTable = tpd.getLaunchHourPatterns();
		SimpleLaunch[] slTable = tpd.getSimpleLaunches();
		System.out.println("-----------------------------------");
		System.out.println("Task Name: "+taskName);
		System.out.println("Mu Name: "+muName);
		System.out.println("Uproc Name: "+uprocName);
		System.out.println("Session Name: "+sessionName);
		System.out.println("Func Period: "+ funcPeriod);
		System.out.println("Task Type: "+taskType);
		System.out.println("Day Offset Type: "+typeDayOff);
		System.out.println("Queue Name:" + queueName);
		System.out.println("Number of Rules: "+tidTable.length);
		System.out.println("Number of Launch Patterns: "+lhpTable.length);
		System.out.println("Number of Simple Launch: "+slTable.length);
		
		for(int j=0;j<lhpTable.length;j++){
			LaunchHourPattern lhp = lhpTable[j];
			String durHour = String.valueOf(lhp.getDurationHour());
			String durMin = String.valueOf(lhp.getDurationMinute());
			String durSec = String.valueOf(lhp.getDurationSecond());
			String endTime = String.valueOf(lhp.getEndTime());
			String freq = String.valueOf(lhp.getFrequency());
			String startTime = lhp.getStartTime();
			System.out.println("lh Time: "+durHour+":"+durMin+":"+durSec);
			System.out.println("End Time: " + endTime);
			System.out.println("Frequency: "+freq);
			System.out.println("Start Time: " + startTime);
		}
		
		for(int i=0;i<tidTable.length;i++){
			TaskImplicitData tid = tidTable[i];
			String ruleName = tid.getName();
			String perNumber = String.valueOf(tid.getPeriodNumber());
			String perType = tid.getPeriodType().toString();
			
			System.out.println("Rule Name: "+ruleName);
			System.out.println("Period Number: "+perNumber);
			System.out.println("Period Type: "+perType);
			
			UniPattern uniPat = tid.getUniversePattern();
			String posDirection = uniPat.getPositionDirection().name();
			String dayType = uniPat.getDayType().toString();
			String posPeriod = String.valueOf(uniPat.getPositionInPeriod());
			String possPeriod = uniPat.getPositionsInPeriod().getPositionsPattern();
			
			System.out.println("Pattern Position Direction: "+posDirection);
			System.out.println("Pattern day type: "+dayType);
			System.out.println("Pattern Position in Period: " + possPeriod);
			
			t.setImpl(new OwlsTaskImpl());
			t.setTaskType(TaskType.Scheduled);
			t.update();
			
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	
	}
	
}
