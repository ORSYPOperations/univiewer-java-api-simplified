package com.orsyp.du6;

import com.orsyp.api.Context;
import com.orsyp.api.mu.Mu;
import com.orsyp.api.mu.MuDependency;
import com.orsyp.api.mu.MuDependencyFilter;
import com.orsyp.api.mu.MuDependencyId;
import com.orsyp.api.mu.MuDependencyItem;
import com.orsyp.api.mu.MuDependencyList;
import com.orsyp.api.mu.MuFilter;
import com.orsyp.api.mu.MuId;
import com.orsyp.api.mu.MuItem;
import com.orsyp.api.mu.MuList;
import com.orsyp.api.mu.TargetVariable;
import com.orsyp.api.mu.TargetVariableId;
import com.orsyp.api.syntaxerules.OwlsSyntaxRules;
import com.orsyp.owls.impl.mu.OwlsMuDepImpl;
import com.orsyp.owls.impl.mu.OwlsMuDepListImpl;
import com.orsyp.owls.impl.mu.OwlsMuImpl;
import com.orsyp.owls.impl.mu.OwlsMuListImpl;
import com.orsyp.std.TargetVariableStdImpl;

public class Mus  extends GenericObjects{
	private Context GlobalCtx;

	public Mus (com.orsyp.stores.UVAPIWrapper conn, Context GlobalCtx){
		super(conn);
		this.GlobalCtx = GlobalCtx;
	}
	private Context getContext(){
		return GlobalCtx;
	}
	
	public void createMUVariable(String mu, String varName, String value) throws Exception {
		TargetVariable targetVariable = new TargetVariable(getContext(), new TargetVariableId(mu, null, varName));
		targetVariable.setVariableValue(value);
		targetVariable.setImpl(new TargetVariableStdImpl());
		targetVariable.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());		
		targetVariable.create();
	}
	public MuList getMuList() throws Exception {  
        MuFilter filter = new MuFilter("*","*");
        MuList list = new MuList(getContext(), filter);
        list.setImpl(new OwlsMuListImpl());
        list.extract();
        return list;
	}
	public MuDependencyList getMuDependencyList() throws Exception {  
        MuDependencyFilter filter = new MuDependencyFilter("*","*");
        MuDependencyList list = new MuDependencyList(getContext(), filter);
        list.setImpl(new OwlsMuDepListImpl());
        list.extract();
        return list;
	}
	public Mu getMu(MuItem item) throws Exception{  
		MuId id = item.getIdentifier();
		Mu obj = new Mu(getContext(),id);
		obj.setImpl(new OwlsMuImpl());
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
	    obj.extract();
	    return obj;
	}
	public Mu getMuByName(String name) throws Exception {
		MuId id = MuId.create(name);
		id.setSyntaxRules(OwlsSyntaxRules.getInstance());
		Mu u = new Mu(getContext(), id);
		u.setImpl(new OwlsMuImpl());
		//u.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
		u.extract();
		return u;
	}
	public MuDependency getMuDependency(MuDependencyItem item) throws Exception{  
		MuDependencyId id = item.getIdentifier();
		MuDependency obj = new MuDependency(getContext(),id);
		obj.setImpl(new OwlsMuDepImpl());
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
	    obj.extract();
	    return obj;
	}
	public void deleteMu(String muName) throws Exception {
		Mu obj = new Mu(getContext(), muName);
		obj.setImpl(new OwlsMuImpl());
		obj.delete();
	}
	public Mu createMu(String muName, String muLabel, String defaultUser) throws Exception {
		Mu obj = new Mu(getContext(), muName);
		obj.setImpl(new OwlsMuImpl());
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
		obj.setLabel(muLabel);
		obj.setDevelopmentAuth(true);
		obj.setProductionAuth(true);
		obj.setNodeName(getContext().getEnvironment().getNodeName());
		obj.setUserName(defaultUser);
		obj.create();
		return obj;
	}
}



















