package com.orsyp.du6;

import com.orsyp.api.Context;
import com.orsyp.api.dqm.DqmQueue;
import com.orsyp.api.dqm.DqmQueueFilter;
import com.orsyp.api.dqm.DqmQueueId;
import com.orsyp.api.dqm.DqmQueueItem;
import com.orsyp.api.dqm.DqmQueueList;
import com.orsyp.api.dqm.DqmQueueStatus;
import com.orsyp.api.dqm.DqmQueueType;
import com.orsyp.api.syntaxerules.OwlsSyntaxRules;
import com.orsyp.owls.impl.dqm.OwlsDqmQueueImpl;
import com.orsyp.owls.impl.dqm.OwlsDqmQueueListImpl;

public class DqmQueues extends GenericObjects  {
	private Context GlobalCtx;

	public DqmQueues (com.orsyp.stores.UVAPIWrapper conn, Context GlobalCtx){
		super(conn);
		this.GlobalCtx = GlobalCtx;
	}
	private Context getContext(){
		return GlobalCtx;
	}
	public DqmQueueList getDqmQueueList() throws Exception {  
		DqmQueueFilter filter = new DqmQueueFilter();
		DqmQueueList list = new DqmQueueList(getContext(), filter,true,"");
        list.setImpl(new OwlsDqmQueueListImpl());
        list.extract();
        return list;
	}
	public DqmQueue getDqmQueue(DqmQueueItem item) throws Exception{  
		DqmQueueId id = item.getIdentifier();
		DqmQueue obj = new DqmQueue(getContext(),id);
		obj.setImpl(new OwlsDqmQueueImpl());
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
	    obj.extract();
	    return obj;
	}	
	public DqmQueue getDqmQueueByName(String name) throws Exception {
		DqmQueueId id = DqmQueueId.create(name);
		DqmQueue u = new DqmQueue(getContext(), id);
		u.setImpl(new OwlsDqmQueueImpl());
		u.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
		u.extract();
		return u;
	}
	public void createDqmQueue(String name, String label) throws Exception {
        DqmQueue obj = new DqmQueue(getContext(), DqmQueueId.create(name));
        obj.setStatus(DqmQueueStatus.STARTED);
        obj.setType(DqmQueueType.PHYSICAL);
        obj.setJobLimit(99999);
        obj.setImpl(new  OwlsDqmQueueImpl());
        obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
        obj.create();
	}
}
