package com.orsyp.uvms6;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.orsyp.kmeleon.document.upgrademasswizard.util.MultipleActionProgressionContext;
import com.orsyp.kmeleon.util.Pair;
import com.orsyp.owls.OwlsImplFactory;
import com.orsyp.Area;
import com.orsyp.UniverseException;
import com.orsyp.api.Context;
import com.orsyp.api.ImplFactory;
import com.orsyp.api.NoImplementationException;
import com.orsyp.api.application.Application;
import com.orsyp.api.bv.BusinessView;
import com.orsyp.api.calendar.Calendar;
import com.orsyp.api.central.networknode.NetworkNodeId.PRODUCT_TYPE;
import com.orsyp.api.deploymentpackage.ApiConflictPolicy;
import com.orsyp.api.deploymentpackage.ApiOperationCode;
import com.orsyp.api.deploymentpackage.ApiPackageElementType;
import com.orsyp.api.deploymentpackage.ApiPackageType;
import com.orsyp.api.deploymentpackage.Package;
import com.orsyp.api.deploymentpackage.PackageElement;
import com.orsyp.api.deploymentpackage.PackageId;
import com.orsyp.api.deploymentpackage.PackageItem;
import com.orsyp.api.deploymentpackage.PackageList;
import com.orsyp.api.domain.Domain;
import com.orsyp.api.dqm.DqmQueue;
import com.orsyp.api.job.Job;
import com.orsyp.api.mu.Mu;
import com.orsyp.api.mu.MuDependency;
import com.orsyp.api.oex.OEX;
import com.orsyp.api.outage.Outage;
import com.orsyp.api.resource.Resource;
import com.orsyp.api.rule.Rule;
import com.orsyp.api.runbook.Runbook;
import com.orsyp.api.security.Operation;
import com.orsyp.api.session.Session;
import com.orsyp.api.syntaxerules.ClassicSyntaxRules;
import com.orsyp.api.syntaxerules.OwlsSyntaxRules;
import com.orsyp.api.syntaxerules.UnijobSyntaxRules;
import com.orsyp.api.task.Task;
import com.orsyp.api.trigger.Trigger;
import com.orsyp.api.upgrade5to6.ApiUpgradeHelper;
import com.orsyp.api.upgrade5to6.DesignObjectsUpgrade;
import com.orsyp.api.upgrade5to6.ProgressionContext;
import com.orsyp.api.uproc.Uproc;
import com.orsyp.api.uproc.cl.InternalScript;
import com.orsyp.api.uprocclass.UprocClass;
import com.orsyp.api.user.User;

import com.orsyp.central.jpa.jpo.NetworkNodeEntity;
import com.orsyp.comm.client.ClientServiceLocator;
import com.orsyp.du6.GenericObjects;
import com.orsyp.std.StdImplFactory;
import com.orsyp.std.UprocStdImpl;
import com.orsyp.std.deploymentpackage.PackageElementStdImpl;
import com.orsyp.std.deploymentpackage.PackageListStdImpl;
import com.orsyp.std.deploymentpackage.PackageStdImpl;
import com.orsyp.std.nfile.LocalBinaryFile;
import java.util.Collection;
import java.util.Map;

public class Packages  extends GenericObjects {
	private Context GlobalCtx;

	public Packages (com.orsyp.stores.UVAPIWrapper conn, Context GlobalCtx){
		super(conn);
		this.GlobalCtx = GlobalCtx;
	}
	private Context getContext(){
		return GlobalCtx;
	}
	public Package getDu6Package(PackageItem item) throws Exception{  
		PackageId id = item.getIdentifier();
		Package obj = new Package(id);
		obj.setContext(getContext());
		obj.setImpl(new PackageStdImpl());
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
	    obj.extract();
	    return obj;
	}
	public Package getDu6PackageFromName(String pckName) throws Exception{  
		PackageList pcklist = getDu6PackageList();
	    for(int i=0;i<pcklist.getCount();i++){
	    	if(pcklist.get(i).getName().equalsIgnoreCase(pckName)){
	    		return getDu6Package(pcklist.get(i));
	    	}
	    }
	    return null;
	}
	public Package getUJPackage(PackageItem item) throws Exception{  
		PackageId id = item.getIdentifier();
		Package obj = new Package(id);
		obj.setContext(getContext());
		obj.setImpl(new PackageStdImpl());
		obj.getIdentifier().setSyntaxRules(UnijobSyntaxRules.getInstance());
	    obj.extract();
	    return obj;
	}
	public void deleteDu6Package(PackageItem item) throws Exception{
		PackageId pckID = item.getIdentifier();
		pckID.setSyntaxRules(OwlsSyntaxRules.getInstance());
		com.orsyp.api.deploymentpackage.Package myPck = new com.orsyp.api.deploymentpackage.Package(pckID);
		myPck.setImpl(new PackageStdImpl());
		myPck.setContext(getContext());
		myPck.extract();
		myPck.delete();
	}
	public PackageList getDu6PackageList() throws Exception {
		PackageList list = new PackageList(getContext(), PRODUCT_TYPE.DU_OWLS);
		// Packages are a central Object. The context needs to be the context of UVMS
		list.setContext(getContext());
		list.setImpl(new PackageListStdImpl());
		list.extract(Operation.DISPLAY);
		return list;
	}
	public PackageList getUJPackageList() throws Exception {
		PackageList list = new PackageList(getContext(), PRODUCT_TYPE.UNIJOB);
		// Packages are a central Object. The context needs to be the context of UVMS
		list.setContext(getContext());
		list.setImpl(new PackageListStdImpl());
		list.extract(Operation.DISPLAY);
		return list;
	}
	// Package Initiation Methods
	public Package initiateUnijobPackage(String name, String comment){
		PackageId id = new PackageId(name,PRODUCT_TYPE.UNIJOB,ApiPackageType.UNIJOB_OBJECTS);
		id.setSyntaxRules(UnijobSyntaxRules.getInstance());
		Package obj = new Package(id);
		obj.setComment(comment);
		obj.setImpl(new PackageStdImpl());
		obj.setContext(getContext());
		return obj;
	}
	public Package initiateDuAdminPackage(String name, String comment){
		PackageId id = new PackageId(name,PRODUCT_TYPE.DU_OWLS,ApiPackageType.NODE_SETTINGS);
		id.setSyntaxRules(OwlsSyntaxRules.getInstance());
		Package obj = new Package(id);
		obj.setComment(comment);
		obj.setImpl(new PackageStdImpl());
		obj.setContext(getContext());
		return obj;
	}
	public Package initiateDu6ObjectPackage(String name, String comment){
		PackageId id = new PackageId(name,PRODUCT_TYPE.DU_OWLS,ApiPackageType.DESIGN_OBJECTS);
		id.setSyntaxRules(OwlsSyntaxRules.getInstance());
		
		Package obj = new Package(id);
		obj.setImpl(new PackageStdImpl());
		
		obj.setComment(comment);
		obj.setContext(getContext());
		//obj.setLastModificationOrigin("file");
		return obj;
	}
	public Package initiateDu5ObjectPackage(String name, String comment){
		PackageId id = new PackageId(name,PRODUCT_TYPE.DU_OWLS,ApiPackageType.DESIGN_OBJECTS);
		id.setSyntaxRules(OwlsSyntaxRules.getInstance());
		
		Package obj = new Package(id);
		obj.setImpl(new PackageStdImpl());
		
		obj.setComment(comment);
	//	obj.setLastModificationOrigin("file");
		obj.setContext(getContext());
		return obj;
	}

	private PackageElement getNewPckElement(){
		PackageElement el = new PackageElement();
		el.setImpl(new PackageElementStdImpl());
		el.enableSyntaxCheck();
		return el;
	}
	public void commitModificationsToPackage(Package pck) throws UniverseException{
		Date mydate = new Date();
		pck.setLastModificationDate(mydate);
		
		pck.save(mydate);
	}
	public void commitModificationsToPackageFromDu5ToDu6(Package pck) throws UniverseException{
		Date mydate = new Date();
		pck.setLastModificationDate(mydate);
		System.out.println("DEBUG:"+pck.getName()+":"+pck.getProduct()+":"+pck.getType());
		pck.save(mydate);
	}
	public void exportPackageToFile(Package pck, String File) throws UniverseException{
		// Typically: "c:\\temp\\"+nodename+"_Objects"+".unipkg"
		LocalBinaryFile myFile = new LocalBinaryFile(File);
		pck.exportPackage(myFile);
	}
	// Package Add Element Methods
	public void addDqmQueueToPackage(DqmQueue obj, Package pck) throws UniverseException{	
		PackageElement p = getNewPckElement();
		p.setDqmQueue(obj);
		pck.addOrUpdatePackageElement(p);
	}
	
	public void addBusinessViewToPackage(BusinessView obj, Package pck) throws UniverseException{	
		PackageElement p = getNewPckElement();
		p.setBusinessView(obj);
		
		pck.addOrUpdatePackageElement(p);
	}
	public void addResourceToPackage(Resource obj, Package pck) throws UniverseException{	
		PackageElement p = getNewPckElement();
		p.setResource(obj);
		pck.addOrUpdatePackageElement(p);
	}
	public void addUprocClassToPackage(UprocClass obj, Package pck) throws UniverseException{	
		PackageElement p = getNewPckElement();
		p.setUprocClass(obj);
		pck.addOrUpdatePackageElement(p);
	}
	public void addDomainToPackage(Domain obj, Package pck) throws UniverseException{	
		PackageElement p = getNewPckElement();
		p.setDomain(obj);
		pck.addOrUpdatePackageElement(p);
	}
	public void addApplicationToPackage(Application obj, Package pck) throws UniverseException{	
		PackageElement p = getNewPckElement();
		p.setApplication(obj);
		pck.addOrUpdatePackageElement(p);
	}
	public void addRuleToPackage(Rule obj, Package pck) throws UniverseException{	
		PackageElement p = getNewPckElement();
		p.setImpl(new PackageElementStdImpl());
		p.setConflictPolicy(ApiConflictPolicy.OVERWRITE);
		p.setContext(getContext());
		p.setProduct(PRODUCT_TYPE.DU_OWLS);
		p.setType(ApiPackageElementType.RULE);
		p.setRule(obj);
		pck.addOrUpdatePackageElement(p);
	}

	public void addUprocToPackage(Uproc obj, Package pck) throws UniverseException{	
		PackageElement p = getNewPckElement();
		
		InternalScript is = new InternalScript(obj);
		
		String FilePath = "c:\\temp\\"+System.currentTimeMillis()+"."+obj.getName().replaceAll(" ", "");
		File TempFile = new File(FilePath);
		is.extractContent(TempFile);
		System.out.println("Extracting script..: "+obj.getName()+":"+is.extractContent().length);
		List<String> flist = new ArrayList<String>();
		flist.add(TempFile.getAbsolutePath());
		p.setFileList(flist);
		p.setUproc(obj);
		
		pck.addOrUpdatePackageElement(p);
	}
	public void addUprocv5ToPackage(Uproc obj, Package pck) throws UniverseException{	
		PackageElement p = getNewPckElement();
		//p.setImpl(new PackageElementStdImpl());
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
		p.setUproc(obj);
		
		pck.addOrUpdatePackageElement(p);
	}
	public void addSessionToPackage(Session obj, Package pck) throws UniverseException{	
		PackageElement p = getNewPckElement();
		obj.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());
		
		p.setSession(obj);
		pck.addOrUpdatePackageElement(p);
	}
	public void addTaskToPackage(Task obj, Package pck) throws UniverseException{	
		PackageElement p = getNewPckElement();
		p.setTask(obj);
		pck.addOrUpdatePackageElement(p);
	}
	public void addRunbookToPackage(Runbook obj, Package pck) throws UniverseException{	
		PackageElement p = getNewPckElement();
		p.setRunbook(obj);
		pck.addOrUpdatePackageElement(p);
	}
	public void addMuToPackage(Mu obj, Package pck) throws UniverseException{	
		PackageElement p = getNewPckElement();
		p.setMu(obj);
		pck.addOrUpdatePackageElement(p);
	}
	public void addMuDependencyToPackage(MuDependency obj, Package pck) throws UniverseException{	
		PackageElement p = getNewPckElement();
		p.setMuDependency(obj);
		pck.addOrUpdatePackageElement(p);
	}
	public void addUserToPackage(User obj, Package pck) throws UniverseException{	
		PackageElement p = getNewPckElement();
		p.setUser(obj);
		pck.addOrUpdatePackageElement(p);
	}
	public void addCalendarToPackage(Calendar obj, Package pck) throws UniverseException{	
		PackageElement p = getNewPckElement();
		p.setCalendar(obj);
		pck.addOrUpdatePackageElement(p);
	}
	public void addQueueToPackage(DqmQueue obj, Package pck) throws UniverseException{	
		PackageElement p = getNewPckElement();
		p.setDqmQueue(obj);
		pck.addOrUpdatePackageElement(p);
	}
	public void addExceptionToPackage(OEX obj, Package pck) throws UniverseException{	
		PackageElement p = getNewPckElement();
		p.setOEX(obj);
		pck.addOrUpdatePackageElement(p);
	}
	public void addOutageToPackage(Outage obj, Package pck) throws UniverseException{	
		PackageElement p = getNewPckElement();
		p.setOutage(obj);
		pck.addOrUpdatePackageElement(p);
	}
	public void addTriggerToPackage(Trigger obj, Package pck) throws UniverseException{	
		PackageElement p = getNewPckElement();
		p.setTrigger(obj);
		pck.addOrUpdatePackageElement(p);
	}
	// UJ Package Methods
	public void addUJTargetToPackage(Mu obj, Package pck) throws UniverseException{	
		// the implementation for Targets (Unijob is identical to Mus)
		this.addMuToPackage(obj, pck);
	}
	public void addUJJobToPackage(Job obj, Package pck) throws UniverseException{	
		
		PackageElement p = getNewPckElement();

		p.setJob(obj);
		pck.addOrUpdatePackageElement(p);
	}
	public void addUJCalendarToPackage(Calendar obj, Package pck) throws UniverseException{	
		if (obj.getTarget() != null && !obj.getTarget().isEmpty()) {
		addCalendarToPackage(obj, pck);
		}
		else{
			System.out.println(" --- WARNING: General Calendar cannot be exported! ignoring.. ");
		}
	}
	public void addUJUserToPackage(User obj, Package pck) throws UniverseException{	
		addUserToPackage(obj,pck);
	}
	public void test(Uproc u){
		ApiUpgradeHelper helper = new ApiUpgradeHelper();
		DesignObjectsUpgrade upg = new DesignObjectsUpgrade();
		//Map<Area, Context> duas5Context, ImplFactory duas5Factory, Map<Area, Context> duas6Context, ImplFactory duas6Factory, Collection<Pair<UniverseException, Context>> errors, ProgressionContext progression)
		
		ImplFactory duas5Factory = new StdImplFactory (); 
		ImplFactory duas6Factory = new OwlsImplFactory (); 
		Map<Area, Context> duas5Context = new HashMap<Area, Context>();
		Map<Area, Context> duas6Context = new HashMap<Area, Context>();
		Collection<Pair<UniverseException, Context>> errors = new ArrayList<Pair<UniverseException, Context>>();
		ProgressionContext progression;// = new ProgressionContext();
		
		final MultipleActionProgressionContext mainProgression = new MultipleActionProgressionContext (){

			@Override
			public boolean isCancelled() {
				// TODO Auto-generated method stub
				return false;
			};
			
		};
		
		upg.upgradeUprocs(duas5Context, duas5Factory, duas6Context, duas6Factory, errors, mainProgression);
		
		
	}
	public void importPackageFromFile(File f,ApiPackageType type) throws UniverseException{
		PackageId id = PackageId.createEmptyPackageId();
		id.setPackageName(f.getName());
		id.setPackageType(type);
		id.setSyntaxRules(UnijobSyntaxRules.getInstance());
		Package importPck = new Package(id);
		importPck.setImpl(new PackageStdImpl());
		
		InputStream in = getClass().getResourceAsStream("/com/orsyp/UJ_CRONMIGKIT.unipkg");
		
		//File f=new File("c:/tmp/tmp.pckage");
		//  OutputStream out=new FileOutputStream(f);
		//  byte buf[]=new byte[1024];
		//  int len;
		//  while((len=in.read(buf))>0)
		//  out.write(buf,0,len);
		// out.close();
	  //   in.close();
		importPck.importPackage(f);
		importPck.save(new Date());
	}
	public ApiOperationCode deployUnijobPackage(Package pck,String nodeName) throws UniverseException{
		pck.deploy(nodeName, true);
		return pck.getOperationStatus();
			
	}
	public boolean deployPackage(Package pkg, NetworkNodeEntity node, Area area) throws NoImplementationException, UniverseException{
		//Context ctx = pkg.getContext();
		//PackageService psm = new PackageService();
		//ServiceLocator sl = new ServiceLocator();
		ClientServiceLocator.setContext(getContext());
		//ClientServiceLocator.g
		//ServiceLocator.getAlertConfigurationService().getAlerts();
		//List<PackageEntity> lpe = ServiceLocator.getPackageService().findAll();
		//System.out.println("DEBUG LPE:"+lpe.size());
		return true; // pkg.deploy(node.getCompany(), node.getName(), area, true);
		
		//ClientServiceLocator.setContext(getContext());
		//ClientServiceLocator.getNetworkNodeService().
		//PackageEntity pck = new PackageEntity();
		
		//ServiceLocator.getPackageService();
		
		//for(int i=0;i<l.size();i++){
		//	PackageEntity p = l.get(i);
		//	System.out.println("DEBUG: "+l.get(i).getName());
		//}
		

		//UserEntity ValidUserEntToAdd = ClientServiceLocator.
		
		//boolean r = pkg.deploy(node.getCompany(), node.getName(), Area.Exploitation, true);
		//return r;
	}

}
