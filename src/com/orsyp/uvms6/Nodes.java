package com.orsyp.uvms6;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.orsyp.UniverseException;
import com.orsyp.api.Context;
import com.orsyp.api.central.networknode.NetworkNode;
import com.orsyp.api.central.networknode.NetworkNodeId;
import com.orsyp.api.central.networknode.NetworkNodeId.PRODUCT_TYPE;
import com.orsyp.api.config.MonitoringConfiguration;
import com.orsyp.api.syntaxerules.ClassicSyntaxRules;
import com.orsyp.api.syntaxerules.OwlsSyntaxRules;
import com.orsyp.api.syntaxerules.UnijobSyntaxRules;
import com.orsyp.central.jpa.jpo.AreaEntity;
import com.orsyp.central.jpa.jpo.NetworkNodeEntity;
import com.orsyp.central.jpa.jpo.NodeRefEntity;
import com.orsyp.central.jpa.jpo.NodeTagEntity;
import com.orsyp.comm.client.ClientServiceLocator;
import com.orsyp.du6.GenericObjects;
import com.orsyp.std.central.network.NetworkNodeStdImpl;

public class Nodes  extends GenericObjects {
	private Context GlobalCtx;

	public Nodes (com.orsyp.stores.UVAPIWrapper conn, Context GlobalCtx){
		super(conn);
		this.GlobalCtx = GlobalCtx;
	}
	private Context getContext(){
		return GlobalCtx;
	}
	
	public boolean isAreaActive(char areaChar, NetworkNodeEntity ent ){
		ArrayList<Character> activeAreas = getActiveAreas(ent);
		if(activeAreas.contains(areaChar)){return true;}
		return false;
	}
	public boolean isTagOnNode(String tagName,NetworkNodeEntity ent){
		Set<NodeTagEntity> tags = ent.getTags();
		Iterator<NodeTagEntity> it = tags.iterator();
		
		while(it.hasNext()){
			NodeTagEntity ae = it.next();
			if(ae.getValue().equalsIgnoreCase(tagName)){return true;}
		}
		return false;
	}
	public MonitoringConfiguration getNodeview(String nodeViewName){

		MonitoringConfiguration lmc = ClientServiceLocator.getNodeViewService().findByName(nodeViewName);
		return lmc;
	}
	
	public ArrayList<Character> getActiveAreas(NetworkNodeEntity ent){
		ArrayList<Character> activeAreas = new ArrayList<Character>();
		Set<AreaEntity> areas = ent.getAreas();
		Iterator<AreaEntity> it = areas.iterator();
		while(it.hasNext()){
			AreaEntity ae = it.next();
			activeAreas.add(ae.getName().charAt(0));
		}
		return activeAreas;
	}
	public int getNodeStatus(NetworkNodeEntity ent,PRODUCT_TYPE product) throws UniverseException{
		
		NetworkNodeId id = new NetworkNodeId(PRODUCT_TYPE.DU_OWLS,ent.getName(),ent.getCompany());
		NetworkNode node = new NetworkNode(id);
		node.setImpl(new NetworkNodeStdImpl());
		if(product.equals(PRODUCT_TYPE.DU_OWLS)){node.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());}
		if(product.equals(PRODUCT_TYPE.UNIJOB)){node.getIdentifier().setSyntaxRules(UnijobSyntaxRules.getInstance());}
		if(product.equals(PRODUCT_TYPE.DU)){node.getIdentifier().setSyntaxRules(ClassicSyntaxRules.getInstance());}
		node.setContext(getContext());
		node.extract();
		return node.getNodeInfo().getStatus();
	}

	public boolean isNodeAlive(NetworkNodeEntity ent,PRODUCT_TYPE product) throws UniverseException {
		NetworkNodeId id = new NetworkNodeId(product,ent.getName(),ent.getCompany());
		NetworkNode node = new NetworkNode(id);
		node.setImpl(new NetworkNodeStdImpl());
		if(product.equals(PRODUCT_TYPE.DU_OWLS)){node.getIdentifier().setSyntaxRules(OwlsSyntaxRules.getInstance());}
		if(product.equals(PRODUCT_TYPE.UNIJOB)){node.getIdentifier().setSyntaxRules(UnijobSyntaxRules.getInstance());}
		if(product.equals(PRODUCT_TYPE.DU)){node.getIdentifier().setSyntaxRules(ClassicSyntaxRules.getInstance());}
		node.setContext(getContext());
		node.extract();
		if(node.getNodeInfo().getStatus()==1){return true;}
		return false;
	}
	public List<NetworkNodeEntity> getUVNodeList(PRODUCT_TYPE product){
		ClientServiceLocator.setContext(getContext());
		
		List<NetworkNodeEntity> list = ClientServiceLocator.getNetworkNodeService().getNodesByTypeAndWildcard(product, "*", "*");
		return list;
	}
	public NetworkNodeEntity getNodeEntityFromName(String name,PRODUCT_TYPE product){
		List<NetworkNodeEntity> list = getUVNodeList(product);
		for(int i=0;i<list.size();i++){
			if(list.get(i).getName().equalsIgnoreCase(name)){return list.get(i);}
		}
		return null;
	}
	public NodeRefEntity[] getNodeReferences(){
		return ClientServiceLocator.getNodeRefService().findAllNodesRef();
	}
	
	public NodeRefEntity addNodeReference(String name, String label, AreaEntity area){
		NodeRefEntity noderef = new NodeRefEntity();
		noderef.setArea(area);
		noderef.setLabel(label);
		noderef.setName(name);		
		return ClientServiceLocator.getNodeRefService().create(noderef);
	}
}
