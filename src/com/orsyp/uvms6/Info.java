package com.orsyp.uvms6;

import java.util.List;

import com.orsyp.api.Context;
import com.orsyp.central.jpa.jpo.security.audit.AuditTrailEntity;
import com.orsyp.central.monitor.ServerInfo;
import com.orsyp.central.monitor.ServerUserInfo;
import com.orsyp.comm.client.ClientServiceLocator;
import com.orsyp.du6.GenericObjects;

public class Info  extends GenericObjects {
	private Context GlobalCtx;

	public Info (com.orsyp.stores.UVAPIWrapper conn, Context GlobalCtx){
		super(conn);
		this.GlobalCtx = GlobalCtx;
	}
	private Context getContext(){
		return GlobalCtx;
	}
	public List<ServerUserInfo> getUVUserInfo(){
		ClientServiceLocator.setContext(getContext());
		return ClientServiceLocator.getServerInfoService().getAllServerUserInfo();	
	}
	public ServerInfo getUVServerInfo(){
		ClientServiceLocator.setContext(getContext());
		return ClientServiceLocator.getServerInfoService().getServerInfo();	
	}
	public List<com.orsyp.central.jpa.jpo.security.audit.AuditTrailEntity> getUVAuditTrail(){
		ClientServiceLocator.setContext(getContext());
		List<AuditTrailEntity> myLst = ClientServiceLocator.getAuditTrailService().getAllAuditTrailsEntries();
		return myLst;
	}
}
