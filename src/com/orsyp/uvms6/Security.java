package com.orsyp.uvms6;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.orsyp.api.Context;
import com.orsyp.central.jpa.jpo.GroupRoleProxyEntity;
import com.orsyp.central.jpa.jpo.GroupUserProxyEntity;
import com.orsyp.central.jpa.jpo.alert.SessionAlertRuleEntity;
import com.orsyp.central.jpa.jpo.security.AuthType;
import com.orsyp.central.jpa.jpo.security.GroupEntity;
import com.orsyp.central.jpa.jpo.security.LoginEntity;
import com.orsyp.central.jpa.jpo.security.PermissionEntity;
import com.orsyp.central.jpa.jpo.security.RoleEntity;
import com.orsyp.central.jpa.jpo.security.RoleType;
import com.orsyp.central.jpa.jpo.security.UserEntity;
import com.orsyp.comm.client.ClientServiceLocator;
import com.orsyp.du6.GenericObjects;

public class Security  extends GenericObjects {
	private Context GlobalCtx;

	public Security (com.orsyp.stores.UVAPIWrapper conn, Context GlobalCtx){
		super(conn);
		this.GlobalCtx = GlobalCtx;
	}
	private Context getContext(){
		return GlobalCtx;
	}
	public void deleteUserFromUVGroup(String rolename, String groupname){
		ClientServiceLocator.setContext(getContext());
	}
	public void addUserToUVGroup(long userid, String groupname){
		ClientServiceLocator.setContext(getContext());

		UserEntity ValidUserEntToAdd = ClientServiceLocator.getSecurityUserService().findById(userid);

		GroupEntity ValidGroupEnt = ClientServiceLocator.getSecurityGroupService().findGroupByName(groupname, false);
		
		Set<GroupUserProxyEntity> UserProxySet = ValidGroupEnt.getGroupUserProxies();

		GroupUserProxyEntity UserProxy = new GroupUserProxyEntity();
		UserProxy.setUser(ValidUserEntToAdd);
		UserProxy.setGroup(ValidGroupEnt);
		UserProxySet.add(UserProxy);
		
		ValidGroupEnt.setGroupUserProxies(UserProxySet);
		ClientServiceLocator.getSecurityGroupService().update(ValidGroupEnt);
	}
	public void deleteRoleFromUVGroup(String rolename, String groupname){
		ClientServiceLocator.setContext(getContext());
		Set<GroupRoleProxyEntity> RoleProxySet = ClientServiceLocator.getSecurityGroupService().findGroupByName(groupname, false).getGroupRoleProxies();
		Set<GroupRoleProxyEntity> NewRoleProxySet = new HashSet<GroupRoleProxyEntity>();
		
		Iterator<GroupRoleProxyEntity> it = RoleProxySet.iterator();
		// only adding all original roles with a different name
		while(it.hasNext()){
			GroupRoleProxyEntity myGrpEnt = it.next();
			if(!myGrpEnt.getRole().getName().equalsIgnoreCase(rolename)){NewRoleProxySet.add(myGrpEnt);}
		}

		ClientServiceLocator.getSecurityGroupService().findGroupByName(groupname, false).setGroupRoleProxies(NewRoleProxySet);
		ClientServiceLocator.getSecurityGroupService().update(ClientServiceLocator.getSecurityGroupService().findGroupByName(groupname, false));
	}
	public void addRoleToUVGroup(String rolename, RoleType type, String groupname){
		ClientServiceLocator.setContext(getContext());

		RoleEntity ValidRoleEnt = ClientServiceLocator.getSecurityRoleService().getRoleByNameAndType(rolename, type);
		GroupEntity ValidGroupEnt = ClientServiceLocator.getSecurityGroupService().findGroupByName(groupname, false);
		
		Set<GroupRoleProxyEntity> RoleProxySet = ValidGroupEnt.getGroupRoleProxies();

		GroupRoleProxyEntity RoleProxy = new GroupRoleProxyEntity();
		RoleProxy.setRole(ValidRoleEnt);
		RoleProxy.setGroup(ValidGroupEnt);
		RoleProxySet.add(RoleProxy);
		
		ValidGroupEnt.setGroupRoleProxies(RoleProxySet);
		ClientServiceLocator.getSecurityGroupService().update(ValidGroupEnt);
	}
	public void createUVGroup(String name, String label,AuthType auth){
		ClientServiceLocator.setContext(getContext());
		Set<GroupRoleProxyEntity> RoleProxySet = new HashSet<GroupRoleProxyEntity>();
		Set<GroupUserProxyEntity> UsrProxyAllSet = new HashSet<GroupUserProxyEntity>();
		Collection<GroupUserProxyEntity> UsrProxySet = new HashSet<GroupUserProxyEntity>();
		GroupEntity myGrp = new GroupEntity();
		myGrp.setName(name);
		if(label!=null){myGrp.setLabel(label);}
		myGrp.setAuthType(auth);
		UsrProxyAllSet.addAll(UsrProxySet);
		myGrp.setGroupUserProxies(UsrProxyAllSet);
		myGrp.setGroupRoleProxies(RoleProxySet);
		myGrp = ClientServiceLocator.getSecurityGroupService().create(myGrp);
	}
	public void deleteUVGroupByName(String name){
		ClientServiceLocator.setContext(getContext());
		GroupEntity myGrp = ClientServiceLocator.getSecurityGroupService().findGroupByName(name, false);
		ClientServiceLocator.getSecurityGroupService()
		.delete(ClientServiceLocator.getSecurityGroupService().findGroupByName(name, false).getId());
	}
	public Collection<GroupEntity> getUVGroupList(){
		ClientServiceLocator.setContext(getContext());
		Collection<GroupEntity> collection = ClientServiceLocator.getSecurityGroupService().getGroups(true);
		return collection;
	}
	public Collection<SessionAlertRuleEntity> getAlertRulesList(){
		ClientServiceLocator.setContext(getContext());
		Collection<SessionAlertRuleEntity> collection = ClientServiceLocator.getOwlsAlertRulesService().getOwlsAlertRules();
		return collection;
	}
	
	// Roles
	public Collection<RoleEntity> getUVRoleList(){
		ClientServiceLocator.setContext(getContext());
		Collection<RoleEntity> collection = ClientServiceLocator.getSecurityRoleService().getAllRoles();
		return collection;
	}
	public RoleEntity duplicateUVRole(RoleEntity RoleSource, String RoleTargetName){
		Set<PermissionEntity> originalPermSet = RoleSource.getPermissions();
		Set<PermissionEntity> newPermSet = new HashSet<PermissionEntity>();
		Iterator<PermissionEntity> ModelPermissionIterator = originalPermSet.iterator();
		RoleEntity TargetRole = new RoleEntity();
		// For each existing Permissions..
		while (ModelPermissionIterator.hasNext()){

			PermissionEntity OnePermissionEnt = ModelPermissionIterator.next();
			PermissionEntity TargetPermissionEnt = new PermissionEntity();
			
			// We duplicate each attribute into the new permission and add to the target set of permissions
			TargetPermissionEnt.setActionName(OnePermissionEnt.getActionName());
			TargetPermissionEnt.setPermissionClass(OnePermissionEnt.getPermissionClass());
			TargetPermissionEnt.setPositive(OnePermissionEnt.isPositive());
			TargetPermissionEnt.setProtectedResource(OnePermissionEnt.getProtectedResource());
			newPermSet.add(TargetPermissionEnt);
			
		}
		
		TargetRole.setName(RoleTargetName);
		TargetRole.setRoleType(RoleSource.getRoleType());
		TargetRole.setArea(RoleSource.getArea());
		TargetRole.setCompany(RoleSource.getCompany());
		TargetRole.setNodeType(RoleSource.getNodeType());
		TargetRole.setNodeName(RoleSource.getNodeName());
		TargetRole.setGroupRoleProxies(new HashSet<GroupRoleProxyEntity>());
	
		TargetRole.setPermissions(newPermSet);
		
		return ClientServiceLocator.getSecurityRoleService().create(TargetRole);

	}
	
	public Set<PermissionEntity> getUVPermissionListForRole(RoleEntity ent){
			Set<PermissionEntity> myPermissions = ClientServiceLocator.getSecurityRoleService().getRoleNoGroupById(ent.getId()).getPermissions();
			return myPermissions;
	}
	public void delPermissionInDU6Role(RoleEntity Role, long PermissionId){
		Set<PermissionEntity> originalPermissions = ClientServiceLocator.getSecurityRoleService().getRoleNoGroupById(Role.getId()).getPermissions();
		Set<PermissionEntity> newPermissions = new HashSet<PermissionEntity>();
		Iterator<PermissionEntity> it = originalPermissions.iterator();
		while(it.hasNext()){
			PermissionEntity ent = it.next();
			if(ent.getId() != PermissionId){
				newPermissions.add(ent);
			}
		}
		Role.setPermissions(newPermissions);
		ClientServiceLocator.getSecurityRoleService().update(Role);
	}
	
	public void addPermissionInDU6Role(RoleEntity Role, String PermissionClass, String ActionName, String ProtectedResource, boolean isPositive){
		Set<PermissionEntity> originalPermissions = ClientServiceLocator.getSecurityRoleService().getRoleNoGroupById(Role.getId()).getPermissions();
		PermissionEntity newPermission = new PermissionEntity();
		newPermission.setActionName(ActionName);
		newPermission.setPermissionClass(PermissionClass);
		newPermission.setProtectedResource(ProtectedResource);
		newPermission.setPositive(isPositive);
		originalPermissions.add(newPermission);
		Role.setPermissions(originalPermissions);
		ClientServiceLocator.getSecurityRoleService().update(Role);
	}

	public void updatePermissionInDU6Role(RoleEntity Role, long PermissionId, String PermissionClass, String ActionName, String ProtectedResource, boolean isPositive){
		
		Set<PermissionEntity> originalPermissions = ClientServiceLocator.getSecurityRoleService().getRoleNoGroupById(Role.getId()).getPermissions();
		Iterator<PermissionEntity> it = originalPermissions.iterator();
		
		Set<PermissionEntity> updatedPermissions = new HashSet<PermissionEntity>();
		
		boolean isPermFound = false;
		while(it.hasNext()){
			PermissionEntity ent = it.next();
			
			if(ent.getId()==PermissionId){
				isPermFound=true;
				ent.setActionName(ActionName);
				ent.setPermissionClass(PermissionClass);
				ent.setProtectedResource(ProtectedResource);
				ent.setPositive(isPositive);
				updatedPermissions.add(ent);
			}else{
				updatedPermissions.add(ent);
			}
		}
		Role.setPermissions(updatedPermissions);
		ClientServiceLocator.getSecurityRoleService().update(Role);
	}

	
	public void addPermissionInDuExplorerRole(RoleEntity role,  String PERMISSION){
		addPermissionInUVMSRole(role,PERMISSION);
	}
	public void delPermissionInDuExplorerRole(RoleEntity role,  String PERMISSION){
		delPermissionInUVMSRole(role,PERMISSION);
	}
	public void addPermissionInReporterRole(RoleEntity role,  String PERMISSION){
		addPermissionInUVMSRole(role,PERMISSION);
	}
	public void delPermissionInReporterRole(RoleEntity role,  String PERMISSION){
		delPermissionInUVMSRole(role,PERMISSION);
	}
	public void delPermissionInUVCRole(RoleEntity role, String PERMISSION){
		delPermissionInUVMSRole(role,PERMISSION);
	}
	public void adddelPermissionInUVCRole(RoleEntity role, String PERMISSION){
		ClientServiceLocator.setContext(getContext());
		Set<PermissionEntity> OriginalPermSet = ClientServiceLocator.getSecurityRoleService().getRoleNoGroupById(role.getId()).getPermissions();

		PermissionEntity NewPerm = new PermissionEntity();
		NewPerm.setPermissionClass(PERMISSION);
		NewPerm.setProtectedResource(PERMISSION);
		NewPerm.setActionName("*");
	
		OriginalPermSet.add(NewPerm);	
		role.setPermissions(OriginalPermSet);
			ClientServiceLocator.getSecurityRoleService().update(role);
	}
	public void addPermissionInUnijobRole(RoleEntity role, String PERMISSION){
		addPermissionInUVMSRole(role,PERMISSION);
	}
	public void delPermissionInUJRole(RoleEntity role, String PERMISSION){
		delPermissionInUVMSRole(role,PERMISSION);
	}
	public void delPermissionInUVMSRole(RoleEntity role, String PERMISSION){
		ClientServiceLocator.setContext(getContext());
		Set<PermissionEntity> OriginalPermSet = ClientServiceLocator.getSecurityRoleService().getRoleNoGroupById(role.getId()).getPermissions();
		Set<PermissionEntity> UpdatedPermSet = new HashSet<PermissionEntity>();
		
		// We loop through all existing permissions. We add all of them as long as they do not match the one to remove
		Iterator<PermissionEntity> it = OriginalPermSet.iterator();
		while(it.hasNext()){
			PermissionEntity PermToCheck = it.next();
			if(!PermToCheck.getPermissionClass().equalsIgnoreCase(PERMISSION)){
				UpdatedPermSet.add(PermToCheck);
			}
		}
		role.setPermissions(UpdatedPermSet);
			ClientServiceLocator.getSecurityRoleService().update(role);
	}
		
	public void addPermissionInUVMSRole(RoleEntity role, String PERMISSION){
		ClientServiceLocator.setContext(getContext());
		Set<PermissionEntity> OriginalPermSet = ClientServiceLocator.getSecurityRoleService().getRoleNoGroupById(role.getId()).getPermissions();

		PermissionEntity NewPerm = new PermissionEntity();
		NewPerm.setPermissionClass(PERMISSION);
		NewPerm.setProtectedResource("*");
		NewPerm.setActionName("*");
	
		OriginalPermSet.add(NewPerm);	
		role.setPermissions(OriginalPermSet);
			ClientServiceLocator.getSecurityRoleService().update(role);
	}
	public Collection<LoginEntity> getUVLoginList(){
		ClientServiceLocator.setContext(getContext());
		Collection<LoginEntity> collection = ClientServiceLocator.getSecurityUserService().getLogins(true);
		return collection;
	}
	public LoginEntity getUVLoginByName(String name){
		ClientServiceLocator.setContext(getContext());
		LoginEntity ent = ClientServiceLocator.getSecurityUserService().getLoginByName(name, true);
		return ent;	
	}
	public void deleteUVLoginByName(String name){
		ClientServiceLocator.setContext(getContext());
		long id = ClientServiceLocator.getSecurityUserService().getLoginByName(name, true).getId();
		ClientServiceLocator.getSecurityUserService().delete(id);
	}
	public UserEntity duplicateUVLoginByName(String name, String newname){
		LoginEntity OriginalLogin = ClientServiceLocator.getSecurityUserService().getLoginByName(name,false);
		
		LoginEntity myUser = new LoginEntity();
		
		myUser.setName(newname);
		myUser.setLabel(OriginalLogin.getLabel());
		myUser.setUserCategory(OriginalLogin.getUserCategory());
		myUser.setAuthType(OriginalLogin.getAuthType());
		myUser.setPasswordChanged(false);
		myUser.setPwdHash(OriginalLogin.getPwdHash());

		Set<GroupUserProxyEntity> allGrps = OriginalLogin.getGroupUserProxies();

		Iterator<GroupUserProxyEntity> it = allGrps.iterator();
		while (it.hasNext()){
			GroupUserProxyEntity oldGrpPrx = it.next();
			GroupUserProxyEntity newGrpPrx = new GroupUserProxyEntity();
			newGrpPrx.setGroup(oldGrpPrx.getGroup());
			newGrpPrx.setUser(myUser);
			myUser.addGroupUserProxies(newGrpPrx);
		}

		UserEntity EnUser=ClientServiceLocator.getSecurityUserService().create(myUser,true);
		return EnUser;
	}
	public Set<GroupUserProxyEntity> getGroupListForLoginByName(String name){
		LoginEntity ent = ClientServiceLocator.getSecurityUserService().getLoginByName(name, true);
		Set<GroupUserProxyEntity> allGrps = ent.getGroupUserProxies();
		return allGrps;
	}
	public void addGroupToLoginByName(String loginname, String groupname){
		LoginEntity ent = ClientServiceLocator.getSecurityUserService().getLoginByName(loginname, true);
		GroupEntity myGrp = ClientServiceLocator.getSecurityGroupService().findGroupByName(groupname, false);
		GroupUserProxyEntity myUPEntity = new GroupUserProxyEntity();
		myUPEntity.setGroup(myGrp);
		myUPEntity.setUser(ent);
		myGrp.addGroupUserProxies(myUPEntity);
		ClientServiceLocator.getSecurityGroupService().update(myGrp);
	}
	public void delGroupFromLoginByName(String loginname, String groupname){
		LoginEntity ent = ClientServiceLocator.getSecurityUserService().getLoginByName(loginname, true);
		GroupEntity myGrp = ClientServiceLocator.getSecurityGroupService().findGroupByName(groupname, false);
		GroupUserProxyEntity myUPEntity = new GroupUserProxyEntity();
		myUPEntity.setGroup(myGrp);
		myUPEntity.setUser(ent);
		
		Set<GroupUserProxyEntity> myProxies = ent.getGroupUserProxies();
		Set<GroupUserProxyEntity> myProxiesToDelete = new HashSet<GroupUserProxyEntity>();
		
		Iterator itProx = myProxies.iterator();
		while(itProx.hasNext()){
			
			GroupUserProxyEntity myProx = (GroupUserProxyEntity) itProx.next();
			if(myProx.getGroup().getName().equals(groupname)){
				myProxiesToDelete.add(myProx);
				ClientServiceLocator.getSecurityGroupService().deleteGroupUserProxy(myProxiesToDelete);
			}
		}
	}
}
