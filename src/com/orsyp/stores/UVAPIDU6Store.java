package com.orsyp.stores;


import com.orsyp.api.Context;
import com.orsyp.du6.Uprocs;
import com.orsyp.du6.*;

/**
 * 
 * DU6 API wrapper 
 * 
 * March 2014 -> adapted to 6.2 (Triggers implemented)
 * @author bsa
 *
 */

public class UVAPIDU6Store extends UVAPIGenericStore{
	private Context Ctx;
	public Uprocs Uprocs;
	public Sessions Sessions;
	public Tasks Tasks;
	public Applications Applications;
	public BusinessViews BusinessViews;
	public Calendars Calendars;
	public Domains Domains;
	public DqmQueues DqmQueues;
	public Engines Engines;
	public Exceptions Exceptions;
	public Executions Executions;
	public Mus Mus;
	public Nodes Nodes;
	public Notes Notes;
	public Resources Resources;
	public Rules Rules;
	public Runbooks Runbooks;
	public Statistics Statistics;
	public Triggers Triggers;
	public Users Users;
	public Licenses Licenses;
	public Deployments Deployments;
	public Crons Crons;
	public WTSchedules WTSchedules;
	
	@Override
	public void setAPIWrapper(com.orsyp.stores.UVAPIWrapper c){
		super.setAPIWrapper(c);
	}
	
	public UVAPIDU6Store(Context GlobalCtx){
		super(GlobalCtx);	
		
	}

	public void setContext(Context ctx){
		ctx.getEnvironment().getNodeName();
		this.Ctx=ctx;
	}
	
	public Context getContext(){
		return this.Ctx;
	}

	protected void initiate(Context ctx) {
		Uprocs = new Uprocs(super.getAPIWrapper(),ctx);
		Sessions = new Sessions(super.getAPIWrapper(),ctx);
		Tasks = new Tasks(super.getAPIWrapper(),ctx);
		Applications= new Applications(super.getAPIWrapper(),ctx);
		BusinessViews= new BusinessViews(super.getAPIWrapper(),ctx);
		Calendars= new Calendars(super.getAPIWrapper(),ctx);
		Domains= new Domains(super.getAPIWrapper(),ctx);
		DqmQueues= new DqmQueues(super.getAPIWrapper(),ctx);
		Engines= new Engines(super.getAPIWrapper(),ctx);
		Exceptions= new Exceptions(super.getAPIWrapper(),ctx);
		Executions= new Executions(super.getAPIWrapper(),ctx);
		Mus= new Mus(super.getAPIWrapper(),ctx);
		Nodes= new Nodes(super.getAPIWrapper(),ctx);
		Notes= new Notes(super.getAPIWrapper(),ctx);
		Resources= new Resources(super.getAPIWrapper(),ctx);
		Rules= new Rules(super.getAPIWrapper(),ctx);
		Runbooks= new Runbooks(super.getAPIWrapper(),ctx);
		Statistics= new Statistics(super.getAPIWrapper(),ctx);
		Triggers= new Triggers(super.getAPIWrapper(),ctx);
		Users= new Users(super.getAPIWrapper(),ctx);
		Licenses = new Licenses(super.getAPIWrapper(),ctx);
		Deployments = new Deployments(super.getAPIWrapper(),ctx);
		Crons = new Crons(super.getAPIWrapper(),ctx);
		WTSchedules = new WTSchedules(super.getAPIWrapper(),ctx);
	}
	/*
	public void getUxTrace(String PathToOutput) throws UniToolsException, InterruptedException{
		int maxDuration  = -1;
		ConnectionFactory connFactory = NodeConnectionFactory.getInstance(this.getCentral());

		UniToolsStdFactory factory = 
			new UniToolsStdFactory(this.getContext(),connFactory);
		
		UniUxtrace uxtrace = (UniUxtrace)factory.getUniTools(UniTools.ToolType.UNIUXTRACE);
		
		UniUxtraceOptions options = new UniUxtraceOptions("/batch:yes",0);
		uxtrace.setOptions(options);

		uxtrace.getDispatcher().addListener(new UniToolsEventListener() {
            public void uniToolsEventOccured(UniToolsEvent e) {
                System.out.println(e.toString());
            }                
        });
		UxTraceNewThread thread = new UxTraceNewThread(uxtrace,PathToOutput);						
		
		int counter = 0;
		while(true) {
			Thread.sleep(1000);
			counter++;
			if ( (counter > maxDuration) && (maxDuration > 0) ) {
				System.out.println("max duration reached, sending stop signal");
				uxtrace.setStop(true);
				break;
			}
			if (uxtrace.getRunStatus() == RunStatus.OK) {
				System.out.println("uxtrace OK");
				return;
			}
			if (uxtrace.getRunStatus() == RunStatus.KO) {
				System.out.println("uxtrace KO");
				return;
			}
		}
		System.out.println("uxtrace unknown status");
	}
*/
}














