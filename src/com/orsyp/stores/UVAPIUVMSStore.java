package com.orsyp.stores;

import com.orsyp.api.Context;
import com.orsyp.uvms6.*;

/**
 * 
 * UVMS API wrapper
 * @author bsa
 *
 */

public class UVAPIUVMSStore extends UVAPIGenericStore{

	private Context CentralCtx;
	public Security Security;
	public Packages Packages;
	public Info Info;
	public Nodes Nodes;
	public Alerts Alerts;
	
	
	public UVAPIUVMSStore(Context GlobalCtx){
		super(GlobalCtx);
	}
	
	public void setContext(Context ctx){
		this.CentralCtx=ctx;
	}
	public void cetCentralContext(Context ctx){
		this.CentralCtx=ctx;
	}
	
	private Context getCentralContext(){
		return this.CentralCtx;
	}
	protected void initiate(Context ctx) {
		Security = new Security(super.getAPIWrapper(),ctx);
		Packages = new Packages(super.getAPIWrapper(),ctx);
		Info = new Info(super.getAPIWrapper(),ctx);
		Nodes= new Nodes(super.getAPIWrapper(),ctx);
		Alerts = new Alerts(super.getAPIWrapper(),ctx);
		
	}

	





}
