package com.orsyp.stores;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import com.orsyp.Area;
import com.orsyp.Environment;
import com.orsyp.Identity;
import com.orsyp.SyntaxException;
import com.orsyp.UniverseException;
import com.orsyp.api.Client;
import com.orsyp.api.Context;
import com.orsyp.api.Product;
import com.orsyp.api.central.UniCentral;
import com.orsyp.api.central.networknode.NetworkNodeId.PRODUCT_TYPE;
import com.orsyp.api.central.services.INetworkNodeService;
import com.orsyp.api.user.UserSystem;
import com.orsyp.central.jpa.NetworkNodeTypeEnum;
import com.orsyp.central.jpa.jpo.HostEntity;
import com.orsyp.central.jpa.jpo.NetworkNodeEntity;
import com.orsyp.comm.client.ClientServiceLocator;

import com.orsyp.std.ClientConnectionManager;
import com.orsyp.std.ConnectionFactory;
import com.orsyp.std.NodeConnectionFactory;
import com.orsyp.std.central.UniCentralStdImpl;
import com.orsyp.util.ApplicationVariables;


/**
 * UVMS connection wrapper.
 * API wrapper
 * @author bsa
 *
 */
public class UVAPIWrapper {



	private HashMap<String, String> nodes = null;
	private HashMap<String, Context> contexts = new HashMap<String, Context>();
	private HashMap<String, String> appDomains = new HashMap<String, String>();
	private Context GlobalContext;
	private Context NodeContext;
	
	public UVAPIUVMSStore UVMSStore; 
	public UVAPIDU6Store DU6Store = new UVAPIDU6Store(GlobalContext);
	public UVAPIDU5Store DU5Store = new UVAPIDU5Store(GlobalContext);
	public UVAPIUJ1Store UJ1Store = new UVAPIUJ1Store(GlobalContext);
	
	// private constructor, force to use factory method
	
	private Product WrapperProduct;
	
	public UniCentral central;
	public String user;
	public String host;
	public String company;
	public String area;
	public String referenceNode;
	
	public String sysUser;
	public String sysPassword;
	public com.orsyp.api.user.UserSystem.Type sysType = UserSystem.Type.W32;
	
	private String defaultVersion = "001";

	// CONSTRUCTOR !
	public UVAPIWrapper(String host, int port, String user, String pw, String company, String area) throws Exception {
		createConnection(host,port,user,pw,company,area);
	}

	private void createConnection(String host, int port, String user, String pw, String company, String area) throws Exception {

		this.central = new UniCentral(host, port);
		this.central.setImplementation(new UniCentralStdImpl(this.central));
		this.central.setSslEnabled(ApplicationVariables.SSL_SECURITY.equals(System.getProperty(ApplicationVariables.SECURITY_TYPE)));
		this.host = host;
		this.user = user;
		this.company = company;
		this.area = area;
		this.referenceNode = "DUMMYNODE"; // referenceNode;
		this.sysUser = "*"; // sysUser;
		this.sysPassword = "*"; // sysPassword;				
		this.sysType = com.orsyp.api.user.UserSystem.Type.W32;
		String systemOs = "WEB";
		
		if (!systemOs.equals("W32"))
			this.sysType = com.orsyp.api.user.UserSystem.Type.OTHER;
		if (Arrays.asList("A", "APP", "I", "INT").contains(area.toUpperCase())) 
			this.defaultVersion = "001";
		else
			this.defaultVersion = "000";

		// login
		try {
			this.central.login(user, pw);
			this.central.authenticate();
			
		} catch (UniverseException e) {
			e.printStackTrace();
			throw new Exception("Login failed");
		}

		ConnectionFactory factory = NodeConnectionFactory.getInstance(this.central);
		ClientConnectionManager.setDefaultFactory(factory);
		
		ClientConnectionManager.setDefaultFactory(NodeConnectionFactory.getInstance(this.central));
		Environment envCentral = null;
		try {
			envCentral = new Environment("UJCENT", "UJCENT");
			envCentral.setNodeId("UJCENT");
		} catch (SyntaxException e) {
			System.err.println("Syntax error for environment: " + e.getMessage());
			
		}
		Client client = new Client(new Identity(user, "", host, "WEB"));
	
		//ImplementationProviderFactory.init(this.central);
		//DUObjExtractor.getDUObjExtractorinstance().;
		Context ctx = new Context(envCentral,client, this.central);
		
		ctx.setProduct(Product.UNICENTRAL);
		ctx.setUnijobCentral(this.central);
		this.UVMSStore = new UVAPIUVMSStore(ctx);
		this.UVMSStore.setAPIWrapper(this);
		this.GlobalContext=ctx;
		this.UVMSStore.setContext(ctx);
		this.UVMSStore.setCentralContext(ctx);
		this.UVMSStore.initiate(ctx);
		ClientServiceLocator.setContext(ctx);
		//List<NetworkNodeEntity> list = ClientServiceLocator.getNetworkNodeService().getNodesByTypeAndWildcard(PRODUCT_TYPE.DU_OWLS, "*", "*");
		
	}
	public static void cleanup() {
		ClientConnectionManager.cleanup();
	}
	public Context getCentralContext(){
		return this.GlobalContext;
	}
	
	public Context setCurrentNodeContext(NetworkNodeEntity ent) throws SyntaxException{
		ent.getProductType();
		if (nodes == null)
			nodes = getNodeList();
		Context context = null;
		String Company = ent.getCompany();
		
		// the following line is necessary because for Product UNIJOB the company must apparently be UNIJOB.
		if(ent.getProductType().equals(NetworkNodeTypeEnum.UNIJOB)){Company="UNIJOB";}
			
		Client client = new Client(new Identity(user, "*", "*", "*"));
		context = new Context(new Environment(Company, ent.getName(), Area.Exploitation), client, central);
		
		
		if(ent.getProductType().toString().equals("UNIJOB")){
			context.setProduct(Product.UNIJOB);
			this.UJ1Store.setContext(context);
			this.UJ1Store.setCentralContext(GlobalContext);
			this.UJ1Store.setCentral(central);
			this.UJ1Store.setAPIWrapper(this);
			this.UJ1Store.initiate(context);
			}
		if(ent.getProductType().toString().equals("DU_OWLS")){
			context.setProduct(Product.OWLS);
			this.DU6Store.setCentralContext(GlobalContext);
			this.DU6Store.setContext(context);
			this.DU6Store.setAPIWrapper(this);
			this.DU6Store.initiate(context);
			}
		
		if(ent.getProductType().toString().equals("DU")){
			context.setProduct(Product.DOLLAR_UNIVERSE);
			this.DU5Store.setCentralContext(GlobalContext);
			this.DU5Store.setCentral(central);
			this.DU5Store.setContext(context);
			this.DU5Store.setAPIWrapper(this);
			this.DU5Store.initiate(context);
			}
		
		this.NodeContext=context;
		return this.NodeContext;
		
	}
	public Context getCurrentNodeContext() throws SyntaxException{
		
		return this.NodeContext;
		
	}
	@Deprecated
	public Context setMainNodeContext(NetworkNodeEntity ent, Product product) throws SyntaxException{
		if (nodes == null)
			nodes = getNodeList();
		Context context = null;
		String Company = ent.getCompany();
		// the following line is necessary because for Product UNIJOB the company must apparently be UNIJOB.
		if(product==Product.UNIJOB){Company="UNIJOB";}
			
		Client client = new Client(new Identity(user, "", nodes.get(ent.getName()), ""));
		context = new Context(new Environment(Company, ent.getName(), Area.Exploitation), client, central);
		context.setProduct(product);
		this.NodeContext=context;
		if(product==Product.UNIJOB){
			this.UJ1Store.setContext(this.NodeContext);
			this.UJ1Store.initiate(this.NodeContext);}
		if(product==Product.OWLS){
			this.DU6Store.setContext(this.NodeContext);
			this.DU6Store.initiate(this.NodeContext);}
		if(product==Product.DOLLAR_UNIVERSE){
			this.DU5Store.setContext(this.NodeContext);
			this.DU5Store.initiate(this.NodeContext);}
		return this.NodeContext;
	}
	public Context getContext() throws Exception {
		return getCurrentNodeContext();
	}
	
	private HashMap<String, String> getNodeList() {		
		Client client = new Client(new Identity(user, "", host, ""));

		ConnectionFactory factory = NodeConnectionFactory.getInstance(central);
		ClientConnectionManager.setDefaultFactory(factory);

		Environment envCentral;
		try {
			envCentral = new Environment("UJCENT", "UJCENT");
			envCentral.setNodeId("UJCENT");
		} catch (SyntaxException e) {
			System.err.println("Syntax error for environment: " + e.getMessage());
			return null;
		}

		Context ctx = new Context(envCentral, client, central);
		ctx.setProduct(Product.UNICENTRAL);

		ClientServiceLocator.setContext(ctx);
		INetworkNodeService nnService = ClientServiceLocator.getNetworkNodeService();
		List<NetworkNodeEntity> nnes = new ArrayList<NetworkNodeEntity>();
		nnes.addAll(nnService.getOwlsNodesForCompanyAndNode("*", "*"));

		HashMap<String, String> map = new HashMap<String, String>();

		for (NetworkNodeEntity entity : nnes) {
			Set<HostEntity> hosts = entity.getHosts();
			HostEntity host = hosts.iterator().next();
			if (host != null)
				map.put(entity.getName(), host.getName());
		}
		return map;
	}

	/*
	 * public void setNodeContext(String node,Product product) throws SyntaxException{
		if (!contexts.containsKey(node))
			contexts.put(node, makeContext(node,area));
		this.GlobalContext=contexts.get(node);
		if(product==Product.OWLS){
		this.DU6Store.setContext(contexts.get(node));}
		if(product==Product.UNIJOB){
			this.UJ1Store.setContext(contexts.get(node));}
	}
	 */
	/*
	public Context makeContext(String node, String targetArea) throws SyntaxException {
	Context context = null;

	Area areaObj = Area.Exploitation;
	if (Arrays.asList("A", "APP").contains(targetArea.toUpperCase())) 
		areaObj = Area.Application;
	else if (Arrays.asList("I", "INT").contains(targetArea.toUpperCase())) 
		areaObj = Area.Integration;
	else if (Arrays.asList("S", "SIM").contains(targetArea.toUpperCase())) 
		areaObj = Area.Simulation;

	if (nodes == null)
		nodes = getNodeList();

	Client client = new Client(new Identity(user, "", nodes.get(node), ""));
	context = new Context(new Environment(company, node, areaObj), client, central);
	context.setProduct(Product.OWLS);

	return context;
}
*/


 
}
