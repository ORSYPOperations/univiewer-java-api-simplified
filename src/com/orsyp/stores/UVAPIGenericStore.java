package com.orsyp.stores;

import com.orsyp.api.Context;
import com.orsyp.api.central.UniCentral;

public class UVAPIGenericStore {

	private UniCentral central;
	private Context CentralContext;
	private Context SpecificContext;
	private com.orsyp.stores.UVAPIWrapper APIWrapper;
	
	public UVAPIGenericStore(Context ctx){
		this.CentralContext = ctx;
	}
	public Context GetCentralContext(){
		return this.CentralContext;
	}
	public void setCentralContext(Context context){
		this.CentralContext = context;
	}
	public void setCentral(UniCentral central){
		this.central = central;
	}
	public UniCentral getCentral(){
		return this.central;
	}
	public void setAPIWrapper(UVAPIWrapper c) {
		this.APIWrapper = c;
	}
	public com.orsyp.stores.UVAPIWrapper getAPIWrapper(){
		return this.APIWrapper;
	}
	public Context getSpecificContext(){
		return this.SpecificContext;
	}
	
}
