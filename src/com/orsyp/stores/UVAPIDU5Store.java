package com.orsyp.stores;

import com.orsyp.api.Context;
import com.orsyp.api.uproc.UprocList;
import com.orsyp.du5.*;
/**
 * 
 * DU5 API wrapper
 * => will only be implemented if there is a need?
 * @author bsa
 *
 */

public class UVAPIDU5Store extends UVAPIGenericStore{
	private Context Ctx;
	public Uprocs Uprocs;
	public Sessions Sessions;
	public Tasks Tasks;
	public Applications Applications;
	public BusinessViews BusinessViews;
	public Calendars Calendars;
	public Domains Domains;
	public DqmQueues DqmQueues;
	public Engines Engines;
	public Exceptions Exceptions;
	public Executions Executions;
	public Mus Mus;
	public Nodes Nodes;
	public Resources Resources;
	public Rules Rules;
	public Statistics Statistics;
	public Users Users;
	
	public UVAPIDU5Store(Context GlobalCtx){
		super(GlobalCtx);
	}
	
	public void setContext(Context ctx){
		this.Ctx=ctx;
	}
	
	private Context getContext(){
		return this.Ctx;
	}
	protected void initiate(Context ctx) {
		Uprocs = new Uprocs(super.getAPIWrapper(),ctx);
		Sessions = new Sessions(super.getAPIWrapper(),ctx);
		Tasks = new Tasks(super.getAPIWrapper(),ctx);
		Applications= new Applications(super.getAPIWrapper(),ctx);
		BusinessViews= new BusinessViews(super.getAPIWrapper(),ctx);
		Calendars= new Calendars(super.getAPIWrapper(),ctx);
		Domains= new Domains(super.getAPIWrapper(),ctx);
		DqmQueues= new DqmQueues(super.getAPIWrapper(),ctx);
		Engines= new Engines(super.getAPIWrapper(),ctx);
		Exceptions= new Exceptions(super.getAPIWrapper(),ctx);
		Executions= new Executions(super.getAPIWrapper(),ctx);
		Mus= new Mus(super.getAPIWrapper(),ctx);
		Nodes= new Nodes(super.getAPIWrapper(),ctx);
		Resources= new Resources(super.getAPIWrapper(),ctx);
		Rules= new Rules(super.getAPIWrapper(),ctx);
		Statistics= new Statistics(super.getAPIWrapper(),ctx);
		Users= new Users(super.getAPIWrapper(),ctx);
		//Uprocs = new Uprocs(ctx);
		//Sessions = new Sessions(ctx);
		//Tasks = new Tasks(ctx);
		//Applications= new Applications(ctx);
		//BusinessViews= new BusinessViews(ctx);
		//Calendars= new Calendars(ctx);
		//Domains= new Domains(ctx);
		//DqmQueues= new DqmQueues(ctx);
		//Engines= new Engines(ctx);
		//Exceptions= new Exceptions(ctx);
		//Executions= new Executions(ctx);
		//Mus= new Mus(ctx);
		//Nodes= new Nodes(ctx);
		//Resources= new Resources(ctx);
		//Rules= new Rules(ctx);
		//Statistics= new Statistics(ctx);
		//Users= new Users(ctx);
	}
	
}
