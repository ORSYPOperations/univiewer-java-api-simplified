package com.orsyp.stores.tests;

import java.util.List;

import com.orsyp.api.Context;
import com.orsyp.api.central.networknode.NetworkNodeId.PRODUCT_TYPE;
import com.orsyp.api.uproc.UprocList;
import com.orsyp.central.jpa.jpo.NetworkNodeEntity;

public class TemplateTests {
	
	/**
	 * 
	 * IMPORTANT: This class serves as a "Manual", all implementations and general usage of this java module should follow this logic.
	 * 
	 */

	public TemplateTests() throws Exception {
		
		// STEP 1: Initiate a UVMS connection wrapper "conn" via the UVAPIWrapper
		//         The connection wrapper will initiate all available Stores (for each product)
		com.orsyp.stores.UVAPIWrapper conn = new com.orsyp.stores.UVAPIWrapper(
				UvmsCoordinates.UVMSHost, 
				UvmsCoordinates.UVMSPort, 
				UvmsCoordinates.UVMSLogin, 
				UvmsCoordinates.UVMSPassword, 
				UvmsCoordinates.DuCompany, 
				UvmsCoordinates.Area
				);
		
		// STEP 2: Use the "conn" object to query UVMS in order to retrieve the list of available agents
		//         Example below: list of DU6 nodes on UVMS
		List<NetworkNodeEntity> networkNodeList = conn.UVMSStore.Nodes.getUVNodeList(PRODUCT_TYPE.DU_OWLS);
		
		for(int i=0;i<networkNodeList.size();i++){
			NetworkNodeEntity ent = networkNodeList.get(i);
			if(conn.UVMSStore.Nodes.isNodeAlive(ent,PRODUCT_TYPE.DU_OWLS)){
				
				// STEP 3: Use the appropriate Store from the "conn" object to query an agent
				// !!!!!!!! IT IS ESSENTIAL TO SET THE "MAIN NODE CONTEXT TO THE LOCAL NODE YOU ARE WORKING ON
				// otherwise the Store will execute its method on another context (UVMS), which will not work		
				Context ctx = conn.setCurrentNodeContext(ent);  // -> setting main node context
				//UprocList list = conn.DU6Store.getUprocList(); // -> querying directly from the Store on the node context set right above
				UprocList list2 = conn.DU6Store.Uprocs.getUprocList();
				System.out.println("Number Of Uprocs on node "+ent.getName()+" : "+list2.getCount());
			}
		}
	}
}
