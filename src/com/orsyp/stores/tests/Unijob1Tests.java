package com.orsyp.stores.tests;

import java.util.List;
import com.orsyp.api.Context;
import com.orsyp.api.central.networknode.NetworkNodeId.PRODUCT_TYPE;
import com.orsyp.api.job.JobList;
import com.orsyp.central.jpa.jpo.NetworkNodeEntity;
import com.orsyp.stores.UVAPIWrapper;

public class Unijob1Tests {

	public Unijob1Tests() throws Exception{
		System.out.println("******************               Unijob 1 Tests               *************************");
		com.orsyp.stores.UVAPIWrapper conn = new com.orsyp.stores.UVAPIWrapper(
				UvmsCoordinates.UVMSHost, 
				UvmsCoordinates.UVMSPort, 
				UvmsCoordinates.UVMSLogin, 
				UvmsCoordinates.UVMSPassword, 
				UvmsCoordinates.DuCompany, 
				UvmsCoordinates.Area
				);		
		
	
		List<NetworkNodeEntity> networkNodeList = conn.UVMSStore.Nodes.getUVNodeList(PRODUCT_TYPE.UNIJOB);
		int i=0;
		NetworkNodeEntity ent = conn.UVMSStore.Nodes.getNodeEntityFromName("UNIJOB_AGENT_02", PRODUCT_TYPE.UNIJOB);
		//for(int i=0;i<networkNodeList.size();i++){
			
		//	NetworkNodeEntity ent = networkNodeList.get(2);

			Context ct = conn.setCurrentNodeContext(ent);
			
			//conn.UJ1Store.Jobs.getCronsAsList("root", "test");
			JobList listj = conn.UJ1Store.Jobs.getJobListWithFilter("L*");
			for(int k=0;k<listj.getCount();k++){
				System.out.println("Job is:"+listj.get(k).getName());
				//conn.UJ1Store.Jobs.checkInteractiveBoxOnJob(conn.UJ1Store.Jobs.getJob(listj.get(k)));
			}

			UVAPIWrapper.cleanup();

		//}
	}

	
	
}
