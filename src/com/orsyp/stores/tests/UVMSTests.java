package com.orsyp.stores.tests;

import java.util.Collection;
import java.util.Iterator;
import com.orsyp.central.jpa.jpo.security.RoleEntity;

public class UVMSTests {

	public UVMSTests() throws Exception{
		System.out.println("******************          Univiewer Management Server Tests          *************************");
		com.orsyp.stores.UVAPIWrapper conn = new com.orsyp.stores.UVAPIWrapper(
				UvmsCoordinates.UVMSHost, 
				UvmsCoordinates.UVMSPort, 
				UvmsCoordinates.UVMSLogin, 
				UvmsCoordinates.UVMSPassword, 
				UvmsCoordinates.DuCompany, 
				UvmsCoordinates.Area
				);
		
		Collection<RoleEntity> col = conn.UVMSStore.Security.getUVRoleList();
		Iterator<RoleEntity> it = col.iterator();
		while(it.hasNext()){
			RoleEntity ent = it.next();
			System.out.println(ent.getName());
		}

	}

	
}
