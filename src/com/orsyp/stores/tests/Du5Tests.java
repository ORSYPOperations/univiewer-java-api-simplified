package com.orsyp.stores.tests;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.orsyp.Area;
import com.orsyp.api.Context;
import com.orsyp.api.ImplFactory;
import com.orsyp.api.central.networknode.NetworkNodeId.PRODUCT_TYPE;
import com.orsyp.api.deploymentpackage.PackageElement;
import com.orsyp.api.deploymentpackage.PackageItem;
import com.orsyp.api.deploymentpackage.PackageList;
import com.orsyp.api.engine.EngineList;
import com.orsyp.api.session.Session;
import com.orsyp.api.session.SessionItem;
import com.orsyp.api.session.SessionList;
import com.orsyp.api.upgrade5to6.DesignObjectsUpgrade;
import com.orsyp.api.uproc.Uproc;
import com.orsyp.api.uproc.UprocItem;
import com.orsyp.api.uproc.UprocList;
import com.orsyp.central.jpa.jpo.NetworkNodeEntity;

public class Du5Tests {

	public Du5Tests() throws Exception{
		System.out.println("******************          Dollar Universe 5 Tests          *************************");
		String Company = "UNIV56";
		com.orsyp.stores.UVAPIWrapper conn = new com.orsyp.stores.UVAPIWrapper(
				
				UvmsCoordinates.UVMSHost, 
				UvmsCoordinates.UVMSPort, 
				UvmsCoordinates.UVMSLogin, 
				UvmsCoordinates.UVMSPassword, 
				Company, 
				UvmsCoordinates.Area
				);	
		
		List<NetworkNodeEntity> networkNodeList = conn.UVMSStore.Nodes.getUVNodeList(PRODUCT_TYPE.DU);

		for(int i=0;i<networkNodeList.size();i++){
			
			NetworkNodeEntity ent = networkNodeList.get(i);
			//System.out.println("DEBUG:"+ent.getName() + ":" + ent.getProductType().name());
			Context ctx = conn.setCurrentNodeContext(ent);
			UprocList list = conn.DU5Store.Uprocs.getUprocList();
			UprocItem uprItem = list.get(0);
			Uproc upr = conn.DU5Store.Uprocs.getUproc(uprItem);
			
			SessionList sesList = conn.DU5Store.Sessions.getSessionList();
			SessionItem sesItem = sesList.get(0);
			Session ses = conn.DU5Store.Sessions.getSession(sesItem);
			
			com.orsyp.api.deploymentpackage.Package pck = conn.UVMSStore.Packages.getDu6PackageFromName("TEST");
			
			System.out.println("DEBUG:"+pck.getType()+":"+pck.getProduct());
			PackageElement[] myEls = pck.getElements();
			
			for(int y=0;y<myEls.length;y++){
				PackageElement el = myEls[y];
				System.out.println("DEBUG El is:"+el.getType());
				if(el.getType().getStringRepresentation().equals("SESSION")){
					System.out.println("Syntax rule:"+el.getSession().getIdentifier().getSyntaxRules());
					System.out.println("STUFF:"+el.getSession().getIdentifier());
				}
				System.out.println();
			}
			//Map<Area, Context> duas5Context, ImplFactory duas5Factory, Map<Area, Context> duas6Context, ImplFactory duas6Factory, Collection<Pair<UniverseException, Context>> errors, ProgressionContext progression
			
		//	Map<Area, Context> m = new HashMap<Area, Context>();
			
		//	m.put(Area.Exploitation, conn.DU5Store.getSpecificContext());
		//	ImplFactory du5Fac = new ImplFactory();
			    
		//	DesignObjectsUpgrade up = new DesignObjectsUpgrade();
		//	up.upgradeUprocs(arg0, arg1, arg2, arg3, arg4, arg5)
			PackageList pckList = conn.UVMSStore.Packages.getDu6PackageList();
			for(int g=0;g<pckList.getCount();g++){
				
				//System.out.println("DEBUG Pck:"+pckList.get(g).getName());
				PackageItem item = pckList.get(g);
				//System.out.println("DEBUG Type:"+item.getProduct()+":"+item.getPackageType());
				
			}
			
			//System.out.println(upr.);
			com.orsyp.api.deploymentpackage.Package mypck = conn.UVMSStore.Packages.initiateDu5ObjectPackage("mypck1", "");
			//System.out.println("Upr:"+upr.getType());
			conn.UVMSStore.Packages.addSessionToPackage(ses, mypck);
			conn.UVMSStore.Packages.commitModificationsToPackageFromDu5ToDu6(mypck);
			
		}
	}

	
}
