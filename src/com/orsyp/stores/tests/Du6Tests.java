package com.orsyp.stores.tests;

import com.orsyp.api.Context;
import com.orsyp.api.central.networknode.NetworkNodeId.PRODUCT_TYPE;
import com.orsyp.central.jpa.jpo.NetworkNodeEntity;

public class Du6Tests {

	public Du6Tests() throws Exception{
		System.out.println("******************          Dollar Universe 6 Tests          *************************");
		
		String NodeSource = "AGENT_PRD_02"; // Server B
		String NodeTarget = "AGENT_PRD_01"; // Server A
		//Server A: Will attempt to copy objects from Server B
		com.orsyp.stores.UVAPIWrapper connSource = new com.orsyp.stores.UVAPIWrapper(
				UvmsCoordinates.UVMSHost, 
				UvmsCoordinates.UVMSPort, 
				UvmsCoordinates.UVMSLogin, 
				UvmsCoordinates.UVMSPassword, 
				UvmsCoordinates.DuCompany, 
				UvmsCoordinates.Area
				);
		
	
		// Server A	
		NetworkNodeEntity networkNodeTarget = connSource.UVMSStore.Nodes.getNodeEntityFromName("AGENT_REPOSITORY", PRODUCT_TYPE.DU_OWLS);
		NetworkNodeEntity nodeB = connSource.UVMSStore.Nodes.getNodeEntityFromName("AGENT_PRD_01", PRODUCT_TYPE.DU_OWLS);

		Context ctx = connSource.setCurrentNodeContext(networkNodeTarget);

		System.out.println(connSource.UVMSStore.Nodes.isAreaActive('A', networkNodeTarget));
		System.out.println(connSource.UVMSStore.Nodes.isAreaActive('I', networkNodeTarget));
		System.out.println(connSource.UVMSStore.Nodes.isAreaActive('S', networkNodeTarget));
		System.out.println(connSource.UVMSStore.Nodes.isAreaActive('X', networkNodeTarget));
		
		
		com.orsyp.api.node.Node node = connSource.DU6Store.Nodes.getDuNodeByName("AGENT_PRD_01");
		
		
		//connSource.DU6Store.Crons.getCronsAsList();
		//check();
		System.exit(0);
	
	}

}
