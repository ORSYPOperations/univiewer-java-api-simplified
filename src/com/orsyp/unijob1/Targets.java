package com.orsyp.unijob1;

import com.orsyp.UniverseException;
import com.orsyp.api.Context;
import com.orsyp.api.mu.Mu;
import com.orsyp.api.mu.MuFilter;
import com.orsyp.api.mu.MuId;
import com.orsyp.api.mu.MuItem;
import com.orsyp.api.mu.MuList;
import com.orsyp.api.syntaxerules.UnijobSyntaxRules;
import com.orsyp.std.MuListStdImpl;
import com.orsyp.std.MuStdImpl;

public class Targets extends GenericObjects  {
	private Context GlobalCtx;

	public Targets (com.orsyp.stores.UVAPIWrapper conn, Context GlobalCtx){
		super(conn);
		this.GlobalCtx = GlobalCtx;
	}
	private Context getContext(){
		return GlobalCtx;
	}
	public void deleteTarget(MuItem item) throws UniverseException{
		MuId id = item.getIdentifier();
		Mu obj = new Mu(getContext(),id);
		obj.setImpl(new MuStdImpl());
		obj.getIdentifier().setSyntaxRules(UnijobSyntaxRules.getInstance());
	    obj.extract();
	    obj.delete();
	}
	public void createTarget(String MuName, String NodeName, String Username, String MuLabel) throws UniverseException{
		Mu obj = new Mu(getContext(),MuId.create(MuName));
		obj.setName(MuName);
		obj.setUserName(Username);
		obj.setNodeName(NodeName);
		obj.setLabel(MuLabel);
		obj.setImpl(new MuStdImpl());
		obj.getIdentifier().setSyntaxRules(UnijobSyntaxRules.getInstance());

	    obj.create();
	}
	public Mu getTarget(MuItem item) throws UniverseException{
		MuId id = item.getIdentifier();
		Mu obj = new Mu(getContext(),id);
		obj.setImpl(new MuStdImpl());
		obj.getIdentifier().setSyntaxRules(UnijobSyntaxRules.getInstance());
	    obj.extract();
	    return obj;
	}
	public MuList getTargetList() throws Exception {  
        MuFilter filter = new MuFilter("*","*");
        MuList list = new MuList(getContext(), filter);
		list.setSyntaxRules(UnijobSyntaxRules.getInstance());
        list.setImpl(new MuListStdImpl());
        list.extract();
        return list;
	}
}
