package com.orsyp.unijob1;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Vector;

import com.orsyp.SyntaxException;
import com.orsyp.UniverseException;
import com.orsyp.api.Context;
import com.orsyp.api.Target;
import com.orsyp.api.job.Job;
import com.orsyp.api.job.JobFilter;
import com.orsyp.api.job.JobId;
import com.orsyp.api.job.JobItem;
import com.orsyp.api.job.JobList;
import com.orsyp.api.mu.MuId;
import com.orsyp.api.mu.MuItem;
import com.orsyp.api.mu.MuList;
import com.orsyp.api.rule.KDayAuthorization;
import com.orsyp.api.rule.KmeleonPattern;
import com.orsyp.api.rule.MonthAuthorization;
import com.orsyp.api.rule.PositionsInPeriod;
import com.orsyp.api.rule.Rule.PeriodTypeEnum;
import com.orsyp.api.rule.WeekAuthorization;
import com.orsyp.api.rule.KmeleonPattern.KDayType;
import com.orsyp.api.rule.PositionsInPeriod.SubPeriodType;
import com.orsyp.api.rule.YearAuthorization;
import com.orsyp.api.syntaxerules.UnijobSyntaxRules;
import com.orsyp.api.task.LaunchHourPattern;
import com.orsyp.api.task.SimpleLaunch;
import com.orsyp.api.task.TaskImplicitData;
import com.orsyp.api.task.TaskPlanifiedData;
import com.orsyp.schedconv.SchedConv;
import com.orsyp.schedconv.SchedFile;
import com.orsyp.schedconv.SchedImplFactory;
import com.orsyp.schedconv.SchedLine;
import com.orsyp.schedconv.std.cron.SchedStdCronImplFactory;
import com.orsyp.std.ClientConnectionManager;
import com.orsyp.std.ConnectionFactory;
import com.orsyp.std.EnvironmentStdImpl;
import com.orsyp.std.JobListStdImpl;
import com.orsyp.std.JobStdImpl;
import com.orsyp.std.NodeConnectionFactory;
import com.orsyp.util.DateTools;
import com.orsyp.util.Run;
import com.orsyp.utils.misc;
import com.orsyp.schedconv.SchedJob;
import com.orsyp.schedconv.SchedVariable;
import com.orsyp.api.job.JobStatus;

public class Jobs extends GenericObjects  {
	private Context GlobalCtx;

	public Jobs (com.orsyp.stores.UVAPIWrapper conn, Context GlobalCtx){
		super(conn);
		this.GlobalCtx = GlobalCtx;
	}
	private Context getContext(){
		return GlobalCtx;
	}
	public Job getJob(JobItem item) throws UniverseException{
		JobId id = item.getIdentifier();
		Job obj = new Job(getContext(),id);
		obj.setImpl(new JobStdImpl());
		obj.getIdentifier().setSyntaxRules(UnijobSyntaxRules.getInstance());
	    obj.extract();
	    return obj;
	}
	public JobList getJobList() throws Exception {  
		
        JobFilter filter = new JobFilter();
        JobList list = new JobList(getContext(), filter);
        list.setSyntaxRules(UnijobSyntaxRules.getInstance());
        list.setImpl(new JobListStdImpl());
        list.extract();
        return list;
	}
public JobList getJobListWithFilter(String jobName) throws Exception {  
		
        JobFilter filter = new JobFilter();
        filter.setName(jobName);
        JobList list = new JobList(getContext(), filter);
        list.setSyntaxRules(UnijobSyntaxRules.getInstance());
        list.setImpl(new JobListStdImpl());
        list.extract();
        return list;
	}
public void setInteractiveBoxOnJob(Job object,boolean isInteractive) throws UniverseException{
	object.setInteractive(isInteractive);
	object.update();
}

	public TaskImplicitData[] getDayPatternList(Job obj){
		TaskPlanifiedData TASKPLANDATA=(TaskPlanifiedData) obj.getPlanification();
		TaskImplicitData[] TASKIMPDATA = TASKPLANDATA.getImplicitData();
		return TASKIMPDATA;
	}
	public TaskPlanifiedData getLaunchPatternsData(Job obj){
		TaskPlanifiedData TASKPLANDATA=(TaskPlanifiedData) obj.getPlanification();
		return TASKPLANDATA;
	}
	public void getDayPatternInformation(TaskImplicitData rule){
		// this method is provided only for information purposes, and has very little purpose besides illustrating how schd information is available through the API..
		KmeleonPattern myPattern = rule.getKmeleonPattern();
		
		PositionsInPeriod pos = myPattern.getPositionsInPeriod();
		
		System.out.println("Position: "+pos.getPositionsPattern()+" - Every: "+pos.getType()+" - From Beginning: "+pos.isForward()+", at: "+myPattern.getSimpleLaunch().getLaunchTime());

		WeekAuthorization myWeek = rule.getWeekAuthorization();
		MonthAuthorization myMonth = rule.getMonthAuthorization();
		
		System.out.println("Week Authorization Mask :"+misc.getBooleanTableAsString(myWeek.getWorkedDays()));
		System.out.println("Month Authorization Mask:"+misc.getBooleanTableAsString(myMonth.getAuthorizations()));
			
		KDayAuthorization myAuthOpen = myPattern.getDayAuthorization(KDayType.OPEN);
		KDayAuthorization myAuthClosed = myPattern.getDayAuthorization(KDayType.CLOSED);
		KDayAuthorization myAuthHoliday = myPattern.getDayAuthorization(KDayType.HOLIDAY);

		System.out.println("Open Day Shift:"+myAuthOpen.getType());
		System.out.println("Closed Day Shift:"+myAuthClosed.getType());
		System.out.println("Holiday Day Shift:"+myAuthHoliday.getType());

	}
	public void deleteJob(JobItem item) throws UniverseException{
		JobId id = item.getIdentifier();
		Job obj = new Job(getContext(),id);
		obj.setImpl(new JobStdImpl());
		obj.getIdentifier().setSyntaxRules(UnijobSyntaxRules.getInstance());
	    obj.extract();
	    obj.delete();
	}
	public void duplicateJob(String NewJobName, Job job) throws Exception{
		MuList list = super.getUVAPIWrapper().UJ1Store.Targets.getTargetList();
		MuItem item = null;
		for(int i=0;i<list.getCount();i++){
			if(list.get(i).getId().equals(job.getTargetId())){item=list.get(i);}
		}
		MuId id = item.getIdentifier();
		JobId newid = JobId.create(NewJobName,id);
		Job newJob = job;
		newJob.setIdentifier(newid);
		newJob.setName(NewJobName);
		newJob.create();
	}
	public List<SchedLine> getCronsAsList(String user, String inputFile) throws UniverseException, IOException{
		
		String sched = "cron";

		
		   ConnectionFactory connFactory = NodeConnectionFactory
                   .getInstance(super.getUVAPIWrapper().central);

           getContext().setImpl(new EnvironmentStdImpl());

           SchedStdCronImplFactory impl = new SchedStdCronImplFactory(connFactory, true,
                       false, false, false);
   
           // Operations
           SchedConv schedConv = new SchedConv(getContext(), impl);
           schedConv.isAuthorized();
        //   schedConv.getFileList();
           for (SchedFile file:schedConv.getFileList()) {
        	   
        	   for (SchedJob job:file.getSchedJobList()) {
        		   job.printAsLine();
        		   
        	   }
        	   
           }

		return null;
	}
	public SchedJob buildJobFromLine(SchedLine schedLine) throws UniverseException, IOException{
		NodeConnectionFactory factory = NodeConnectionFactory.getInstance(super.getUVAPIWrapper().central);
		ClientConnectionManager.setDefaultFactory(factory);
		SchedImplFactory mySchedFact = new SchedStdCronImplFactory(factory, false, true, true, true);
		SchedJob mySchedJob = new SchedJob(getContext(),mySchedFact,schedLine,null);
		ArrayList<SchedVariable> schedVarList = new ArrayList();
		SchedVariable myHome = new SchedVariable("HOME", "/home/bsa/", "Text");
		SchedVariable myShell = new SchedVariable("SHELL", "/usr/ksh/", "Text");

		schedVarList.add(myHome);
		schedVarList.add(myShell);
		mySchedJob.setSchedVarList(schedVarList);

		mySchedJob.buildUniObj();
		
		//mySchedJob.print(mySchedJob.getJob());
		Job theJob = mySchedJob.getJob();
		theJob.setComment("");
		theJob.setStatus(JobStatus.DISABLED);
		mySchedJob.setJob(theJob);
		
		return mySchedJob;
	}
	public Job getJobFromSchedJob(SchedJob sjob) throws SyntaxException{
		Job myJob = sjob.getJob();
		myJob.setTargetName("XXX_TARGET_DEFAULT");		
		return myJob;
	}
	public Job getJobFromSchedJob(SchedJob sjob, Target target) throws SyntaxException{
		Job myJob = sjob.getJob();
		myJob.setTargetName(target.getTargetName());		
		return myJob;
	}
	/**
	 * 
	 * The method below isnt particularly useful.. But it illustrates quite well how scheduling patterns are organized in Java Objects!
	 * It serves mostly as a reference to people who would want to use the scheduling patterns 
	 *
	 */
	public void displayLaunchPatterns(Job job){
		TaskPlanifiedData TASKPLANDATA = (TaskPlanifiedData) job.getPlanification();
		TaskImplicitData[] TASKIMPDATA = TASKPLANDATA.getImplicitData();
		
		for (int l=0;l<TASKIMPDATA.length;l++){
			int index = l+1;
			System.out.println("\nPattern Number "+index+" For Job: "+job.getName()+"");
			System.out.println("---------------------------------------------------");
			KmeleonPattern KPattern = TASKIMPDATA[l].getKmeleonPattern();
			
			int pNum = TASKIMPDATA[l].getPeriodNumber();
			PeriodTypeEnum pType = TASKIMPDATA[l].getPeriodType();
			String PosInPeriod = KPattern.getPositionsInPeriod().getPositionsPattern();
			SubPeriodType SubPerType = KPattern.getPositionsInPeriod().getType();
			boolean isForward = KPattern.getPositionsInPeriod().isForward();

			System.out.println("Every: "+pNum+" "+pType+" On: "+PosInPeriod+" "+SubPerType+" Forward: "+isForward);
			
			SimpleLaunch LaunchTime = KPattern.getSimpleLaunch();
			
			//System.out.println("At: " + LaunchTime.getLaunchTime().substring(0, 4));
			
			System.out.println("End Date: "+KPattern.getValidTo());
			
			WeekAuthorization WeekAuth = TASKIMPDATA[l].getWeekAuthorization();
			MonthAuthorization MonthAuth = TASKIMPDATA[l].getMonthAuthorization();
			YearAuthorization YearAuth = TASKIMPDATA[l].getYearAuthorization();
		
			System.out.println("Days of the Week: "+Arrays.toString(WeekAuth.getWorkedDays()));
			//System.out.println("Days of the Month [useless in UJ]: "+Arrays.toString(MonthAuth.getAuthorizations()));
			System.out.println("Months of the Year: "+Arrays.toString(YearAuth.getAuthorizations()));

			Hashtable<KDayType, KDayAuthorization> DayAuth = TASKIMPDATA[l].getKmeleonPattern().getDayAuthorizations();
			
			Iterator<Entry<KDayType, KDayAuthorization>> it = DayAuth.entrySet().iterator();
			
			while(it.hasNext()){
				
				Entry<KDayType, KDayAuthorization> type = it.next();
				String StatusOfDay = "";
				if(type.getValue().isAuthorized()){StatusOfDay = "Authorized";}
				if(type.getValue().isExcluded()){StatusOfDay = "Excluded";}	
				if(type.getValue().isExcludedWithShift()){
					
					StatusOfDay = "Excluded with shift: ";
					StatusOfDay = StatusOfDay + type.getValue().getDayShift().getShiftNumber();
					StatusOfDay = StatusOfDay + " " + type.getValue().getDayShift().getOffsetDayType().name();
				}
				
				System.out.println(type.getKey() +" : "+ StatusOfDay);
			}
			System.out.println("---------------------------------------------------");
		}	
	}
	// the following method should be developed..
	public String GetCronPatternForJob(Job job){
		
		final String UNSUPPORTED="UNSUPPORTED";
		
		TaskPlanifiedData TASKPLANDATA = (TaskPlanifiedData) job.getPlanification();
		TaskImplicitData[] tidTable = TASKPLANDATA.getImplicitData();
		//LaunchHourPattern[] lhpTable = TASKPLANDATA.getLaunchHourPatterns();
		LaunchHourPattern[] slTable = TASKPLANDATA.getLaunchHourPatterns();
		
		int numberOfDayPatterns = tidTable.length;
		int numberOfLaunchTimes = slTable.length;
		
		if(tidTable.length > 1){return UNSUPPORTED + ": Too Many Day Patterns";}
		if(numberOfLaunchTimes>1){return UNSUPPORTED + ": Too Many Launch Times";}
		if(numberOfDayPatterns>1){return UNSUPPORTED + ": Too Many Day Patterns";}
		
		TaskImplicitData tid = tidTable[0];
		
		ArrayList<String> DaySingletons = new ArrayList<String>(); 	
		
		// Converting Days in Month
		DaySingletons.add(tid.getKmeleonPattern().getPositionsInPeriod().getPositionsPattern());
		
		// Converting Months in Year
		boolean[] mAuth = tid.getYearAuthorization().getAuthorizations();
		String Months = "";
			// little silly trick below to retrieve the proper numbering for months..
		for(int i=1;i<mAuth.length+1;i++){
			if(mAuth[i-1]){
				Months = Months +i+",";
			}
		}
		boolean[] wAuth = tid.getWeekAuthorization().getWorkedDays();
		String Days = "";
		// little silly trick below to retrieve the proper numbering for months..
	for(int i=1;i<wAuth.length+1;i++){
		if(wAuth[i-1]){
			Days = Days +i+",";
		}
	}
	
	String LaunchHour = slTable[0].getStartTime().substring(0, 2);
	String LaunchMin = slTable[0].getStartTime().substring(2, 4);
	String DaysInMonth = tid.getKmeleonPattern().getPositionsInPeriod().getPositionsPattern().replace(" ", "");
	String MonthsInYear = Months.replace(" ", "");
	String DaysInWeek = Days.replace(" ", "");
	

		return LaunchMin +" "+ LaunchHour + " " + DaysInMonth +" " + MonthsInYear + " " + DaysInWeek;
	}
	public Vector <Run> GetRunsWithinWindow(Job job, String startdate, String starttime, String enddate, String endtime) throws UniverseException {

		Vector <Date> allDates = new Vector<Date>();
		
		// 1- Start Date and End Dates without timestamp (hour is 0000)
		Date tmpD=DateTools.toDate(startdate);
		Date tmpE=DateTools.toDate(enddate);
		
		// 2- we add minutes and hours based on what is passed by parameters
		Date tmpDFULL=DateTools.toDate(startdate+starttime);
		Date tmpEFULL=DateTools.toDate(enddate+endtime);
		
		// BSA - Dec 14th 2012 - Bug fix, shifting start time and end time by + and - 1 min 
		Date tmpDFULLA=(Date) tmpDFULL.clone();
		tmpDFULLA.setTime(tmpDFULL.getTime() - 60 * 1000 + 1 * 24 * 60 * 60 * 1000);
		
		Date tmpEFULLA=(Date) tmpEFULL.clone();
		tmpEFULLA.setTime(tmpEFULL.getTime() + 60 * 1000 + 1 * 24 * 60 * 60 * 1000);
		
		
		tmpD.setTime(tmpD.getTime() + 1 * 24 * 60 * 60 * 1000);

		// 3- We create a table of dates, we put the begin date first then.. 
		allDates.add(new Date(tmpD.getTime()));
		
		// 4- while begin date and end date not equal..
		while(tmpD.before(tmpE)){
		// 5- We add one day and then we add it to the table
			tmpD.setTime(tmpD.getTime() + 1 * 24 * 60 * 60 * 1000);
			allDates.add(new Date(tmpD.getTime()));
		}

		// 6- at this point we have a table of dates. Ex between Jan 1st and Jan 3rd, we have a table of 3 dates, Jan 1st, 2nd and 3rd 
		Vector<Run> allValidExec = new Vector<Run>();
		Vector<Run> allRuns = new Vector<Run>();
		
		if(allDates.size()>0){

			for (int dn=0;dn<allDates.size();dn++){

				//For each Day, we declare two dates, one from beginning of day (St) one for end of day(En)
				Date St = (Date) allDates.get(dn).clone();
				Date En = (Date) allDates.get(dn).clone(); 

				/**
				 * The SimulateFull method returns Runs with a one day offset.. if a run is scheduled on a monday, simulateFull will return an equivalent run on... Tuesday
				 *    THEREFORE:
				 * We Offset EVERYTHING by one day because the simulation does not work properly:
				 * Original Start Date: Monday 1st, 0000 => New Etart Date: Tuesday 2nd, 0000
				 * Original End Date: Monday 1st, 0000 => New End Date: Wednesday 3nd, 0000
				 */
				
				St.setTime(St.getTime());//  + 1 * 24 * 60 * 60 * 1000); //+24hr
				En.setTime(En.getTime()   + 1 * 23 * 60 * 60 * 1000 + 59*60*1000);// + 1 * 24 * 60 * 60 * 1000); //+24hr + 23hr59min

				Run[] myRuns = job.simulateFull(
						St,
						En
						).getRuns(St);
				
				// Changing into a vector.. easier to manipulate
				allRuns = new Vector<Run>(Arrays.asList(myRuns));

			}
			for (int myK=0;myK<allRuns.size();myK++){
				Date tmpDate = allRuns.get(myK).getTime();
				tmpDate.setTime(tmpDate.getTime());// - 1 * 24 * 60 * 60 * 1000);
				if(tmpDate.after(tmpDFULLA) && tmpDate.before(tmpEFULLA)){
					allValidExec.add(allRuns.get(myK));
				}
			}
				
			}
		if(allValidExec.isEmpty()){
			return null;
		}
		else{
			return allValidExec;
		}
	}
	
}
