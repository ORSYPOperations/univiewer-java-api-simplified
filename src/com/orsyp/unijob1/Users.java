package com.orsyp.unijob1;

import com.orsyp.UniverseException;
import com.orsyp.api.Context;
import com.orsyp.api.syntaxerules.UnijobSyntaxRules;
import com.orsyp.api.user.User;
import com.orsyp.api.user.UserFilter;
import com.orsyp.api.user.UserId;
import com.orsyp.api.user.UserItem;
import com.orsyp.api.user.UserList;
import com.orsyp.api.user.UserType;
import com.orsyp.std.UserListStdImpl;
import com.orsyp.std.UserStdImpl;

public class Users extends GenericObjects  {
	private Context GlobalCtx;

	public Users (com.orsyp.stores.UVAPIWrapper conn, Context GlobalCtx){
		super(conn);
		this.GlobalCtx = GlobalCtx;
	}
	private Context getContext(){
		return GlobalCtx;
	}
	public UserList getUnijobUserList() throws Exception {  
        UserFilter filter = new UserFilter("*");
        UserList list = new UserList(getContext(), filter);
        list.setImpl(new UserListStdImpl());
        list.setSyntaxRules(UnijobSyntaxRules.getInstance());
        list.extract();
        return list;
	}
	public User getUser(UserItem item) throws UniverseException{
		UserId id = item.getIdentifier();
		User obj = new User(getContext(),id);
		obj.setImpl(new UserStdImpl());
		obj.getIdentifier().setSyntaxRules(UnijobSyntaxRules.getInstance());
	    obj.extract();
	    return obj;
	}
	public void deleteUser(UserItem item) throws UniverseException{
		UserId id = item.getIdentifier();
		User obj = new User(getContext(),id);
		obj.setImpl(new UserStdImpl());
		obj.getIdentifier().setSyntaxRules(UnijobSyntaxRules.getInstance());
	    obj.extract();
	    obj.delete();
	}
	public void createUser(String userName, String AuthCode) throws Exception{
		String Profile = "PROFADM";
		UserList list = getUnijobUserList();
		for(int i=0;i<list.getCount();i++){
			System.out.println(list.get(i).getAuthorCode()+":"+list.get(i).getProfile());
		}
		User obj = new User(getContext(),UserId.create(userName));
		obj.setName(userName);
		obj.setProfile(Profile);
		obj.setAuthorCode(AuthCode);
		obj.setUserType(UserType.Both);
		obj.setImpl(new UserStdImpl());
		obj.getIdentifier().setSyntaxRules(UnijobSyntaxRules.getInstance());

	    obj.create();
	}
}
