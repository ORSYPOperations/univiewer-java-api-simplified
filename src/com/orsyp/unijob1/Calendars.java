package com.orsyp.unijob1;

import com.orsyp.UniverseException;
import com.orsyp.api.Context;
import com.orsyp.api.calendar.CalendarFilter;
import com.orsyp.api.calendar.CalendarId;
import com.orsyp.api.calendar.CalendarItem;
import com.orsyp.api.calendar.CalendarList;
import com.orsyp.api.syntaxerules.UnijobSyntaxRules;
import com.orsyp.std.CalendarStdImpl;
import com.orsyp.std.KalendarListStdImpl;
import com.orsyp.std.KalendarStdImpl;

public class Calendars extends GenericObjects {
	private Context GlobalCtx;

	public Calendars (com.orsyp.stores.UVAPIWrapper conn, Context GlobalCtx){
		super(conn);
		this.GlobalCtx = GlobalCtx;
	}
	private Context getContext(){
		return GlobalCtx;
	}
	public CalendarList getCalendarList() throws Exception {  
		CalendarFilter filter = new CalendarFilter();
		CalendarList list = new CalendarList(getContext(), filter);
		list.setSyntaxRules(UnijobSyntaxRules.getInstance());
        list.setImpl(new KalendarListStdImpl());
        list.extract();
        return list;
	}
	/**
	 * Old implementation for UJ (before syntaxRules were implemented
	 * @deprecated use {@link getCalendar(CalendarItem item)} instead.  
	 */
	@Deprecated
	public com.orsyp.api.calendar.Calendar getKalendar(CalendarItem item) throws UniverseException{
		CalendarId id = item.getIdentifier();
		com.orsyp.api.calendar.Calendar obj = new com.orsyp.api.calendar.Calendar(getContext(),id);
		obj.setImpl(new KalendarStdImpl());
		obj.getIdentifier().setSyntaxRules(UnijobSyntaxRules.getInstance());
	    obj.extract();
	    return obj;
	}
	public com.orsyp.api.calendar.Calendar getCalendar(CalendarItem item) throws UniverseException{
		CalendarId id = item.getIdentifier();
		id.setSyntaxRules(UnijobSyntaxRules.getInstance());
		com.orsyp.api.calendar.Calendar obj = new com.orsyp.api.calendar.Calendar(getContext(),id);
		//obj.getIdentifier().setSyntaxRules(UnijobSyntaxRules.getInstance());
		obj.setImpl(new CalendarStdImpl());
	    obj.extract();
	    return obj;
	}
	public void deleteCalendar(CalendarItem item) throws UniverseException{
		CalendarId id = item.getIdentifier();
		
		com.orsyp.api.calendar.Calendar obj = new com.orsyp.api.calendar.Calendar(getContext(),id);
		obj.setImpl(new KalendarStdImpl());
		obj.getIdentifier().setSyntaxRules(UnijobSyntaxRules.getInstance());
	    obj.extract();
	    obj.delete();
	}
}
