package com.orsyp.unijob1;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.orsyp.UniverseException;
import com.orsyp.api.Context;
import com.orsyp.api.central.ExecutionInfoFilter;
import com.orsyp.api.central.ExecutionInfoItem;
import com.orsyp.api.central.ExecutionInfoList;
import com.orsyp.api.execution.Execution;
import com.orsyp.api.execution.ExecutionFilter;
import com.orsyp.api.execution.ExecutionId;
import com.orsyp.api.execution.ExecutionList;
import com.orsyp.api.job.Job;
import com.orsyp.api.launch.Launch;
import com.orsyp.api.launch.LaunchId;
import com.orsyp.api.security.Operation;
import com.orsyp.api.spec.ExecutionListImpl;
import com.orsyp.api.syntaxerules.OwlsSyntaxRules;
import com.orsyp.std.ExecutionObjectListStdImpl;
import com.orsyp.std.ExecutionStdImpl;
import com.orsyp.std.KLaunchStdImpl;
import com.orsyp.std.LaunchStdImpl;
import com.orsyp.std.UnijobExecutionInfoListStdImpl;

public class Executions  extends GenericObjects {
	private Context GlobalCtx;

	public Executions (com.orsyp.stores.UVAPIWrapper conn, Context GlobalCtx){
		super(conn);
		this.GlobalCtx = GlobalCtx;
	}
	private Context getContext(){
		return GlobalCtx;
	}
	public ExecutionList getExecutionListTemp() throws UniverseException{
		ExecutionFilter filter = new ExecutionFilter();
		filter.setMuName("*");
		filter.setMuId("*");
		ExecutionList list = new ExecutionList(getContext(),filter);
		list.setSyntaxRules(OwlsSyntaxRules.getInstance());
		list.setImpl((ExecutionListImpl) new ExecutionObjectListStdImpl());
		
		list.extract();
		return list;
	}
	public ExecutionInfoList getExecutionList() throws UniverseException{
	    ExecutionInfoFilter jobFilter = new ExecutionInfoFilter();
        ExecutionInfoList executionInfoList = new ExecutionInfoList(getContext(), jobFilter);
        executionInfoList.setImplUnijob(new UnijobExecutionInfoListStdImpl());
        executionInfoList.setSyntaxRules(OwlsSyntaxRules.getInstance());
        executionInfoList.pressOut(1,Operation.DISPLAY);
        
        return executionInfoList;
	}
	public String LaunchJobNow(Job job) throws ParseException, UniverseException{
		// why 20hrs? because the API rests the time to GMT everytime ...
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		//sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
		Date start = new Date(System.currentTimeMillis()-20*60*60*1000);
		String sDate= sdf.format(start);
		Date end = new Date(System.currentTimeMillis()+20*60*60*1000);
		String eDate= sdf.format(end);
		String numlanc = LaunchJob(job,sDate.substring(0, 8),sDate.substring(0, 8),sDate.substring(8, 12),eDate.substring(0, 8),eDate.substring(8, 12));
		return numlanc;
	}
	public String LaunchJob(Job job, String processingDate, String startDate, String startTime, String endDate, String endTime) throws ParseException, UniverseException{
		String jobVersion="000"; // in Unijob, it is always 000
		String numlanc="0000000"; // arbitrary, it is dynamically generated during the call to Launch.create() method below
		LaunchId myId = LaunchId.createWithName(null,null,job.getName(),jobVersion,job.getTargetName(),numlanc);
		 Launch myLaunch = new Launch(getContext(), myId);
		 myLaunch.setImpl(new LaunchStdImpl());
		 
		 myLaunch.setUprocName(job.getName());
		 myLaunch.setProcessingDate(processingDate);
		 myLaunch.setUserName(job.getUserName());
		 myLaunch.setQueue("SYS_BATCH");
		 myLaunch.setPriority("1");
		 myLaunch.setPrinter("IMPR");

		 SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmss");
		 //sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
         Date beginDate = sdf.parse(startDate+startTime+"00");
         Date finDate =   sdf.parse(endDate+endTime+"00");
		 myLaunch.setBeginDate(beginDate);
		 myLaunch.setEndDate(finDate);
		 myLaunch.create();
		 return myLaunch.getNumlanc();
	}
	public Launch getLaunch(String jobname,String targetname, String numlanc) throws UniverseException{
		 String jobVersion="000";
		 //String numlanc="0000000";
		 LaunchId myId = LaunchId.createWithName(null,null,jobname,jobVersion,targetname,numlanc);
		 Launch myLaunch = new Launch(getContext(), myId);
		 myLaunch.setImpl(new KLaunchStdImpl());
		 myLaunch.extract();
		 return myLaunch;

	}
	public Execution getExecution(ExecutionInfoItem item) throws UniverseException{
		 
		// ExecutionId myId = ExecutionId.create(null,null,item.getJobName(),item.getJobId(),item.getTargetName(),item.getTargetId(),"0",String.valueOf(item.getLastNumproc())); // numproc as numlanc?
ExecutionId myId = ExecutionId.create("",null,"LALA",item.getUprocId(),item.getTargetName(),item.getTargetId(),null,String.valueOf(item.getLastNumproc()));
			
		 myId.setSyntaxRules(OwlsSyntaxRules.getInstance());
		 myId.setUprocName(item.getJobName());
		 myId.setUprocId(""+item.getIdentifier().getNumlanc());

		 Execution myExec = new Execution(getContext(), myId);
		 myExec.setImpl(new ExecutionStdImpl());
		// System.out.println("DEBUG:"+myExec.toString());
		 myExec.extract();
		 return myExec; 
	}



}
