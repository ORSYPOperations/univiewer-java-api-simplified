package com.orsyp.du5;

import com.orsyp.api.Context;
import com.orsyp.api.node.NodeFilter;
import com.orsyp.api.node.NodeList;
import com.orsyp.api.settings.NodeSettingItem;
import com.orsyp.du6.GenericObjects;
import com.orsyp.std.NodeListStdImpl;

public class Nodes  extends GenericObjects {
	private Context GlobalCtx;

	public Nodes (com.orsyp.stores.UVAPIWrapper conn, Context GlobalCtx){
		super(conn);
		this.GlobalCtx = GlobalCtx;
	}
	private Context getContext(){
		return GlobalCtx;
	}
	
	public NodeSettingItem getNodeSetting(NodeSettingItem item) throws Exception{  
		return item;
	}
	
	
	
	public NodeList getDuNodeList() throws Exception {  
        NodeFilter filter = new NodeFilter("*");
        NodeList list = new NodeList(getContext(), filter);
        list.setImpl(new NodeListStdImpl());
        list.extract();
        return list;
	}
}
