package com.orsyp.du5;

import com.orsyp.UniverseException;
import com.orsyp.api.Context;
import com.orsyp.api.engine.Engine;
import com.orsyp.api.engine.EngineId;
import com.orsyp.api.engine.EngineItem;
import com.orsyp.api.engine.EngineList;
import com.orsyp.du6.GenericObjects;
import com.orsyp.std.EngineListStdImpl;
import com.orsyp.std.EngineStdImpl;

public class Engines  extends GenericObjects {
	private Context GlobalCtx;

	public Engines (com.orsyp.stores.UVAPIWrapper conn, Context GlobalCtx){
		super(conn);
		this.GlobalCtx = GlobalCtx;
	}
	private Context getContext(){
		return GlobalCtx;
	}
	
	
	public EngineList getEngineList() throws UniverseException{
		EngineList list = new EngineList(getContext());
		list.setImpl(new  EngineListStdImpl());
		list.extract();
		return list;
	}



	public Engine getEngine(EngineItem item) throws UniverseException{
		EngineId id = item.getIdentifier();
		Engine obj = new Engine(getContext(),id);
		obj.setImpl(new  EngineStdImpl());
		//obj.getIdentifier().setSyntaxRules(ClassicSyntaxRules.getInstance());
	    obj.extract();
	    return obj;
	}
}
