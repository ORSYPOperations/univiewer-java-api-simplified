package com.orsyp.du5;

import com.orsyp.UniverseException;
import com.orsyp.api.Context;
import com.orsyp.api.event.JobEvent;
import com.orsyp.api.event.JobEventId;
import com.orsyp.api.event.JobEventItem;
import com.orsyp.api.event.JobEventList;
import com.orsyp.api.execution.Execution;
import com.orsyp.api.execution.ExecutionId;
import com.orsyp.api.execution.ExecutionItem;
import com.orsyp.api.execution.ExecutionList;
import com.orsyp.api.history.ExecutionHistory;
import com.orsyp.api.history.ExecutionHistoryId;
import com.orsyp.api.history.ExecutionHistoryItem;
import com.orsyp.api.launch.Launch;
import com.orsyp.api.launch.LaunchId;
import com.orsyp.api.launch.LaunchItem;
import com.orsyp.api.launch.LaunchList;
import com.orsyp.api.syntaxerules.ClassicSyntaxRules;
import com.orsyp.du6.GenericObjects;
import com.orsyp.std.ExecutionListStdImpl;
import com.orsyp.std.ExecutionStdImpl;
import com.orsyp.std.JobEventListStdImpl;
import com.orsyp.std.JobEventStdImpl;
import com.orsyp.std.LaunchListStdImpl;
import com.orsyp.std.LaunchStdImpl;
import com.orsyp.std.history.ExecutionHistoryStdImpl;

public class Executions  extends GenericObjects {
	private Context GlobalCtx;

	public Executions (com.orsyp.stores.UVAPIWrapper conn, Context GlobalCtx){
		super(conn);
		this.GlobalCtx = GlobalCtx;
	}
	private Context getContext(){
		return GlobalCtx;
	}
	public JobEvent getJobEvent(JobEventItem item) throws UniverseException{
		JobEventId id = item.getIdentifier();
		JobEvent obj = new JobEvent(getContext(),id);
		obj.setImpl(new  JobEventStdImpl());
		obj.getIdentifier().setSyntaxRules(ClassicSyntaxRules.getInstance());
	    obj.extract();
	    return obj;
	}

	public Execution getExecution(ExecutionItem item) throws Exception{  
		ExecutionId id = item.getIdentifier();
		Execution obj = new Execution(getContext(),id);
		obj.setImpl(new  ExecutionStdImpl());
		obj.getIdentifier().setSyntaxRules(ClassicSyntaxRules.getInstance());
	    obj.extract();
	    return obj;
	}
	public ExecutionHistory getExecutionHistory(ExecutionHistoryItem item) throws Exception{  
		ExecutionHistoryId id = item.getIdentifier();
		ExecutionHistory obj = new ExecutionHistory(getContext(),id);
		obj.setImpl(new  ExecutionHistoryStdImpl());
		obj.getIdentifier().setSyntaxRules(ClassicSyntaxRules.getInstance());
	    obj.extract();
	    return obj;
	}

	public Launch getLaunch(LaunchItem item) throws Exception{  
		LaunchId id = item.getIdentifier();
		Launch obj = new Launch(getContext(),id);
		obj.setImpl(new  LaunchStdImpl());
		obj.getIdentifier().setSyntaxRules(ClassicSyntaxRules.getInstance());
	    obj.extract();
	    return obj;
	}
	public JobEventList getJobEventList() throws UniverseException{
		JobEventList list = new JobEventList(getContext());
		list.setImpl(new  JobEventListStdImpl());
		list.extract();
		return list;
	}
	public ExecutionList getExecutionList() throws UniverseException{
		ExecutionList list = new ExecutionList(getContext());
		list.setImpl(new  ExecutionListStdImpl());
		list.extract();
		return list;
	}



	public LaunchList getLaunchList() throws UniverseException{
		LaunchList list = new LaunchList(getContext());
		list.setImpl(new  LaunchListStdImpl());
		list.extract();
		return list;
	}
}
