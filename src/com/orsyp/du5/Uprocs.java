package com.orsyp.du5;

import com.orsyp.api.Context;
import com.orsyp.api.syntaxerules.ClassicSyntaxRules;
import com.orsyp.api.syntaxerules.OwlsSyntaxRules;
import com.orsyp.api.uproc.Uproc;
import com.orsyp.api.uproc.UprocFilter;
import com.orsyp.api.uproc.UprocId;
import com.orsyp.api.uproc.UprocItem;
import com.orsyp.api.uproc.UprocList;
import com.orsyp.api.uprocclass.UprocClass;
import com.orsyp.api.uprocclass.UprocClassFilter;
import com.orsyp.api.uprocclass.UprocClassId;
import com.orsyp.api.uprocclass.UprocClassItem;
import com.orsyp.api.uprocclass.UprocClassList;
import com.orsyp.du5.GenericObjects;
import com.orsyp.std.UprocClassListStdImpl;
import com.orsyp.std.UprocClassStdImpl;
import com.orsyp.std.UprocListStdImpl;
import com.orsyp.std.UprocStdImpl;

public class Uprocs  extends GenericObjects {
	
	private Context GlobalCtx;

	public Uprocs (com.orsyp.stores.UVAPIWrapper conn, Context GlobalCtx){
		super(conn);
		this.GlobalCtx = GlobalCtx;
	}
	private Context getContext(){
		return GlobalCtx;
	}

	public UprocList getUprocList() throws Exception {  
        UprocFilter filter = new UprocFilter("*");
        UprocList list = new UprocList(getContext(), filter);
        //list.setSyntaxRules(ClassicSyntaxRules.getInstance());
        list.setImpl(new UprocListStdImpl());
        list.extract();
        return list;
	}
	public UprocClass getUprocClass(UprocClassItem item) throws Exception{  
		UprocClassId id = item.getIdentifier();
		UprocClass obj = new UprocClass(getContext(),id);
		obj.setImpl(new UprocClassStdImpl());
		obj.getIdentifier().setSyntaxRules(ClassicSyntaxRules.getInstance());
	    obj.extract();
	    return obj;
	}
	public Uproc getUproc(UprocItem item) throws Exception{  
		UprocId id = item.getIdentifier();
		Uproc obj = new Uproc(getContext(),id);
		obj.setImpl(new UprocStdImpl());
		obj.getIdentifier().setSyntaxRules(ClassicSyntaxRules.getInstance());
	    obj.extract();
	    return obj;
	}
	public Uproc getUprocByName(String name, String version) throws Exception {
		UprocId uprocId = new UprocId(name, version);
		Uproc u = new Uproc(getContext(), uprocId);
		u.setImpl(new UprocStdImpl());
		u.getIdentifier().setSyntaxRules(ClassicSyntaxRules.getInstance());
		u.extract();
		return u;
	}

	public void deleteUProc(String name, String version) throws Exception {
		Context ctx = getContext();
		Uproc obj = new Uproc(ctx, UprocId.create(name, name, version));
		obj.setImpl(new UprocStdImpl());
		obj.disableSyntaxCheck();
		obj.getIdentifier().setSyntaxRules(ClassicSyntaxRules.getInstance());
		obj.delete();
	}
	public Uproc duplicateUproc(Uproc originalObj, String newName, String newLabel) throws Exception{  
		UprocId id = new UprocId(newName,originalObj.getVersion());
		id.setSyntaxRules(ClassicSyntaxRules.getInstance());
		originalObj.duplicate(id, newLabel);
		return getUprocByName(newName,originalObj.getVersion());
	}
	
	public UprocClassList getUprocClassList() throws Exception {  
        UprocClassFilter filter = new UprocClassFilter("*");
        UprocClassList list = new UprocClassList(getContext(), filter);
        list.setImpl(new UprocClassListStdImpl());
        list.extract();
        return list;
	}
	
}
