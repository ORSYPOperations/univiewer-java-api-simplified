package com.orsyp.du5;

import com.orsyp.api.Context;
import com.orsyp.api.syntaxerules.ClassicSyntaxRules;
import com.orsyp.api.user.User;
import com.orsyp.api.user.UserFilter;
import com.orsyp.api.user.UserId;
import com.orsyp.api.user.UserItem;
import com.orsyp.api.user.UserList;
import com.orsyp.du6.GenericObjects;
import com.orsyp.std.UserListStdImpl;
import com.orsyp.std.UserStdImpl;

public class Users  extends GenericObjects {
	private Context GlobalCtx;

	public Users (com.orsyp.stores.UVAPIWrapper conn, Context GlobalCtx){
		super(conn);
		this.GlobalCtx = GlobalCtx;
	}
	private Context getContext(){
		return GlobalCtx;
	}
	
	public void deleteUser(String userName) throws Exception {
		Context ctx = getContext();
		User obj = new User(ctx, UserId.create(userName));
		obj.setImpl(new UserStdImpl());
		obj.disableSyntaxCheck();
		obj.getIdentifier().setSyntaxRules(ClassicSyntaxRules.getInstance());
		obj.delete();
	}
	public UserList getDu6UserList() throws Exception {  
        UserFilter filter = new UserFilter("*","*");
        UserList list = new UserList(getContext(), filter);
        list.setImpl(new UserListStdImpl());
        list.extract();
        return list;
	}
	public User getUser(UserItem item) throws Exception{  
		UserId id = item.getIdentifier();
		User obj = new User(getContext(),id);
		obj.setImpl(new UserStdImpl());
		obj.getIdentifier().setSyntaxRules(ClassicSyntaxRules.getInstance());
	    obj.extract();
	    return obj;
	}
}
