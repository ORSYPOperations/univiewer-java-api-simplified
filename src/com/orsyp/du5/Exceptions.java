package com.orsyp.du5;

import com.orsyp.api.Context;
import com.orsyp.api.outage.Outage;
import com.orsyp.api.outage.OutageFilter;
import com.orsyp.api.outage.OutageId;
import com.orsyp.api.outage.OutageItem;
import com.orsyp.api.outage.OutageList;
import com.orsyp.api.syntaxerules.ClassicSyntaxRules;
import com.orsyp.du6.GenericObjects;
import com.orsyp.std.outage.OutageListStdImpl;
import com.orsyp.std.outage.OutageStdImpl;

public class Exceptions  extends GenericObjects {
	private Context GlobalCtx;

	public Exceptions (com.orsyp.stores.UVAPIWrapper conn, Context GlobalCtx){
		super(conn);
		this.GlobalCtx = GlobalCtx;
	}
	private Context getContext(){
		return GlobalCtx;
	}
	public OutageList getOutageList() throws Exception {  
		OutageFilter filter = new OutageFilter();
		OutageList list = new OutageList(getContext(), filter);
        list.setImpl(new  OutageListStdImpl());
        list.extract();
        return list;
	}
	public Outage getOutage(OutageItem item) throws Exception{  
		OutageId id = item.getIdentifier();
		Outage obj = new Outage(getContext(),id);
		obj.setImpl(new  OutageStdImpl());
		obj.getIdentifier().setSyntaxRules(ClassicSyntaxRules.getInstance());
	    obj.extract();
	    return obj;
	}
}
