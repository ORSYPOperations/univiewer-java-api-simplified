package com.orsyp.du5;

import com.orsyp.UniverseException;
import com.orsyp.api.Context;
import com.orsyp.api.event.ResourceEvent;
import com.orsyp.api.event.ResourceEventItem;
import com.orsyp.api.event.ResourceEventList;
import com.orsyp.api.resource.Resource;
import com.orsyp.api.resource.ResourceFilter;
import com.orsyp.api.resource.ResourceId;
import com.orsyp.api.resource.ResourceItem;
import com.orsyp.api.resource.ResourceList;
import com.orsyp.api.syntaxerules.ClassicSyntaxRules;
import com.orsyp.du6.GenericObjects;
import com.orsyp.std.ResourceListStdImpl;
import com.orsyp.std.ResourceStdImpl;
import com.orsyp.std.event.ResourceEventListStdImpl;
import com.orsyp.std.event.ResourceEventStdImpl;

public class Resources  extends GenericObjects {
	private Context GlobalCtx;

	public Resources (com.orsyp.stores.UVAPIWrapper conn, Context GlobalCtx){
		super(conn);
		this.GlobalCtx = GlobalCtx;
	}
	private Context getContext(){
		return GlobalCtx;
	}
	public ResourceList getResourceList() throws Exception {  
        ResourceFilter filter = new ResourceFilter("*");
        ResourceList list = new ResourceList(getContext(), filter);
        list.setImpl(new ResourceListStdImpl());
        list.extract();
        return list;
	}
	public ResourceEventList getResourceEventList() throws UniverseException{
		ResourceEventList list = new ResourceEventList(getContext());
		list.setImpl(new ResourceEventListStdImpl());
		list.extract();
		return list;
	}
	public ResourceEvent getResourceEvent(ResourceEventItem item) throws UniverseException{
		ResourceId id = item.getIdentifier();
		ResourceEvent obj = new ResourceEvent(getContext(),id);
		obj.setImpl(new ResourceEventStdImpl());
		obj.getIdentifier().setSyntaxRules(ClassicSyntaxRules.getInstance());
	    //obj.extract();
	    return obj;
	}
	public Resource getResource(ResourceItem item) throws Exception{  
		ResourceId id = item.getIdentifier();
		Resource obj = new Resource(getContext(),id);
		obj.setImpl(new ResourceStdImpl());
		obj.getIdentifier().setSyntaxRules(ClassicSyntaxRules.getInstance());
	    obj.extract();
	    return obj;
	}
	public void deleteResource(String resName) throws Exception {
		Context ctx = getContext();
		Resource obj = new Resource(ctx, ResourceId.create(resName));
		obj.setImpl(new ResourceStdImpl());
		obj.disableSyntaxCheck();
		obj.getIdentifier().setSyntaxRules(ClassicSyntaxRules.getInstance());
		obj.delete();
	}
}
