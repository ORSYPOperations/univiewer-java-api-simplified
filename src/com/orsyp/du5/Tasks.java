package com.orsyp.du5;

import com.orsyp.api.Context;
import com.orsyp.api.syntaxerules.ClassicSyntaxRules;
import com.orsyp.api.task.Task;
import com.orsyp.api.task.TaskFilter;
import com.orsyp.api.task.TaskId;
import com.orsyp.api.task.TaskItem;
import com.orsyp.api.task.TaskList;
import com.orsyp.du6.GenericObjects;
import com.orsyp.std.TaskListStdImpl;
import com.orsyp.std.TaskStdImpl;

public class Tasks  extends GenericObjects {
	private Context GlobalCtx;

	public Tasks (com.orsyp.stores.UVAPIWrapper conn, Context GlobalCtx){
		super(conn);
		this.GlobalCtx = GlobalCtx;
	}
	private Context getContext(){
		return GlobalCtx;
	}
	
	public TaskList getTaskList() throws Exception {
		TaskFilter filter = new TaskFilter();
		TaskList list = new TaskList(getContext(), filter);
		list.setImpl(new TaskListStdImpl());
		list.extract();
		return list;
	}
	public Task getTask(TaskItem item) throws Exception{  
		TaskId id = item.getIdentifier();
		Task obj = new Task(getContext(),id);
		obj.setImpl(new TaskStdImpl());
		obj.getIdentifier().setSyntaxRules(ClassicSyntaxRules.getInstance());
	    obj.extract();
	    return obj;
	}
	public Task getTaskByName(String name,boolean isTemplate) throws Exception {
		TaskList list = getTaskList();
		for(int i=0;i<list.getCount();i++){
			TaskItem ent = list.get(i);
			ent.getIdentifier().setSyntaxRules(ClassicSyntaxRules.getInstance());
			if(isTemplate){
				if(ent.getIdentifier().getName().equalsIgnoreCase(name) && ent.isTemplate()){
					TaskId id = ent.getIdentifier();
					Task u = new Task(getContext(), id);
					u.setImpl(new TaskStdImpl());
					u.extract();
					return u;
				}
			}else{
				if(ent.getIdentifier().getName().equalsIgnoreCase(name) && !ent.isTemplate()){
					TaskId id = ent.getIdentifier();
					Task u = new Task(getContext(), id);
					u.setImpl(new TaskStdImpl());
					u.extract();
					return u;
			}
		}
		}
		return null;
	}
	public void deleteTask(String name, String muName, String version) throws Exception {
		Context ctx = getContext();
		Task obj = new Task(ctx, TaskId.createWithName(name, version, muName, true));
		obj.setImpl(new TaskStdImpl());
		obj.disableSyntaxCheck();
		obj.getIdentifier().setSyntaxRules(ClassicSyntaxRules.getInstance());
		obj.delete();
	}
	// method incomplete
	public Task duplicateTask(Task originalObj, String newName) throws Exception{  
		TaskId id = new TaskId();
		id.setSyntaxRules(ClassicSyntaxRules.getInstance());
		id.setSessionName(originalObj.getSessionName());
		id.setSessionVersion(originalObj.getSessionVersion());
		id.setUprocName(originalObj.getUprocName());
		id.setUprocVersion(originalObj.getUprocVersion());
		id.setMuName(originalObj.getIdentifier().getMuName());
		id.setTemplate(originalObj.getIdentifier().isTemplate());
		System.out.println("Version:"+originalObj.getIdentifier().getVersion());
		id.setName(newName);
		id.setVersion("000");
		
		Task mytask = new Task(getContext(), id);
		mytask.setImpl(new TaskStdImpl());
		mytask.setAdvanceDays(originalObj.getAdvanceDays());
		mytask.setAdvanceHours(originalObj.getAdvanceHours());
		mytask.setAdvanceMinutes(originalObj.getAdvanceMinutes());
		
		mytask.setUserName(originalObj.getUserName());
		mytask.setSpecificData(originalObj.getSpecificData());
		mytask.setPriority(originalObj.getPriority());
		mytask.setQueue(originalObj.getQueue());
		mytask.setPrinter(originalObj.getPrinter());
		mytask.setActive(originalObj.isActive());
		mytask.create()
		;
		//originalObj.setImpl(new TaskStdImpl());
		//originalObj.duplicate(id);
		
		Task task = getTaskByName(newName,originalObj.isTemplate());
		return task;
	}
}
