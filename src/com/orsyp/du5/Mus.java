package com.orsyp.du5;

import com.orsyp.api.Context;
import com.orsyp.api.mu.Mu;
import com.orsyp.api.mu.MuDependency;
import com.orsyp.api.mu.MuDependencyFilter;
import com.orsyp.api.mu.MuDependencyId;
import com.orsyp.api.mu.MuDependencyItem;
import com.orsyp.api.mu.MuDependencyList;
import com.orsyp.api.mu.MuFilter;
import com.orsyp.api.mu.MuId;
import com.orsyp.api.mu.MuItem;
import com.orsyp.api.mu.MuList;
import com.orsyp.api.syntaxerules.ClassicSyntaxRules;
import com.orsyp.du6.GenericObjects;
import com.orsyp.std.MuDependencyListStdImpl;
import com.orsyp.std.MuDependencyStdImpl;
import com.orsyp.std.MuListStdImpl;
import com.orsyp.std.MuStdImpl;

public class Mus  extends GenericObjects {
	private Context GlobalCtx;

	public Mus (com.orsyp.stores.UVAPIWrapper conn, Context GlobalCtx){
		super(conn);
		this.GlobalCtx = GlobalCtx;
	}
	private Context getContext(){
		return GlobalCtx;
	}
	
	public MuList getMuList() throws Exception {  
        MuFilter filter = new MuFilter("*","*");
        MuList list = new MuList(getContext(), filter);
        list.setImpl(new MuListStdImpl());
        list.extract();
        return list;
	}
	public MuDependencyList getMuDependencyList() throws Exception {  
        MuDependencyFilter filter = new MuDependencyFilter("*","*");
        MuDependencyList list = new MuDependencyList(getContext(), filter);
        list.setImpl(new MuDependencyListStdImpl());
        list.extract();
        return list;
	}
	public Mu getMu(MuItem item) throws Exception{  
		MuId id = item.getIdentifier();
		Mu obj = new Mu(getContext(),id);
		obj.setImpl(new MuStdImpl());
		obj.getIdentifier().setSyntaxRules(ClassicSyntaxRules.getInstance());
	    obj.extract();
	    return obj;
	}
	public MuDependency getMuDependency(MuDependencyItem item) throws Exception{  
		MuDependencyId id = item.getIdentifier();
		MuDependency obj = new MuDependency(getContext(),id);
		obj.setImpl(new MuDependencyStdImpl());
		obj.getIdentifier().setSyntaxRules(ClassicSyntaxRules.getInstance());
	    obj.extract();
	    return obj;
	}
	public void deleteMu(String muName) throws Exception {
		Mu obj = new Mu(getContext(), muName);
		obj.setImpl(new MuStdImpl());
		obj.delete();
	}
}
