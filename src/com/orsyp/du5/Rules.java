package com.orsyp.du5;

import com.orsyp.api.Context;
import com.orsyp.api.rule.Rule;
import com.orsyp.api.rule.RuleFilter;
import com.orsyp.api.rule.RuleId;
import com.orsyp.api.rule.RuleItem;
import com.orsyp.api.rule.RuleList;
import com.orsyp.api.syntaxerules.ClassicSyntaxRules;
import com.orsyp.du6.GenericObjects;
import com.orsyp.std.RuleListStdImpl;
import com.orsyp.std.RuleStdImpl;

public class Rules  extends GenericObjects {
	private Context GlobalCtx;

	public Rules (com.orsyp.stores.UVAPIWrapper conn, Context GlobalCtx){
		super(conn);
		this.GlobalCtx = GlobalCtx;
	}
	private Context getContext(){
		return GlobalCtx;
	}
	public RuleList getRuleList() throws Exception {  
        RuleFilter filter = new RuleFilter("*","*");
        RuleList list = new RuleList(getContext(), filter);
        list.setImpl(new RuleListStdImpl());
        list.extract();
        return list;
	}
	public Rule getRuleByName(String name) throws Exception{  
		Rule obj = new Rule(getContext(), RuleId.create(name));
	    obj.setImpl(new RuleStdImpl());
	    obj.getIdentifier().setSyntaxRules(ClassicSyntaxRules.getInstance ());
	    obj.extract();
	    return obj;
	}
	public void deleteRule(String ruleName) throws Exception {
		Context ctx = getContext();
		Rule obj = new Rule(ctx, RuleId.create(ruleName));
		obj.setImpl(new RuleStdImpl());
		obj.disableSyntaxCheck();
		obj.getIdentifier().setSyntaxRules(ClassicSyntaxRules.getInstance());
		obj.delete();
	}
	public Rule getRule(RuleItem item) throws Exception{  
		RuleId id = item.getIdentifier();
		Rule obj = new Rule(getContext(),id);
		obj.setImpl(new RuleStdImpl());
		obj.getIdentifier().setSyntaxRules(ClassicSyntaxRules.getInstance());
	    obj.extract();
	    return obj;
	}
}
