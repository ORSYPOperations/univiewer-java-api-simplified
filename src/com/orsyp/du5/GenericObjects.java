package com.orsyp.du5;

public class GenericObjects {

	private com.orsyp.stores.UVAPIWrapper conn;
	
	public GenericObjects(com.orsyp.stores.UVAPIWrapper c){
		
		this.conn = c;
	}
	
	public com.orsyp.stores.UVAPIWrapper getUVAPIWrapper(){
		return this.conn;
	}
	
	public void setUVAPIWrapper(com.orsyp.stores.UVAPIWrapper c){
		this.conn = c;
	}
}
