package com.orsyp.du5;

import com.orsyp.api.Context;
import com.orsyp.api.application.Application;
import com.orsyp.api.application.ApplicationFilter;
import com.orsyp.api.application.ApplicationId;
import com.orsyp.api.application.ApplicationItem;
import com.orsyp.api.application.ApplicationList;
import com.orsyp.api.syntaxerules.ClassicSyntaxRules;
import com.orsyp.du6.GenericObjects;
import com.orsyp.std.ApplicationListStdImpl;
import com.orsyp.std.ApplicationStdImpl;

public class Applications  extends GenericObjects {
	private Context GlobalCtx;

	public Applications (com.orsyp.stores.UVAPIWrapper conn, Context GlobalCtx){
		super(conn);
		this.GlobalCtx = GlobalCtx;
	}
	private Context getContext(){
		return GlobalCtx;
	}
	public ApplicationList getApplicationList() throws Exception {  
        ApplicationFilter filter = new ApplicationFilter("*");
        ApplicationList list = new ApplicationList(getContext(), filter);
        list.setImpl(new ApplicationListStdImpl());
        list.extract();
        return list;
	}
	public Application getApplication(ApplicationItem item) throws Exception{  
		ApplicationId id = item.getIdentifier();
		Application obj = new Application(getContext(),id);
		obj.setImpl(new  ApplicationStdImpl());
		obj.getIdentifier().setSyntaxRules(ClassicSyntaxRules.getInstance());
	    obj.extract();
	    return obj;
	}
	public void createApp(String appName,String appDomain, String label) throws Exception {
        Application obj = new Application(getContext(), appName);
        obj.setDomain(appDomain);
        obj.setLabel(label);
        obj.setImpl(new  ApplicationStdImpl());
        obj.getIdentifier().setSyntaxRules(ClassicSyntaxRules.getInstance());
        obj.create();
	}

}
