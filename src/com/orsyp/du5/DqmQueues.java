package com.orsyp.du5;

import com.orsyp.api.Context;
import com.orsyp.api.dqm.DqmQueue;
import com.orsyp.api.dqm.DqmQueueFilter;
import com.orsyp.api.dqm.DqmQueueId;
import com.orsyp.api.dqm.DqmQueueItem;
import com.orsyp.api.dqm.DqmQueueList;
import com.orsyp.api.syntaxerules.ClassicSyntaxRules;
import com.orsyp.du6.GenericObjects;
import com.orsyp.std.dqm.DqmQueueListStdImpl;
import com.orsyp.std.dqm.DqmQueueStdImpl;

public class DqmQueues  extends GenericObjects {
	private Context GlobalCtx;

	public DqmQueues (com.orsyp.stores.UVAPIWrapper conn, Context GlobalCtx){
		super(conn);
		this.GlobalCtx = GlobalCtx;
	}
	private Context getContext(){
		return GlobalCtx;
	}
	
	
	
	
	public DqmQueueList getDqmQueueList() throws Exception {  
		DqmQueueFilter filter = new DqmQueueFilter();
		DqmQueueList list = new DqmQueueList(getContext(), filter,true,"");
        list.setImpl(new  DqmQueueListStdImpl());
        list.extract();
        return list;
	}
	public DqmQueue getDqmQueue(DqmQueueItem item) throws Exception{  
		DqmQueueId id = item.getIdentifier();
		DqmQueue obj = new DqmQueue(getContext(),id);
		obj.setImpl(new  DqmQueueStdImpl());
		obj.getIdentifier().setSyntaxRules(ClassicSyntaxRules.getInstance());
	    obj.extract();
	    return obj;
	}
}
