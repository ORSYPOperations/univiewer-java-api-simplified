package com.orsyp.du5;

import com.orsyp.api.Context;
import com.orsyp.api.domain.Domain;
import com.orsyp.api.domain.DomainFilter;
import com.orsyp.api.domain.DomainId;
import com.orsyp.api.domain.DomainItem;
import com.orsyp.api.domain.DomainList;
import com.orsyp.api.syntaxerules.ClassicSyntaxRules;
import com.orsyp.du6.GenericObjects;
import com.orsyp.std.DomainListStdImpl;
import com.orsyp.std.DomainStdImpl;

public class Domains  extends GenericObjects {
	private Context GlobalCtx;

	public Domains (com.orsyp.stores.UVAPIWrapper conn, Context GlobalCtx){
		super(conn);
		this.GlobalCtx = GlobalCtx;
	}
	private Context getContext(){
		return GlobalCtx;
	}
	public DomainList getDomainList() throws Exception {  
        DomainFilter filter = new DomainFilter("*");
        DomainList list = new DomainList(getContext(), filter);
        list.setImpl(new  DomainListStdImpl());
        list.extract();
        return list;
	}
	public Domain getDomain(DomainItem item) throws Exception{  
		DomainId id = item.getIdentifier();
		Domain obj = new Domain(getContext(),id);
		obj.setImpl(new  DomainStdImpl());
		obj.getIdentifier().setSyntaxRules(ClassicSyntaxRules.getInstance());
	    obj.extract();
	    return obj;
	}
	public void createDomain(String name, String label) throws Exception {
        Domain obj = new Domain(getContext(), name);
        obj.setLabel(label);
        obj.setImpl(new  DomainStdImpl());
        obj.getIdentifier().setSyntaxRules(ClassicSyntaxRules.getInstance());
        obj.create();
	}
}
