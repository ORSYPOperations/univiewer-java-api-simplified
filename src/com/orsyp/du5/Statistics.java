package com.orsyp.du5;

import com.orsyp.api.Context;
import com.orsyp.api.hr.HrStatistic;
import com.orsyp.api.hr.HrStatisticId;
import com.orsyp.api.hr.HrStatisticItem;
import com.orsyp.api.syntaxerules.ClassicSyntaxRules;
import com.orsyp.du6.GenericObjects;

public class Statistics  extends GenericObjects {
	private Context GlobalCtx;

	public Statistics (com.orsyp.stores.UVAPIWrapper conn, Context GlobalCtx){
		super(conn);
		this.GlobalCtx = GlobalCtx;
	}
	private Context getContext(){
		return GlobalCtx;
	}
	// following method will prob not work?
	public HrStatistic getHrStatistic(HrStatisticItem item) throws Exception{  
		HrStatisticId id = item.getIdentifier();
		HrStatistic obj = new HrStatistic(getContext(),id);
		//obj.setImpl(new HrStatisticStdStdImpl());
		obj.getIdentifier().setSyntaxRules(ClassicSyntaxRules.getInstance());
	    obj.extract();
	    return obj;
	}	
}
