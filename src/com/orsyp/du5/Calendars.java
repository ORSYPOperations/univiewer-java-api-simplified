package com.orsyp.du5;

import com.orsyp.api.Context;
import com.orsyp.api.calendar.Calendar;
import com.orsyp.api.calendar.CalendarFilter;
import com.orsyp.api.calendar.CalendarId;
import com.orsyp.api.calendar.CalendarItem;
import com.orsyp.api.calendar.CalendarList;
import com.orsyp.api.syntaxerules.ClassicSyntaxRules;
import com.orsyp.du6.GenericObjects;
import com.orsyp.std.CalendarListStdImpl;
import com.orsyp.std.CalendarStdImpl;

public class Calendars  extends GenericObjects {
	private Context GlobalCtx;

	public Calendars (com.orsyp.stores.UVAPIWrapper conn, Context GlobalCtx){
		super(conn);
		this.GlobalCtx = GlobalCtx;
	}
	private Context getContext(){
		return GlobalCtx;
	}
	
	
	public CalendarList getCalendarList() throws Exception {  
        CalendarFilter filter = new CalendarFilter();
        CalendarList list = new CalendarList(getContext(), filter);
        list.setImpl(new  CalendarListStdImpl());
        list.extract();
        return list;
	}
	public Calendar getCalendar(CalendarItem item) throws Exception{  
		CalendarId id = item.getIdentifier();
		Calendar obj = new Calendar(getContext(),id);
		obj.setImpl(new  CalendarStdImpl());
		obj.getIdentifier().setSyntaxRules(ClassicSyntaxRules.getInstance());
	    obj.extract();
	    return obj;
	}
}
