ORSYP Workload Automation Packaged Stores
March 2014 - v1.0
April 2014 - v1.1: 
	- Added features for Packages, BVs and Tasks for DU6
	- Added the ability to call methods from other stores while being within a store: the removes code duplication and eases the building of new / advanced features
		ex: creating a method in the Tasks catalogue that returns a UVMS Package containing all the task's object (it requires methods from the UVMS Store)